## Main information

**BPMNDiffViz** - a web based application for comparing business process models in BPMN 2.0 XML format.

BPMNDiffViz was developed as a tool, which helps users to find structural differences between two business processes represented in BPMN format. The differences are visualized and the user can navigate between them. BPMNDiffViz shows, which elements match (blue color), which should be added (green color), which should be deleted (red color) to transform one process model to another.

![BPMN.jpg](https://bitbucket.org/repo/699g5X/images/3279316693-BPMN.jpg)

## Get sources
```
$ git clone https://sivanov68@bitbucket.org/sivanov68/bpmndiffviz.git
```

## Installation
1. Install Tomcat (tested with versions `7.0.54`, `9.0.12` and `9.0.43`) - https://tomcat.apache.org/download-90.cgi
1. Install PostgreSQL with pgAdmin (tested with versions `9.1.1` and `10.16`) - http://www.postgresql.org/download/
    1. Type `89106540101` as a password for the `postgres` database user during setup
1. Launch pgAdmin
    - pgAdmin 3:
        1. Connect to the localhost (File -> Add server... -> Name: *any_name_you_want*, Host: localhost, other fields: *default*)
        1. If connection is successfully established but you do not see any servers in the list restart pgAdmin3 (looks like a bug)
        1. Create database "vkr"
    - pgAdmin 4:
        1. Click on `Servers` > `PostgreSQL <version>` > `Databases` and create a new database "vkr"
1. If you want to make any changes (if you do not want then go to the next step):
    1. Get sources of BPMNDiffViz
    1. Set up database connection (`BPMNDiffViz/src/main/webapp/jdbc.properties`)
    1. Build BPMNDiffViz.war by yourself using Maven ("mvn package" command)
       - You can find the instuctions for installing maven here: https://maven.apache.org/install.html
    1. Get `BPMNDiffViz/target/BPMNDiffViz.war`
1. Put BPMNDiffViz.war into Tomcat webapps folder (`<Tomcat installation root>/webapps`)
1. Start Tomcat using shell (`<Tomcat installation root>/bin/startup.bat`)
    * If you are getting an error when tomcat can not bind to the port change it to 8081 (or any other not used port) in the `<Tomcat installation root>/conf/server.xml`
1. Open browser and go to URL: [http://localhost:8080/BPMNDiffViz/](http://localhost:8080/BPMNDiffViz/) (it may be another port if you changed it in the previous step)
1. Create a folder for storing models and input it into the *Models path* field on the *Settings* tab

## Website
[BPMNDiffViz web page](http://pais.hse.ru/research/projects/CompBPMN)

## License
Apache License Version 2.0

## Contacts
Anna A.Kalenkova (akalenkova@gmail.com)  
Andrey Skobtsov (andrewskobcov@yandex.ru)
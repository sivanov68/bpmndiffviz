package ru.pais.vkr.speed

import org.camunda.bpm.model.bpmn.Bpmn
import org.camunda.bpm.model.bpmn.BpmnModelInstance
import org.junit.Test
import ru.pais.vkr.comparator.entities.ComparisonResult
import ru.pais.vkr.comparator.entities.CostConfig
import ru.pais.vkr.services.model_comparators.a_star.AStarModelComparator
import ru.pais.vkr.services.model_comparators.ants.AntsModelComparator
import ru.pais.vkr.services.model_comparators.greedy.GreedyModelComparator
import ru.pais.vkr.services.model_comparators.simulated_annealing.SimulatedAnnealingComparator
import ru.pais.vkr.services.model_comparators.tabu_search.TabuSearchModelComparator
import java.io.File
import java.io.OutputStreamWriter
import java.nio.file.Paths

class MethodsBenchmarks {

    companion object {
        private const val WARM_UP_RUNS = 0
        private const val MEASUREMENTS = 1
    }

    private val outputFolder = Paths.get("benchmark_results")
    private val modelsFolder = utils.Paths.startPath.resolve("speed").resolve("benchmarks")

    @Test
    fun evaluate() {
        outputFolder.toFile().mkdirs()

        val resultFile: File = generateSequence(1) { it + 1 }.map { outputFolder.resolve("$it.csv").toFile() }.first { !it.exists() }

        resultFile.outputStream().writer(Charsets.UTF_8).use {
            // artificial
            val artificialCount = 13
            for (i in 1..artificialCount) {
                println("artificial: $i of $artificialCount")

                val source = modelsFolder.resolve("artificial_${i}_source.bpmn")
                val target = modelsFolder.resolve("artificial_${i}_target.bpmn")

                measureFiles(source.toFile(), target.toFile(), it, i == 1)
            }

            it.write("\n")
            println()

            // alpha vs inductive
            var i = 1
            val aviPercentages = arrayOf(10, 20, 30, 40, 50, 60, 70, 80, 90, 100)
            for (percentage in aviPercentages) {
                println("alpha vs inductive: $i of ${aviPercentages.size}")

                val source = modelsFolder.resolve("alpha_vs_inductive_$percentage%_source.bpmn")
                val target = modelsFolder.resolve("alpha_vs_inductive_$percentage%_target.bpmn")

                measureFiles(source.toFile(), target.toFile(), it, i == 1)
                i++
            }

            it.write("\n")
            println()

            // inductive vs inductive
            i = 1
            val iviPercentages = arrayOf(10, 20, 30, 40, 50, 60, 70, 80, 90, 99, 100)
            for (percentage in iviPercentages) {
                println("inductive vs inductive: $i of ${iviPercentages.size}")

                val source = modelsFolder.resolve("inductive_vs_inductive_$percentage%_source.bpmn")
                val target = modelsFolder.resolve("inductive_vs_inductive_$percentage%_target.bpmn")

                measureFiles(source.toFile(), target.toFile(), it, i == 1)
                i++
            }
        }
    }

    private fun measureFiles(source: File, target: File, writer: OutputStreamWriter, firstInFiles: Boolean) {
        val sourceModel: BpmnModelInstance = loadModel(source)
        val targetModel: BpmnModelInstance = loadModel(target)

        val stats = ArrayList<OverallStats>()

        fun measure(method: (source: BpmnModelInstance, target: BpmnModelInstance, costConfig: CostConfig) -> OneStats) {
            stats.add(measureMethod(sourceModel, targetModel, method))
        }

        // measuring methods
        measure(this::measureGreedyUsual)
        measure(this::measureGreedyTypePriority)
        measure(this::measureGreedyAlreadyMatchedPriority)
        measure(this::measureGreedyTypeAndMatchedPriority)
        measure(this::measureTabuUsual)
        measure(this::measureTabuTypePriority)
        measure(this::measureTabuAlreadyMatchedPriority)
        measure(this::measureTabuTypeAndMatchedPriority)
        measure(this::measureAStar)
        measure(this::measureSimulatedAnnealingUsual)
        measure(this::measureSimulatedAnnealingTypePriority)
        measure(this::measureSimulatedAnnealingAlreadyMatchedPriority)
        measure(this::measureSimulatedAnnealingTypeAndMatchedPriority)
        measure(this::measureAntsDefault)
        measure(this::measureAntsBest)
//        measureAntsAll(::measure)

        // column titles
        if (firstInFiles) {
            writer.write("Source vertices,Source edges,Target vertices,Target edges")
            (stats + stats).forEach {
                writer.write(",")
                writer.write(it.methodName)
            }
            writer.write("\n")
        }

        // general info
        writer.write(stats.first().sourceGraphVertices.toString())
        writer.write(",")
        writer.write(stats.first().sourceGraphEdges.toString())
        writer.write(",")
        writer.write(stats.first().targetGraphVertices.toString())
        writer.write(",")
        writer.write(stats.first().targetGraphEdges.toString())

        // times
        stats.forEach {
            writer.write(",")
            writer.write(it.timeAverage.toString())
        }

        // penalties
        stats.forEach {
            writer.write(",")
            writer.write(it.penaltyAverage.toString())
        }

        writer.write("\n")
        writer.flush()
    }

    // region Greedy

    private fun measureGreedyUsual(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig
    ): OneStats {
        return measureGreedy(
            source = source,
            target = target,
            costConfig = costConfig,
            useTypePriority = false,
            useAlreadyMatchedPriority = false
        )
    }

    private fun measureGreedyTypePriority(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig
    ): OneStats {
        return measureGreedy(
            source = source,
            target = target,
            costConfig = costConfig,
            useTypePriority = true,
            useAlreadyMatchedPriority = false
        )
    }

    private fun measureGreedyAlreadyMatchedPriority(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig
    ): OneStats {
        return measureGreedy(
            source = source,
            target = target,
            costConfig = costConfig,
            useTypePriority = false,
            useAlreadyMatchedPriority = true
        )
    }

    private fun measureGreedyTypeAndMatchedPriority(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig
    ): OneStats {
        return measureGreedy(
            source = source,
            target = target,
            costConfig = costConfig,
            useTypePriority = true,
            useAlreadyMatchedPriority = true
        )
    }

    private fun measureGreedy(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig,
        useTypePriority: Boolean,
        useAlreadyMatchedPriority: Boolean
    ): OneStats {
        val result: ComparisonResult = GreedyModelComparator.compareModels(
            source, target, costConfig,
            with (GreedyModelComparator.Parameters) {
                mapOf(
                    USE_TYPE_PRIORITY.pairWith(useTypePriority),
                    USE_ALREADY_MATCHED_PRIORITY.pairWith(useAlreadyMatchedPriority)
                )
            },
            useParallelization = false
        )

        var name = "Greedy"
        if (useTypePriority && useAlreadyMatchedPriority) {
            name += " (type priority + already matched priority)"
        } else if (useTypePriority) {
            name += " (type priority)"
        } else if (useAlreadyMatchedPriority) {
            name += " (already matched priority)"
        }

        with(result.statistics) {
            GreedyModelComparator.Statistics.let {
                return OneStats(
                    name,
                    it.SOURCE_GRAPH_VERTICES.valueFrom(this),
                    it.TARGET_GRAPH_VERTICES.valueFrom(this),
                    it.SOURCE_GRAPH_EDGES.valueFrom(this),
                    it.TARGET_GRAPH_EDGES.valueFrom(this),
                    result.penalty,
                    it.TOTAL_TIME_MILLIS.valueFrom(this)
                )
            }
        }
    }

    // endregion

    // region Tabu

    private fun measureTabuUsual(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig
    ): OneStats {
        return measureTabu(
            source = source,
            target = target,
            costConfig = costConfig,
            useGreedyTypePriority = false,
            useGreedyAlreadyMatchedPriority = false
        )
    }

    private fun measureTabuTypePriority(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig
    ): OneStats {
        return measureTabu(
            source = source,
            target = target,
            costConfig = costConfig,
            useGreedyTypePriority = true,
            useGreedyAlreadyMatchedPriority = false
        )
    }

    private fun measureTabuAlreadyMatchedPriority(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig
    ): OneStats {
        return measureTabu(
            source = source,
            target = target,
            costConfig = costConfig,
            useGreedyTypePriority = false,
            useGreedyAlreadyMatchedPriority = true
        )
    }

    private fun measureTabuTypeAndMatchedPriority(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig
    ): OneStats {
        return measureTabu(
            source = source,
            target = target,
            costConfig = costConfig,
            useGreedyTypePriority = true,
            useGreedyAlreadyMatchedPriority = true
        )
    }

    private fun measureTabu(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig,
        useGreedyTypePriority: Boolean,
        useGreedyAlreadyMatchedPriority: Boolean
    ): OneStats {
        val result: ComparisonResult = TabuSearchModelComparator.compareModels(
            source, target, costConfig,
            with (TabuSearchModelComparator.Parameters) {
                mapOf(
                    MAX_EXPANSIONS.pairWith(100),
                    TABU_LIST_SIZE.pairWith(100),
                    USE_GREEDY_TYPE_PRIORITY.pairWith(useGreedyTypePriority),
                    USE_GREEDY_ALREADY_MATCHED_PRIORITY.pairWith(useGreedyAlreadyMatchedPriority)
                )
            },
            useParallelization = false
        )

        var name = "Tabu"
        if (useGreedyTypePriority && useGreedyAlreadyMatchedPriority) {
            name += " (type priority + already matched priority)"
        } else if (useGreedyTypePriority) {
            name += " (type priority)"
        } else if (useGreedyAlreadyMatchedPriority) {
            name += " (already matched priority)"
        }

        with(result.statistics) {
            TabuSearchModelComparator.Statistics.let {
                return OneStats(
                    name,
                    it.SOURCE_GRAPH_VERTICES.valueFrom(this),
                    it.TARGET_GRAPH_VERTICES.valueFrom(this),
                    it.SOURCE_GRAPH_EDGES.valueFrom(this),
                    it.TARGET_GRAPH_EDGES.valueFrom(this),
                    result.penalty,
                    it.ALGORITHM_TOTAL_TIME_MS.valueFrom(this)
                )
            }
        }
    }

    // endregion

    // region AStar

    private fun measureAStar(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig
    ): OneStats {
        val result: ComparisonResult = AStarModelComparator().compareModels(
            source, target, costConfig,
            emptyMap(),
            useParallelization = false
        )

        with(result.statistics) {
            return OneStats(
                "A*",
                -1,
                -1,
                -1,
                -1,
                result.penalty,
                AStarModelComparator.Statistics.TOTAL_TIME_MS.valueFrom(this)
            )
        }
    }

    // endregion

    // region Simulated annealing

    private fun measureSimulatedAnnealingUsual(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig
    ): OneStats {
        return measureSimulatedAnnealing(
            source = source,
            target = target,
            costConfig = costConfig,
            useGreedyTypePriority = false,
            useGreedyAlreadyMatchedPriority = false
        )
    }

    private fun measureSimulatedAnnealingTypePriority(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig
    ): OneStats {
        return measureSimulatedAnnealing(
            source = source,
            target = target,
            costConfig = costConfig,
            useGreedyTypePriority = true,
            useGreedyAlreadyMatchedPriority = false
        )
    }

    private fun measureSimulatedAnnealingAlreadyMatchedPriority(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig
    ): OneStats {
        return measureSimulatedAnnealing(
            source = source,
            target = target,
            costConfig = costConfig,
            useGreedyTypePriority = false,
            useGreedyAlreadyMatchedPriority = true
        )
    }

    private fun measureSimulatedAnnealingTypeAndMatchedPriority(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig
    ): OneStats {
        return measureSimulatedAnnealing(
            source = source,
            target = target,
            costConfig = costConfig,
            useGreedyTypePriority = true,
            useGreedyAlreadyMatchedPriority = true
        )
    }

    private fun measureSimulatedAnnealing(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig,
        useGreedyTypePriority: Boolean,
        useGreedyAlreadyMatchedPriority: Boolean
    ): OneStats {
        val result: ComparisonResult = SimulatedAnnealingComparator.compareModels(
            source, target, costConfig,
            with (SimulatedAnnealingComparator.Parameters) {
                mapOf(
                    INITIAL_TEMPERATURE.pairWith(100.0),
                    MINIMAL_TEMPERATURE.pairWith(0.0),
                    TEMPERATURE_DECREASE.pairWith(0.001),
                    USE_GREEDY_TYPE_PRIORITY.pairWith(useGreedyTypePriority),
                    USE_GREEDY_ALREADY_MATCHED_PRIORITY.pairWith(useGreedyTypePriority),
                    CHOOSE_THE_BEST.pairWith(true)
                )
            },
            useParallelization = false
        )

        var name = "Simulated Annealing"
        if (useGreedyTypePriority && useGreedyAlreadyMatchedPriority) {
            name += " (type priority + already matched priority)"
        } else if (useGreedyTypePriority) {
            name += " (type priority)"
        } else if (useGreedyAlreadyMatchedPriority) {
            name += " (already matched priority)"
        }

        with(result.statistics) {
            SimulatedAnnealingComparator.Statistics.let {
                return OneStats(
                    name,
                    it.SOURCE_GRAPH_VERTICES.valueFrom(this),
                    it.TARGET_GRAPH_VERTICES.valueFrom(this),
                    it.SOURCE_GRAPH_EDGES.valueFrom(this),
                    it.TARGET_GRAPH_EDGES.valueFrom(this),
                    result.penalty,
                    it.TOTAL_TIME_MILLIS.valueFrom(this)
                )
            }
        }
    }

    private fun measureAntsAll(measurer: (method: (source: BpmnModelInstance, target: BpmnModelInstance, costConfig: CostConfig) -> OneStats) -> Unit) {
        for (initialPheromoneLevel in 1..3) {
            for (pheromonePower in 1..3) {
                for (distancePower in 1..3) {
                    for (distanceCoefficient in 1..3) {
                        for (pheromoneEvaporation in 1..3) {
                            measurer {
                                source, target, costConfig ->
                                measureAnts(source, target, costConfig,
                                    initialPheromoneLevel = initialPheromoneLevel.toDouble(),
                                    numberOfAntsPerIteration = 10,
                                    numberOfIterations = 10,
                                    pheromonePower = pheromonePower.toDouble(),
                                    distancePower = distancePower.toDouble(),
                                    distanceCoefficient = distanceCoefficient.toDouble(),
                                    pheromoneEvaporation = pheromoneEvaporation.toDouble()
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    private fun measureAntsDefault(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig
    ): OneStats {
        return measureAnts(source, target, costConfig,
            initialPheromoneLevel = 1.0,
            numberOfAntsPerIteration = 10,
            numberOfIterations = 10,
            pheromonePower = 1.0,
            distancePower = 1.0,
            distanceCoefficient = 1.0,
            pheromoneEvaporation = 1.0
        )
    }

    private fun measureAntsBest(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig
    ): OneStats {
        return measureAnts(source, target, costConfig,
            initialPheromoneLevel = 2.0,
            numberOfAntsPerIteration = 10,
            numberOfIterations = 10,
            pheromonePower = 2.0,
            distancePower = 3.0,
            distanceCoefficient = 3.0,
            pheromoneEvaporation = 2.0
        )
    }

    private fun measureAnts(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        costConfig: CostConfig,
        initialPheromoneLevel: Double,
        numberOfAntsPerIteration: Int,
        numberOfIterations: Int,
        pheromonePower: Double,
        distancePower: Double,
        distanceCoefficient: Double,
        pheromoneEvaporation: Double
    ): OneStats {
        val result: ComparisonResult = AntsModelComparator.compareModels(
            source, target, costConfig,
            with (AntsModelComparator.Parameters) {
                mapOf(
                    INITIAL_PHEROMONE_LEVEL.pairWith(initialPheromoneLevel),
                    NUMBER_OF_ANTS_PER_ITERATION.pairWith(numberOfAntsPerIteration),
                    NUMBER_OF_ANTS_PER_ITERATION.pairWith(numberOfIterations),
                    PHEROMONE_POWER.pairWith(pheromonePower),
                    DISTANCE_POWER.pairWith(distancePower),
                    DISTANCE_COEFFICIENT.pairWith(distanceCoefficient),
                    PHEROMONE_EVAPORATION.pairWith(pheromoneEvaporation)
                )
            },
            useParallelization = false
        )

        val name = "Ants ($numberOfAntsPerIteration-$numberOfIterations-$initialPheromoneLevel-$pheromonePower-$distancePower-$distanceCoefficient-$pheromoneEvaporation)"

        with(result.statistics) {
            AntsModelComparator.Statistics.let {
                return OneStats(
                    name,
                    it.SOURCE_GRAPH_VERTICES.valueFrom(this),
                    it.TARGET_GRAPH_VERTICES.valueFrom(this),
                    it.SOURCE_GRAPH_EDGES.valueFrom(this),
                    it.TARGET_GRAPH_EDGES.valueFrom(this),
                    result.penalty,
                    it.TOTAL_TIME_MILLIS.valueFrom(this)
                )
            }
        }
    }

    // endregion

    private fun measureMethod(
        source: BpmnModelInstance,
        target: BpmnModelInstance,
        method: (source: BpmnModelInstance, target: BpmnModelInstance, costConfig: CostConfig) -> OneStats
    ): OverallStats {
        for (i in 1..WARM_UP_RUNS) {
            method(source, target, genConfig())
        }

        val stats = ArrayList<OneStats>()
        for (i in 1..MEASUREMENTS) {
            stats.add(method(source, target, genConfig()))
        }

        return OverallStats.from(stats)
    }

    private fun loadModel(file: File): BpmnModelInstance {
        return Bpmn.readModelFromFile(file)
    }

    private fun genConfig(): CostConfig {
        return CostConfig().apply { factorLevenshtein = 10.0 }
    }

    private data class OverallStats(
        val methodName: String,
        val sourceGraphVertices: Int,
        val targetGraphVertices: Int,
        val sourceGraphEdges: Int,
        val targetGraphEdges: Int,
        val penaltyAverage: Int,
        val timeAverage: Int
    ) {
        companion object {
            fun from(stats: List<OneStats>): OverallStats {
                if (stats.isEmpty())
                    throw IllegalArgumentException()

                if (
                    stats.map { it.methodName }.toSet().size != 1 ||
                    stats.map { it.sourceGraphVertices }.toSet().size != 1 ||
                    stats.map { it.targetGraphVertices }.toSet().size != 1 ||
                    stats.map { it.sourceGraphEdges }.toSet().size != 1 ||
                    stats.map { it.targetGraphEdges }.toSet().size != 1
                ) {
                    throw Exception("different values")
                }

                return OverallStats(
                    methodName = stats.first().methodName,
                    sourceGraphVertices = stats.first().sourceGraphVertices,
                    targetGraphVertices = stats.first().targetGraphVertices,
                    sourceGraphEdges = stats.first().sourceGraphEdges,
                    targetGraphEdges = stats.first().targetGraphEdges,
                    penaltyAverage = Math.ceil(stats.map { it.penalty }.average()).toInt(),
                    timeAverage = Math.ceil(stats.map { it.time }.average()).toInt()
                )
            }
        }
    }

    private data class OneStats(
        val methodName: String,
        val sourceGraphVertices: Int,
        val targetGraphVertices: Int,
        val sourceGraphEdges: Int,
        val targetGraphEdges: Int,
        val penalty: Double,
        val time: Int
    )
}
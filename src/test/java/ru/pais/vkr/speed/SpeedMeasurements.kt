package ru.pais.vkr.speed

import org.camunda.bpm.model.bpmn.Bpmn
import org.camunda.bpm.model.bpmn.BpmnModelInstance
import org.junit.Test
import ru.pais.vkr.comparator.entities.ComparisonResult
import ru.pais.vkr.comparator.entities.CostConfig
import ru.pais.vkr.services.model_comparators.greedy.GreedyModelComparator
import ru.pais.vkr.services.model_comparators.tabu_search.TabuSearchModelComparator
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SpeedMeasurements {
    companion object {
        private val startPath: Path = utils.Paths.startPath.resolve("speed").resolve("measurements")
        private const val line = "-------------------------------"
        private const val lineSmall = "---"
    }

    @Test
    fun greedy() {
        checkMethod("greedy", 594.0, 170.0) { source, target ->
            GreedyModelComparator.compareModels(
                source, target,
                genConfig(), emptyMap(), false
            )
        }
    }

    @Test
    fun tabu() {
        checkMethod("tabu", 582.0, 790.0) { source, target ->
            TabuSearchModelComparator.compareModels(
                source, target,
                genConfig(), genParameters(), false
            )
        }
    }

    private inline fun checkMethod(
        methodName: String,
        expectedPenalty: Double,
        maximalAverage: Double,
        method: (source: BpmnModelInstance, target: BpmnModelInstance) -> ComparisonResult
    ) {
        val source = Paths.get(startPath.toString(), "1_source.bpmn")
        val target = Paths.get(startPath.toString(), "1_target.bpmn")

        val times = ArrayList<Long>()
        val warmupTimes = 30
        val checkTimes = 10

        for (i in 1..(warmupTimes + checkTimes)) {
            val sourceModel = Bpmn.readModelFromFile(source.toFile())
            val targetModel = Bpmn.readModelFromFile(target.toFile())

            val start = System.currentTimeMillis()
            val result = method(sourceModel, targetModel)
            val end = System.currentTimeMillis()
            val diff = end - start

            assertEquals(expectedPenalty, result.penalty)

            if (i <= warmupTimes)
                continue

            times.add(diff)
        }

        printStats(methodName, times)
        assertTrue { times.average() <= maximalAverage }
    }

    private fun printStats(method: String, times: List<Long>) {
        println(line)
        println(method)
        println(lineSmall)
        println("total checks: ${times.size}")
        println("all results: ${times.joinToString(", ")}")
        println("min: ${times.min()}")
        println("max: ${times.max()}")
        println("average: ${times.average()}")
    }

    private fun genConfig(): CostConfig {
        return CostConfig().apply { factorLevenshtein = 10.0 }
    }

    private fun genParameters(): Map<String, Any> {
        return mapOf(
            TabuSearchModelComparator.Parameters.TABU_LIST_SIZE.name to 50,
            TabuSearchModelComparator.Parameters.MAX_EXPANSIONS.name to 50
        )
    }
}
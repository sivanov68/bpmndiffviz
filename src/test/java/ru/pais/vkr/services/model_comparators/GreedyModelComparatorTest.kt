package ru.pais.vkr.services.model_comparators

import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import ru.pais.vkr.comparator.entities.BPMNElement
import ru.pais.vkr.comparator.entities.BPMNElementType
import ru.pais.vkr.comparator.entities.CostConfig
import ru.pais.vkr.graph_elements.Graph
import ru.pais.vkr.graph_elements.Vertex
import ru.pais.vkr.services.model_comparators.greedy.GreedyModelComparator
import ru.pais.vkr.utils.StringUtil
import java.nio.file.Path
import kotlin.test.assertEquals

class GreedyModelComparatorTest {

    companion object {
        private val startPath: Path = utils.Paths.startPath.resolve("greedy tests")
    }

    @Test
    fun `equal`() {
        val sourceRawVertex: BPMNElement = mockBpmnElement("1", BPMNElementType.Task)
        val targetRawVertex: BPMNElement = mockBpmnElement("1", BPMNElementType.Task)

        val sourceGraph = Graph(mutableListOf(Vertex(sourceRawVertex)), mutableListOf())
        val targetGraph = Graph(mutableListOf(Vertex(targetRawVertex)), mutableListOf())

        val costConfig = CostConfig()

        val output = GreedyModelComparator.compareNew(
            sourceGraph,
            targetGraph,
            costConfig,
            useTypePriority = false,
            useAlreadyMatchedPriority = false
        )

        assertEquals(1, output.stepsCount)
        assertEquals(0.0, totalCost(output))
    }

    @Test
    fun `string diff`() {
        val sourceRawVertex: BPMNElement = mockBpmnElement("1", BPMNElementType.Task)
        val targetRawVertex: BPMNElement = mockBpmnElement("12345", BPMNElementType.Task)

        val sourceGraph = Graph(mutableListOf(Vertex(sourceRawVertex)), mutableListOf())
        val targetGraph = Graph(mutableListOf(Vertex(targetRawVertex)), mutableListOf())

        val costConfig = CostConfig()

        val output = GreedyModelComparator.compareNew(
            sourceGraph,
            targetGraph,
            costConfig,
            useTypePriority = false,
            useAlreadyMatchedPriority = false
        )

        assertEquals(1, output.stepsCount)
        assertEquals(
            StringUtil.computeLevenshteinDistance(sourceRawVertex.name, targetRawVertex.name).toDouble(),
            totalCost(output)
        )
    }

    @Test
    fun `string diff 2`() {
        val sourceRawVertices: List<BPMNElement> = listOf(
            mockBpmnElement("1", BPMNElementType.Task)
        )
        val targetRawVertices: List<BPMNElement> = listOf(
            mockBpmnElement("12345", BPMNElementType.Task)
        )

        val sourceVertices: MutableList<Vertex> = sourceRawVertices.map { Vertex(it) }.toMutableList()
        val targetVertices: MutableList<Vertex> = targetRawVertices.map { Vertex(it) }.toMutableList()

        val sourceGraph = Graph(sourceVertices, mutableListOf())
        val targetGraph = Graph(targetVertices, mutableListOf())

        val costConfig = CostConfig()

        val output = GreedyModelComparator.compareNew(
            sourceGraph,
            targetGraph,
            costConfig,
            useTypePriority = false,
            useAlreadyMatchedPriority = false
        )

        assertEquals(1, output.stepsCount)
        assertEquals(StringUtil.computeLevenshteinDistance(sourceRawVertices[0].name, targetRawVertices[0].name).toDouble(), totalCost(output))
    }

    private fun mockBpmnElement(name: String, type: BPMNElementType): BPMNElement {
        val element: BPMNElement = mock(BPMNElement::class.java)

        `when`(element.name).thenReturn(name)
        `when`(element.type).thenReturn(type)

        return element
    }

    private fun vertexCost(output: GreedyModelComparator.ComparisonOutput): Double {
        return output.changes.sumByDouble { it.vertex.differenceCost }
    }

    private fun edgeCost(output: GreedyModelComparator.ComparisonOutput): Double {
        return output.changes.sumByDouble { it.edges.sumByDouble { it.differenceCost } }
    }

    private fun totalCost(output: GreedyModelComparator.ComparisonOutput): Double {
        return vertexCost(output) + edgeCost(output)
    }

}
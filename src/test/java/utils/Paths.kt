package utils

import java.nio.file.Path
import java.nio.file.Paths

class Paths private constructor() {

    companion object {
        val startPath: Path = Paths.get("src", "test", "resources")
    }

}
<%@ tag description="Generic panel page" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ attribute name="header" fragment="true" %>
<%@ attribute name="stylesheets" fragment="true" %>
<%@ attribute name="scripts" fragment="true" %>
<%@ attribute name="container_class" required="true" %>

<t:genericpage container_class="${container_class}">
    <jsp:attribute name="stylesheets">
        <jsp:invoke fragment="stylesheets"/>
    </jsp:attribute>

    <jsp:attribute name="scripts">
        <jsp:invoke fragment="scripts"/>
    </jsp:attribute>

    <jsp:attribute name="header">
        <jsp:invoke fragment="header"/>
    </jsp:attribute>

    <jsp:body>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <jsp:doBody/>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:genericpage>
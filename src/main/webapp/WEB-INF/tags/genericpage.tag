<%@ tag description="Generic page" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ attribute name="header" fragment="true" %>
<%@ attribute name="stylesheets" fragment="true" %>
<%@ attribute name="scripts" fragment="true" %>
<%@ attribute name="container_class" required="true" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>BPMN Comparator by PAIS Lab</title>

        <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.css"/>"/>
        <link rel="stylesheet" href="<c:url value="/resources/css/font-awesome.css"/>"/>
        <link rel="stylesheet" href="<c:url value="/resources/css/custom-styles.css"/>"/>
        <jsp:invoke fragment="stylesheets" />
        <jsp:invoke fragment="scripts" />
    </head>

    <body>
        <div id="wrapper">
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a class="navbar-brand" href="<c:url value="/"/>">
                            BPMNDiffViz<b> BY PAIS LAB</b>
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="main-menu">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="<c:url value="/"/>">
                                    <i class="fa fa-dashboard"></i>
                                    Dashboard
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/comparison/first_step"/>">
                                    <i class="fa fa-link"></i>
                                    New comparison
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/uploadModel"/>">
                                    <i class="fa fa-cloud-upload"></i>
                                    New model
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/models"/>">
                                    <i class="fa fa-sitemap"></i>
                                    Models
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/results"/>">
                                    <i class="fa fa-tasks"></i>
                                    Results
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/settings"/>">
                                    <i class="fa fa-cogs"></i>
                                    Settings
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="${container_class}">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="page-header">
                            <jsp:invoke fragment="header" />
                        </h1>
                    </div>
                </div>

                <jsp:doBody />

                <div class="row">
                    <div class="col-md-12">
                        <footer>
                            All right reserved.
                            Laboratory of Process-Aware Information Systems (PAIS Lab).
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

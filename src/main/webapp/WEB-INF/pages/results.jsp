<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<jsp:useBean id="fmtBean" class="ru.pais.vkr.utils.FormaterBean"/>

<t:genericpanelpage container_class="container">
    <jsp:attribute name="stylesheets">
        <link rel="stylesheet" href="<c:url value="/resources/css/dataTables.bootstrap.css"/>"/>
    </jsp:attribute>

    <jsp:attribute name="scripts">
        <script src="<c:url value="/resources/js/bpmn/jquery.js"/>"></script>
        <script src="<c:url value="/resources/js/dataTables/jquery.dataTables.js"/>"></script>
        <script src="<c:url value="/resources/js/dataTables/dataTables.bootstrap.js"/>"></script>
        <script src="<c:url value="/resources/js/my/results.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="header">
        Results of comparisons
    </jsp:attribute>

    <jsp:body>
        <input name="firstModelID" id="firstModelID" type="hidden"/>
        <table class="table table-bordered" id="dataTable">
            <thead>
            <tr>
                <th>Date</th>
                <th>First model</th>
                <th>Second model</th>
                <th>Costs</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${results}" var="i">
                <tr>
                    <td><a href="<c:url value="/result?id=${i.id}"/>"><fmt:formatDate
                            value="${i.date}" pattern="yyyy-MM-dd HH:mm:ss"/></a></td>
                    <td><a href="<c:url value="/model?id=${i.firstID}"/>"
                           target="_blank">${i.firstName}</a></td>
                    <td><a href="<c:url value="/model?id=${i.secondID}"/>"
                           target="_blank">${i.secondName}</a></td>
                    <td>${fmtBean.format(i.penalty)}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </jsp:body>
</t:genericpanelpage>

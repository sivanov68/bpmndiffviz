<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage container_class="container">
    <jsp:attribute name="scripts">
        <script src="<c:url value="/resources/js/jquery/jquery-1.10.2.js"/>"></script>
        <script src="<c:url value="/resources/js/bootstrap/bootstrap.min.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="header">
        System failure
    </jsp:attribute>

    <jsp:body>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-body padding-top-5">
                                -------------------------------------------------------------------------------------------------------------------------
                                URL: ${url}
                                <br/>
                                Exception: ${exp}
                                <br/>
                                Message: ${errorMessage}
                                <br/>
                                -------------------------------------------------------------------------------------------------------------------------
                                <br/>
                                Please, contact <a href="mailto:syuivanov@gmail.com">syuivanov@gmail.com</a> and
                                send time, models, and list of actions
                                for diagnostic this error.
                                <br/>
                                Thanks, PAIS Team.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:genericpage>

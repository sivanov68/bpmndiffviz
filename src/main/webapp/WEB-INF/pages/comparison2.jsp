<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpanelpage container_class="container">
    <jsp:attribute name="stylesheets">
        <link rel="stylesheet" href="<c:url value="/resources/css/dataTables.bootstrap.css"/>"/>
    </jsp:attribute>

    <jsp:attribute name="scripts">
        <script src="<c:url value="/resources/js/bpmn/jquery.js"/>"></script>
        <script src="<c:url value="/resources/js/dataTables/jquery.dataTables.js"/>"></script>
        <script src="<c:url value="/resources/js/dataTables/dataTables.bootstrap.js"/>"></script>
        <script src="<c:url value="/resources/js/my/comparison2.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="header">
        Second step: Select second model
    </jsp:attribute>

    <jsp:body>
        <div class="alert alert-success">
            <strong>First model:</strong> <a href="<c:url value="/model?id=${firstModel.id}"/>" target="_blank">${firstModel.name}</a>
        </div>

        <form class="margin-bottom-0" action="<c:url value="/comparison/third_step"/>"
              method="post">
            <input name="firstModelID" id="firstModelID" type="hidden" value="${firstModel.id}"/>
            <input name="secondModelID" id="secondModelID" type="hidden"/>
            <table class="table table-bordered" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 14px;"></th>
                    <th>Model id</th>
                    <th>Model name</th>
                    <th>Upload date</th>
                    <th>Origin file name</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${modelList}" var="model">
                    <tr>
                        <td><input name="firstModelID" class="checkbox" type="checkbox"/></td>
                        <td>${model.id}</td>
                        <td><a href="<c:url value="/model?id=${model.id}"/>"
                               target="_blank">${model.name}</a></td>
                        <td>${model.document.uploadDate}</td>
                        <td>${model.document.name}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <a class="btn btn-danger btn-lg" href="<c:url value="/comparison/first_step"/>" style="width: 222px">
                Previous step
            </a>
            <button id="nextStep" class="btn btn-success btn-lg" style="width: 222px">
                Next step
            </button>
        </form>
    </jsp:body>
</t:genericpanelpage>

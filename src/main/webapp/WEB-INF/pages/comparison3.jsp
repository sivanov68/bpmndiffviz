<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpanelpage container_class="container">
    <jsp:attribute name="stylesheets">
        <link rel="stylesheet" href="<c:url value="/resources/css/dataTables.bootstrap.css"/>"/>
    </jsp:attribute>

    <jsp:attribute name="scripts">
        <script src="<c:url value="/resources/js/bpmn/jquery.js"/>"></script>
        <script src="<c:url value="/resources/js/bootstrap/bootstrap.min.js"/>"></script>
        <script src="<c:url value="/resources/js/jquery/jquery.form.min.js"/>"></script>
        <script src="<c:url value="/resources/js/my/comparison3.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="header">
        Third step: Cost of operations
    </jsp:attribute>

    <jsp:body>
        <div class="alert alert-success">
            <strong>First model:</strong>
            <a href="<c:url value="/model?id=${firstModel.id}"/>" target="_blank">
                    ${firstModel.name}
            </a>

            <br/>

            <strong>Second model:</strong>
            <a href="<c:url value="/model?id=${secondModel.id}"/>" target="_blank">
                    ${secondModel.name}
            </a>
        </div>

        <!-- previous step -->
        <form action="<c:url value="/comparison/second_step"/>" style="display: inline-block">

            <input name="firstModelID" type="hidden" value="${firstModel.id}"/>
            <input name="secondModelID" type="hidden" value="${secondModel.id}"/>

            <button class="btn btn-danger btn-lg" style="width: 222px">Previous step</button>

        </form>

        <!-- next step -->
        <form id="fourth_step" action="<c:url value="/comparison/fourth_step"/>" style="display: inline-block" method="post">

            <input name="firstModelID" type="hidden" value="${firstModel.id}"/>
            <input name="secondModelID" type="hidden" value="${secondModel.id}"/>

            <button id="nextStep" class="btn btn-success btn-lg" style="width: 222px">Final step</button>

        </form>

        <!-- algorithm selector -->
        <div class="form-group">
            <label for="algorithmSelector">Compare algorithm</label>
            <select id="algorithmSelector" name="algorithm" form="fourth_step" class="form-control">
                <c:forEach items="${modelComparators}" var="comparator" varStatus="loop">
                    <option value="${comparator}">${comparator.toString()}</option>
                </c:forEach>
            </select>
        </div>

        <!-- algorithm parameters -->
        <div class="panel-group">
            <c:forEach items="${modelComparatorParameters}" var="typeToParameters" varStatus="loop">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#collapse_param_${loop.index}">${typeToParameters.key}</a>
                        </h4>
                    </div>

                    <div id="collapse_param_${loop.index}" class="panel-collapse collapse">
                        <div class="panel-body">
                            <c:forEach items="${typeToParameters.value}" var="parameterToDefault">
                                <div class="row form-group">
                                    <label for="${"inputParam_".concat(typeToParameters.key).concat("_").concat(parameterToDefault.name)}" class="col-sm-3">${parameterToDefault.name}</label>
                                    <div class="col-sm-3">
                                        <input id="${"inputParam_".concat(typeToParameters.key).concat("_").concat(parameterToDefault.name)}"
                                               name="${"inputParam_".concat(typeToParameters.key).concat("_").concat(parameterToDefault.name)}"
                                               form="fourth_step"
                                               type="text"
                                               class="form-control"
                                               value="${parameterToDefault.defaultValue}">
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>

        <!-- levenshtein distance -->
        <div class="row padding-5 form-inline">
            <label for="Ldistance">
                <strong>Coefficient for label comparison (Levenshtein distance)</strong>
            </label>
            <input id="Ldistance" name="Ldistance" type="text"
                   class="form-control" form="fourth_step"
                   style="width:70px;" value="10.00">
        </div>

        <!-- graph elements matching parameters -->
        <div class="panel-group margin-bottom-0">
            <input id="formSave" type="hidden" value="<c:url value="/saveConfig"/>"/>

            <c:forEach items="${configs}" var="i" varStatus="loop">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion"
                               href="#collapse${loop.index}" class=""
                               aria-expanded="true">${i.comparedType.toString()}</a>
                        </h4>
                    </div>
                    <div id="collapse${loop.index}" class="panel-collapse collapse"
                         aria-expanded="true">
                        <div class="panel-body">
                            <form class="config margin-bottom-0 form-horizontal"
                                  action="<c:url value="/saveConfig"/>"
                                  method="post">
                                <input name="configType" type="hidden" value="${i.comparedType.toString()}">

                                <div class="form-group">
                                    <label class="col-sm-2 my-label ">Insert</label>

                                    <div class="col-sm-3">
                                        <input name="insert" type="text"
                                               class="form-control"
                                               value="${i.insert}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 my-label ">Delete</label>

                                    <div class="col-sm-3">
                                        <input name="delete" type="text"
                                               class="form-control"
                                               value="${i.delete}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 my-label ">Change label</label>

                                    <div class="col-sm-3">
                                        <input class="form-control"
                                               value="Levenshtein distance"
                                               disabled>
                                    </div>
                                </div>
                                <button class="btn btn-primary"
                                        style="width: 100px">Save
                                </button>

                                <h4 id="${i.comparedType.toString()}"
                                    style="display:inline-block"
                                    class="myConfig margin-bottom-0"><span
                                        class="label label-success">Successfully saved</span>
                                </h4>

                            </form>
                        </div>
                    </div>
                </div>
            </c:forEach>

        </div>
    </jsp:body>
</t:genericpanelpage>

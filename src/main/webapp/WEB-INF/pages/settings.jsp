<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpanelpage container_class="container">
    <jsp:attribute name="scripts">
        <script src="<c:url value="/resources/js/jquery/jquery-1.10.2.js"/>"></script>
        <script src="<c:url value="/resources/js/bootstrap/bootstrap.min.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="header">
        Settings
    </jsp:attribute>

    <jsp:body>
        <form action="<c:url value="/saveSettings"/>" method="post">
            <div class="form-group">
                <label for="models_path">Models path:</label>
                <input id="models_path"
                       name="models_path"
                       type="text"
                       class="form-control"
                       value="${models_path}">
            </div>

            <div class="form-check">
                <input id="use_parallelization"
                       name="use_parallelization"
                       type="checkbox"
                       class="form-check-input"
                       ${use_parallelization ? "checked" : ""}>
                <label class="form-check-label" for="use_parallelization">Use parallelization</label>
            </div>

            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </jsp:body>
</t:genericpanelpage>

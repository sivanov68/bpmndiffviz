<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage container_class="container">
    <jsp:attribute name="scripts">
        <script src="<c:url value="/resources/js/jquery/jquery-1.10.2.js"/>"></script>
        <script src="<c:url value="/resources/js/bootstrap/bootstrap.min.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="header">
        Dashboard
    </jsp:attribute>

    <jsp:body>
        <div class="row">
            <div class="col-sm-6">
                <div class="dashboard-statistics-panel panel green">
                    <div class="dashboard-statistics-icon">
                        <i class="fa fa-sitemap fa-5x"></i>
                    </div>
                    <div class="dashboard-statistics-text">
                        <h3>${number_of_models}</h3>
                        <strong>Number of models</strong>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="dashboard-statistics-panel panel brown">
                    <div class="dashboard-statistics-icon">
                        <i class="fa fa-tasks fa-5x"></i>
                    </div>
                    <div class="dashboard-statistics-text">
                        <h3>${number_of_comparisons}</h3>
                        <strong>Number of comparisons</strong>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Features in Version 1.0
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-body padding-top-5">
                                <span class="glyphicon glyphicon-ok color-green" aria-hidden="true"></span>
                                Full support BPMN 2.0 notation
                                <br/>
                                <span class="glyphicon glyphicon-ok color-green" aria-hidden="true"></span>
                                Importing BPMN 2.0 models
                                <br/>
                                <span class="glyphicon glyphicon-ok color-green" aria-hidden="true"></span> View
                                BPMN 2.0 models
                                <br/>
                                <span class="glyphicon glyphicon-ok color-green" aria-hidden="true"></span>
                                Comparison BPMN 2.0 models
                                <br/>
                                <span class="glyphicon glyphicon-ok color-green" aria-hidden="true"></span>
                                User-defined comparison settings
                                <br/>
                                <span class="glyphicon glyphicon-ok color-green" aria-hidden="true"></span>
                                Saving results of comparison BPMN 2.0 models
                                <br/>
                                <span class="glyphicon glyphicon-ok color-green" aria-hidden="true"></span>
                                Visualization of the comparison results
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:genericpage>

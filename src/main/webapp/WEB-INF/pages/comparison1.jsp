<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpanelpage container_class="container">
    <jsp:attribute name="stylesheets">
        <link rel="stylesheet" href="<c:url value="/resources/css/dataTables.bootstrap.css"/>"/>
    </jsp:attribute>

    <jsp:attribute name="scripts">
        <script src="<c:url value="/resources/js/bpmn/jquery.js"/>"></script>
        <script src="<c:url value="/resources/js/dataTables/jquery.dataTables.js"/>"></script>
        <script src="<c:url value="/resources/js/dataTables/dataTables.bootstrap.js"/>"></script>
        <script src="<c:url value="/resources/js/my/comparison1.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="header">
        First step: Select first model
    </jsp:attribute>

    <jsp:body>
        <form style="margin-bottom: 0" action="<c:url value="/comparison/second_step"/>"
              method="post">
            <input name="firstModelID" id="firstModelID" type="hidden"/>
            <table class="table table-bordered" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 14px;"></th>
                    <th>Model id</th>
                    <th>Model name</th>
                    <th>Upload date</th>
                    <th>Origin file name</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${modelList}" var="model">
                    <tr>
                        <td><input class="checkbox" type="checkbox"/></td>
                        <td>${model.id}</td>
                        <td><a href="<c:url value="/model?id=${model.id}"/>"
                               target="_blank">${model.name}</a></td>
                        <td>${model.document.uploadDate}</td>
                        <td>${model.document.name}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <button id="nextStep" class="btn btn-success btn-lg" style="width: 222px">Next step
            </button>
        </form>
    </jsp:body>
</t:genericpanelpage>

<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpanelpage container_class="container">
    <jsp:attribute name="stylesheets">
        <link rel="stylesheet" href="<c:url value="/resources/css/dataTables.bootstrap.css"/>"/>
    </jsp:attribute>

    <jsp:attribute name="scripts">
        <script src="<c:url value="/resources/js/bpmn/jquery.js"/>"></script>
        <script src="<c:url value="/resources/js/dataTables/jquery.dataTables.js"/>"></script>
        <script src="<c:url value="/resources/js/dataTables/dataTables.bootstrap.js"/>"></script>
        <script src="<c:url value="/resources/js/my/models.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="header">
        Uploaded models <small>BPMN 2.0 (.bpmn)</small>
    </jsp:attribute>

    <jsp:body>
        <table class="table table-bordered table-hover" id="dataTable">
            <thead>
            <tr>
                <th>Model id</th>
                <th>Model name</th>
                <th>Upload date</th>
                <th>Origin file name</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${modelList}" var="model">
                <tr>
                    <td>${model.id}</td>
                    <td><a href="<c:url value="/model?id=${model.id}"/>">${model.name}</a></td>
                    <td>${model.document.uploadDate}</td>
                    <td>${model.document.name}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </jsp:body>
</t:genericpanelpage>

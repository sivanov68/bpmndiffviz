<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage container_class="container">
    <jsp:attribute name="stylesheets">
        <link rel="stylesheet" href="<c:url value="/resources/css/morris-0.4.3.min.css"/>"/>
    </jsp:attribute>

    <jsp:attribute name="scripts">
        <script src="<c:url value="/resources/js/jquery/jquery-1.10.2.js"/>"></script>
        <script src="<c:url value="/resources/js/bootstrap/bootstrap.min.js"/>"></script>
        <script src="<c:url value="/resources/js/bpmn/bpmn-navigated-viewer.js"/>"></script>
        <script src="<c:url value="/resources/js/morris/morris.js"/>"></script>
        <script src="<c:url value="/resources/js/raphael/raphael-2.1.0.min.js"/>"></script>
        <script src="<c:url value="/resources/js/my/model.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="header">
        Model Viewer
    </jsp:attribute>

    <jsp:body>
        <div class="panel-body">
            <ul class="nav nav-tabs" id="nav">
                <li class="">
                    <a href="#Diagram" data-toggle="tab" aria-expanded="true">
                        Model diagram
                    </a>
                </li>
                <li class="active">
                    <a href="#Profile" data-toggle="tab" aria-expanded="true">
                        Model profile
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade" id="Diagram">
                    <div class="canvas" id="canvas">
                    </div>
                </div>
                <div class="tab-pane fade active in" id="Profile">
                    <div class="canvas">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="alert alert-info text-center" role="alert"><h3 class="margin-top-0">
                                    Information</h3></div>
                                <h3>Model name: <span style="font-weight: bold">${model.name}</span></h3>
                                <br/>

                                <h3>File name: <span style="font-weight: bold">${model.document.name}</span>
                                </h3>
                                <br/>

                                <h3>Uploaded date: <span style="font-weight: bold">${model.document.uploadDate}</span>
                                </h3>

                            </div>
                            <div class="col-md-8">
                                <div class="alert alert-info text-center" role="alert"><h3 class="margin-top-0">
                                    Statistics</h3></div>
                                <div class="text-center" id="morris-donut-chart"></div>
                                <div style="text-align: right"><span style="color: red">*</span> Only key elements
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="id" id="id" value="${model.id}">

        <input type="hidden" id="taskCount" value="${taskCount}">
        <input type="hidden" id="sequenceFlowCount" value="${sequenceFlowCount}">
        <input type="hidden" id="startEventCount" value="${startEventCount}">
        <input type="hidden" id="endEventCount" value="${endEventCount}">
        <input type="hidden" id="exclusiveGatewayCount" value="${exclusiveGatewayCount}">
        <input type="hidden" id="parallelGatewayCount" value="${parallelGatewayCount}">
        <input type="hidden" id="processCount" value="${processCount}">
        <input type="hidden" id="messageFlowCount" value="${messageFlowCount}">
    </jsp:body>
</t:genericpage>

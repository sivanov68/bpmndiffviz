<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpanelpage container_class="container">
    <jsp:attribute name="stylesheets">
        <link rel="stylesheet" href="<c:url value="/resources/css/formValidation.min.css"/>"/>
    </jsp:attribute>

    <jsp:attribute name="scripts">
        <script src="<c:url value="/resources/js/jquery/jquery-1.10.2.js"/>"></script>
        <script src="<c:url value="/resources/js/bootstrap/bootstrap.min.js"/>"></script>
        <script src="<c:url value="/resources/js/validation/formValidation.min.js"/>"></script>
        <script src="<c:url value="/resources/js/validation/bootstrap.min.js"/>"></script>
        <script src="<c:url value="/resources/js/bootstrap/bootstrap-filestyle.js"/>"></script>
        <script src="<c:url value="/resources/js/my/uploadModel.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="header">
        Upload model <small>BPMN 2.0 (.bpmn)</small>
    </jsp:attribute>

    <jsp:body>
        <form enctype="multipart/form-data"
              action="<c:url value="/saveModel"/>" method="post" id="uploadForm">
            <div class="form-group">
                <label>Model name</label>
                <input name="name" class="form-control" placeholder="BPMN 2.0 model">
            </div>
            <div class="form-group">
                <label>File input (.bpmn)</label>
                <input type="file" class="filestyle" data-buttonBefore="true" name="file"
                       accept=".bpmn">
            </div>
            <button id="uploadModelSubmit" type="submit" class="btn btn-default"><i
                    class="fa fa-plus-circle"></i>
                Submit
            </button>
        </form>

        <c:if test="${id ne null && id ne 0}">
            <div class="alert alert-success" style="margin-bottom: 0">
                    ${message} Open in viewer: <a href="<c:url value="/model?id=${id}"/>">${modelName}</a>
            </div>
        </c:if>

        <c:if test="${id eq null}">
            <div class="alert alert-danger" style="margin-bottom: 0">
                    ${message}
            </div>
        </c:if>
    </jsp:body>
</t:genericpanelpage>
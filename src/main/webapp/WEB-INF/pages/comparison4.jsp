<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<jsp:useBean id="fmtBean" class="ru.pais.vkr.utils.FormaterBean"/>

<t:genericpage container_class="container-fluid">
    <jsp:attribute name="stylesheets">
        <link rel="stylesheet" href="<c:url value="/resources/css/morris-0.4.3.min.css"/>"/>
    </jsp:attribute>

    <jsp:attribute name="scripts">
        <script src="<c:url value="/resources/js/jquery/jquery-1.10.2.js"/>"></script>
        <script src="<c:url value="/resources/js/bootstrap/bootstrap.min.js"/>"></script>
        <script src="<c:url value="/resources/js/bpmn/bpmn-navigated-viewer.js"/>"></script>
        <script src="<c:url value="/resources/js/morris/morris.js"/>"></script>
        <script src="<c:url value="/resources/js/raphael/raphael-2.1.0.min.js"/>"></script>
        <script src="<c:url value="/resources/js/charts/easypiechart.js"/>"></script>
        <script src="<c:url value="/resources/js/my/comparison4.js"/>"></script>
    </jsp:attribute>

    <jsp:attribute name="header">
        Final step: Comparison results
    </jsp:attribute>

    <jsp:body>
        <div class="panel-body">
            <ul class="nav nav-tabs" id="nav">
                <li class="active">
                    <a href="#Results" data-toggle="tab" aria-expanded="true">
                        Results
                    </a>
                </li>
                <li class="">
                    <a href="#Statistics" data-toggle="tab" aria-expanded="true">
                        Statistics
                    </a>
                </li>
                <li class="">
                    <a href="#Settings" data-toggle="tab" aria-expanded="true">
                        Settings
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade active in" id="Results">
                    <div class="row">
                        <div class="col-md-8 padding-right-0">
                            <div class="canvasParent">
                                <div class="canvasChild">
                                    Model: <span style="font-weight: bold">${firstModel.name}</span>

                                    <div class="modelCanvas" id="canvas1"></div>
                                    <br/>

                                    Model: <span style="font-weight: bold">${secondModel.name}</span>

                                    <div class="modelCanvas" id="canvas2"></div>
                                </div>
                            </div>
                            <div class="checkbox margin-bottom-0">
                                <label>
                                    <input id="showAllMarkers" type="checkbox"> Show all markers
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4 padding-left-0">
                            <div class="canvasParent">
                                Final score: <span style="font-weight: bold">
                                    ${fmtBean.format(result.penalty)}</span>
                                <c:forEach items="${result.deletedSet}" var="i">
                                    <div class="cursorPointer alert alert-danger margin-bottom-3 margin-top-3 padding-5">
                                        <span class="my-white-badge-danger">${fmtBean.format(i.differentLabel)}</span>
                                        Delete ${i.first.type}
                                        with ${i.first.name != null ? "name \"".concat(i.first.name).concat("\"") : "no name"}
                                        <input class="del" type="hidden" value="${i.first.id}">
                                    </div>
                                </c:forEach>
                                <c:forEach items="${result.addedSet}" var="i">
                                    <div class="cursorPointer alert alert-success margin-bottom-3 margin-top-3 padding-5">
                                        <span class="my-white-badge-success">${fmtBean.format(i.differentLabel)}</span>
                                        Add ${i.first.type}
                                        with ${i.first.name != null ? "name \"".concat(i.first.name).concat("\"") : "no name"}
                                        <input class="add" type="hidden" value="${i.first.id}">
                                    </div>
                                </c:forEach>
                                <c:forEach items="${result.matchingSet}" var="i">
                                    <div class="cursorPointer alert alert-info margin-bottom-3 margin-top-3 padding-5">
                                        <span class="my-white-badge-info">${fmtBean.format(i.differentLabel)}</span>
                                        Match ${i.first.type}
                                        with ${i.first.name != null ? "name \"".concat(i.first.name).concat("\"") : "no name"}
                                        with ${i.second.type} with
                                            ${i.second.name != null ? "name \"".concat(i.second.name).concat("\"") : "no name"}
                                        <input class="match1" type="hidden"
                                               value="${i.first.id}">
                                        <input class="match2" type="hidden"
                                               value="${i.second.id}">
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="Statistics">
                    <div class="canvas">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>
                                    Final score: <span style="font-weight: bold">${fmtBean.format(result.penalty)}</span>
                                </h4>

                                <c:forEach items="${result.statistics}" var="item">
                                    <h4>
                                            ${item.key}: <span style="font-weight: bold">${item.value}</span>
                                    </h4>
                                </c:forEach>
                            </div>

                            <div class="col-md-2 text-center">
                                <h4 style="text-align: center">Matched elements</h4>

                                <div class="easypiechart" style="text-align: center" id="easypiechart-teal"
                                     data-percent="${percent2}"><span
                                        class="percent">${percent2}%</span>
                                </div>

                                <h3><span
                                        style="font-weight: bold">${result.matchingSet.size()}</span>
                                </h3>
                            </div>

                            <div class="col-md-2 text-center">
                                <h4 style="text-align: center">Deleted elements</h4>

                                <div class="easypiechart" style="text-align: center" id="easypiechart-orange"
                                     data-percent="${percent3}"><span
                                        class="percent">${percent3}%</span>
                                </div>

                                <h3><span
                                        style="font-weight: bold">${result.deletedSet.size()}</span>
                                </h3>
                            </div>

                            <div class="col-md-2 text-center">
                                <h4 style="text-align: center">Added elements</h4>

                                <div class="easypiechart" style="text-align: center" id="easypiechart-red"
                                     data-percent="${percent4}"><span
                                        class="percent">${percent4}%</span>
                                </div>

                                <h3><span
                                        style="font-weight: bold">${result.addedSet.size()}</span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="Settings">
                    <div class="col-md-12 padding-left-0 padding-right-0">
                        <div class="canvasParent">

                            <div class="row form-inline margin-0 padding-bottom-5">
                                <strong>Coefficient for label
                                    comparison (Levenshtein distance)</strong>
                                <input name="Ldistance" type="text"
                                       class="form-control"
                                       style="width:70px;" value="${result.factorLevenshtein}" disabled>
                            </div>

                            <div class="panel-group margin-bottom-0">
                                <c:forEach items="${result.costConfig.getComparators()}" var="i" varStatus="loop">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapse${loop.index}" class=""
                                                   aria-expanded="true">${i.comparedType.toString()}</a>
                                            </h4>
                                        </div>
                                        <div id="collapse${loop.index}" class="panel-collapse collapse"
                                             aria-expanded="true">
                                            <div class="panel-body">
                                                <form class="config margin-bottom-0 form-horizontal">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 my-label ">Insert</label>

                                                        <div class="col-sm-3">
                                                            <input name="insert" type="text"
                                                                   class="form-control"
                                                                   value="${i.insert}" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 my-label ">Delete</label>

                                                        <div class="col-sm-3">
                                                            <input name="delete" type="text"
                                                                   class="form-control"
                                                                   value="${i.delete}" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 my-label ">Change label</label>

                                                        <div class="col-sm-3">
                                                            <input class="form-control"
                                                                   value="Levenshtein distance"
                                                                   disabled>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="id1" id="id1" value="${firstModel.id}">
        <input type="hidden" name="id1" id="id2" value="${secondModel.id}">
        <input type="hidden" name="root" id="root" value="${pageContext.request.contextPath}">
    </jsp:body>
</t:genericpage>

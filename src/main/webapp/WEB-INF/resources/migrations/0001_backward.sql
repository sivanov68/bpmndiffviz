CREATE TABLE activationcondition (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE activity (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE definitions (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE process (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE laneset (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE lane (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE subprocess (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE participant (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE artifact (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE erroreventdefinition (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE escalationeventdefinition (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE documentation (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE operation (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE resourceassignmentexpression (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE inputdataitem (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE correlationsubscription (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE conversationassociation (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE property (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE categoryvalue (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE import (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE callconversation (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE sendtask (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE resource (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE conversation (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE text (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE endevent (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE servicetask (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE timecycle (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE compensateeventdefinition (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE datainputassociation (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE dataoutputassociation (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE canceleventdefinition (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE resourceparameterbinding (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE dataobjectreference (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE task (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE usertask (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE conditionexpression (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE extensionelements (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE correlationpropertybinding (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE resourceparameter (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE complexbehaviordefinition (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE assignment (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE potentialowner (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE dataobject (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE timeduration (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE receivetask (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE correlationpropertyretrievalexpression (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE outputset (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE itemdefinition (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE monitoring (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE timedate (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE startevent (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE messageeventdefinition (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE multiinstanceloopcharacteristics (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE escalation (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE parallelgateway (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE conditionaleventdefinition (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE correlationkey (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE intermediatecatchevent (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE rendering (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE message (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE auditing (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE participantmultiplicity (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE iobinding (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE condition (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE signal (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE globalconversation (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE inclusivegateway (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE exclusivegateway (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE subconversation (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE error (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE businessruletask (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE eventbasedgateway (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE loopcardinality (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE interface (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE correlationproperty (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE outputdataitem (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE linkeventdefinition (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE script (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE signaleventdefinition (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE inputset (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE boundaryevent (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE callactivity (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE relationship (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE endpoint (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE completioncondition (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE terminateeventdefinition (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE intermediatethrowevent (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE datastate (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE extension (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE manualtask (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE complexgateway (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE textannotation (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE timereventdefinition (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE iospecification (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE participantassociation (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE scripttask (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE messageflowassociation (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE association (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE conversationlink (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE messageflow (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE sequenceflow (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE throwevent (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE resourcerole (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE performer (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE humanperformer (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE formalexpression (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE expression (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE dataoutput (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE datainput (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE callableelement (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE dataassociation (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);

CREATE TABLE collaboration (
    id        BIGSERIAL NOT NULL PRIMARY KEY,
    ins       DOUBLE PRECISION   NOT NULL,
    del       DOUBLE PRECISION   NOT NULL,
    diff     DOUBLE PRECISION   NOT NULL,
    result_id INTEGER REFERENCES result (id)
);
CREATE TABLE HistoryComparator (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    comparator_type INTEGER NOT NULL,
    insert_cost DOUBLE PRECISION NOT NULL,
    delete_cost DOUBLE PRECISION NOT NULL,
    result_id INTEGER REFERENCES result (id)
);
--Storage
CREATE TABLE storage (
  id           BIGSERIAL NOT NULL PRIMARY KEY,
  is_available BOOLEAN   NOT NULL,
  path         VARCHAR   NOT NULL
);

--Document
CREATE TABLE document (
  id          BIGSERIAL NOT NULL PRIMARY KEY,
  uploadDate  DATE      NOT NULL,
  name        VARCHAR   NOT NULL,
  system_name VARCHAR   NOT NULL,
  storage_id  INTEGER REFERENCES storage (id)
);

--Model
CREATE TABLE model (
  id          BIGSERIAL NOT NULL PRIMARY KEY,
  name        VARCHAR   NOT NULL,
  document_id INTEGER REFERENCES document (id)
);

--ComparisonResult
CREATE TABLE result (
  id          BIGSERIAL NOT NULL PRIMARY KEY,
  rdate       TIMESTAMP NOT NULL,
  rec_counter INTEGER   NOT NULL,
  penalty     DOUBLE PRECISION   NOT NULL,
  model_id_1  INTEGER REFERENCES model (id),
  model_id_2  INTEGER REFERENCES model (id),
  factor_levenshtein     DOUBLE PRECISION   NOT NULL
);

--MatchingPair matchingSet
CREATE TABLE matching_pair_match (
  id                  BIGSERIAL NOT NULL PRIMARY KEY,
  first               VARCHAR,
  second              VARCHAR,
  definition          VARCHAR   NOT NULL,
  different_container DOUBLE PRECISION   NOT NULL,
  different_label    DOUBLE PRECISION   NOT NULL,
  result_id           INTEGER REFERENCES result (id)
);

--MatchingPair addedSet
CREATE TABLE matching_pair_add (
  id                  BIGSERIAL NOT NULL PRIMARY KEY,
  first               VARCHAR,
  second              VARCHAR,
  definition          VARCHAR   NOT NULL,
  different_container DOUBLE PRECISION   NOT NULL,
  different_label    DOUBLE PRECISION   NOT NULL,
  result_id           INTEGER REFERENCES result (id)
);

--MatchingPair deletedSet
CREATE TABLE matching_pair_delete (
  id                  BIGSERIAL NOT NULL PRIMARY KEY,
  first               VARCHAR,
  second              VARCHAR,
  definition          VARCHAR   NOT NULL,
  different_container DOUBLE PRECISION   NOT NULL,
  different_label    DOUBLE PRECISION   NOT NULL,
  result_id           INTEGER REFERENCES result (id)
);

CREATE TABLE Definitions (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE PROCESS (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE LaneSet (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Lane (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE SubProcess (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Participant (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Artifact (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ErrorEventDefinition (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE EscalationEventDefinition (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Documentation (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Operation (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ResourceAssignmentExpression (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE InputDataItem (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE CorrelationSubscription (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ConversationAssociation (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE PROPERTY (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE CategoryValue (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Import (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE CallConversation (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE SendTask (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE RESOURCE (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE CONVERSATION (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE TEXT (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE EndEvent (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ServiceTask (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE TimeCycle (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ActivationCondition (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE CompensateEventDefinition (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE DataInputAssociation (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE DataOutputAssociation (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE CancelEventDefinition (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ResourceParameterBinding (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE DataObjectReference (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Task (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE UserTask (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ConditionExpression (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ExtensionElements (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE CorrelationPropertyBinding (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ResourceParameter (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ComplexBehaviorDefinition (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Assignment (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE PotentialOwner (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE DataObject (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE TimeDuration (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ReceiveTask (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE CorrelationPropertyRetrievalExpression (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE OutputSet (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ItemDefinition (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Monitoring (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE TimeDate (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE StartEvent (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE MessageEventDefinition (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE MultiInstanceLoopCharacteristics (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Escalation (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ParallelGateway (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ConditionalEventDefinition (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE CorrelationKey (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE IntermediateCatchEvent (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Rendering (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE MESSAGE (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Auditing (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ParticipantMultiplicity (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE IoBinding (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE CONDITION (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Signal (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE GlobalConversation (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE InclusiveGateway (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ExclusiveGateway (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE SubConversation (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ERROR (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE BusinessRuleTask (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE EventBasedGateway (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE LoopCardinality (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Interface (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE CorrelationProperty (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE OutputDataItem (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE LinkEventDefinition (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Script (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE SignalEventDefinition (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE InputSet (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE BoundaryEvent (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE CallActivity (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Relationship (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ENDPOINT (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE CompletionCondition (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE TerminateEventDefinition (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE IntermediateThrowEvent (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE DataState (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Extension (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ManualTask (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ComplexGateway (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE TextAnnotation (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE TimerEventDefinition (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE IoSpecification (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ParticipantAssociation (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ScriptTask (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE MessageFlowAssociation (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Association (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ConversationLink (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE MessageFlow (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE SequenceFlow (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Activity (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ThrowEvent (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE ResourceRole (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Performer (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE HumanPerformer (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE FormalExpression (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Expression (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE DataOutput (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE DataInput (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE CallableElement (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE DataAssociation (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);

CREATE TABLE Collaboration (
  id        BIGSERIAL NOT NULL PRIMARY KEY,
  ins       DOUBLE PRECISION   NOT NULL,
  del       DOUBLE PRECISION   NOT NULL,
  diff     DOUBLE PRECISION   NOT NULL,
  result_id INTEGER REFERENCES result (id)
);
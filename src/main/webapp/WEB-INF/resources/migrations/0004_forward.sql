CREATE TABLE matching_pair (
    id             BIGSERIAL NOT NULL PRIMARY KEY,
    pair_type      INTEGER NOT NULL,
    first_name     VARCHAR,
    second_name    VARCHAR,
    description    VARCHAR NOT NULL,
    operation_cost DOUBLE PRECISION NOT NULL,
    result_id 	   BIGINT REFERENCES result (id)
);

INSERT INTO matching_pair (pair_type, first_name, second_name, description, operation_cost, result_id)
SELECT 0, first, second, definition, different_label, result_id FROM matching_pair_delete;

INSERT INTO matching_pair (pair_type, first_name, second_name, description, operation_cost, result_id)
SELECT 1, first, second, definition, different_label, result_id FROM matching_pair_add;

INSERT INTO matching_pair (pair_type, first_name, second_name, description, operation_cost, result_id)
SELECT 2, first, second, definition, different_label, result_id FROM matching_pair_match;

DROP TABLE matching_pair_delete;
DROP TABLE matching_pair_add;
DROP TABLE matching_pair_match;
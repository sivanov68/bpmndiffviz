ALTER TABLE matching_pair_match
ADD COLUMN different_container DOUBLE PRECISION NOT NULL DEFAULT 0;

ALTER TABLE matching_pair_match 
ALTER COLUMN different_container 
DROP DEFAULT;

ALTER TABLE matching_pair_add
ADD COLUMN different_container DOUBLE PRECISION NOT NULL DEFAULT 0;

ALTER TABLE matching_pair_add 
ALTER COLUMN different_container 
DROP DEFAULT;

ALTER TABLE matching_pair_delete
ADD COLUMN different_container DOUBLE PRECISION NOT NULL DEFAULT 0;

ALTER TABLE matching_pair_delete 
ALTER COLUMN different_container 
DROP DEFAULT;
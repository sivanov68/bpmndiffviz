ALTER TABLE document
DROP COLUMN storage_id;

CREATE TABLE settings
(
  id BIGSERIAL NOT NULL PRIMARY KEY,
  database_version INTEGER NOT NULL,
  models_directory VARCHAR
);

INSERT INTO settings (database_version, models_directory)
SELECT 5, path FROM storage;

DROP TABLE storage;
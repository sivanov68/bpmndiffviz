ALTER TABLE matching_pair_match DROP COLUMN different_container;
ALTER TABLE matching_pair_add DROP COLUMN different_container;
ALTER TABLE matching_pair_delete DROP COLUMN different_container;
CREATE TABLE matching_pair_delete (
  id                  BIGSERIAL NOT NULL PRIMARY KEY,
  first               VARCHAR,
  second              VARCHAR,
  definition          VARCHAR   NOT NULL,
  different_label     DOUBLE PRECISION   NOT NULL,
  result_id           INTEGER REFERENCES result (id)
);

CREATE TABLE matching_pair_add (
  id                  BIGSERIAL NOT NULL PRIMARY KEY,
  first               VARCHAR,
  second              VARCHAR,
  definition          VARCHAR   NOT NULL,
  different_label     DOUBLE PRECISION   NOT NULL,
  result_id           INTEGER REFERENCES result (id)
);

CREATE TABLE matching_pair_match (
  id                  BIGSERIAL NOT NULL PRIMARY KEY,
  first               VARCHAR,
  second              VARCHAR,
  definition          VARCHAR   NOT NULL,
  different_label     DOUBLE PRECISION   NOT NULL,
  result_id           INTEGER REFERENCES result (id)
);

INSERT INTO matching_pair_delete (first, second, definition, different_label, result_id)
SELECT first_name, second_name, description, operation_cost, result_id FROM matching_pair WHERE pair_type = 0;

INSERT INTO matching_pair_add (first, second, definition, different_label, result_id)
SELECT first_name, second_name, description, operation_cost, result_id FROM matching_pair WHERE pair_type = 1;

INSERT INTO matching_pair_match (first, second, definition, different_label, result_id)
SELECT first_name, second_name, description, operation_cost, result_id FROM matching_pair WHERE pair_type = 2;

DROP TABLE matching_pair;
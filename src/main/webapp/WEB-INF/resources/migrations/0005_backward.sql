CREATE TABLE storage (
  id           BIGSERIAL NOT NULL PRIMARY KEY,
  is_available BOOLEAN   NOT NULL,
  path         VARCHAR   NOT NULL
);

INSERT INTO storage (id, is_available, path)
SELECT 1, true, models_directory FROM settings;

DROP TABLE settings;

ALTER TABLE document
ADD COLUMN storage_id INTEGER REFERENCES storage (id) DEFAULT 1;

ALTER TABLE document 
ALTER COLUMN storage_id 
DROP DEFAULT;
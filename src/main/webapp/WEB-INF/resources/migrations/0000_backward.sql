DROP TABLE Collaboration;
DROP TABLE DataAssociation;
DROP TABLE CallableElement;
DROP TABLE DataInput;
DROP TABLE DataOutput;
DROP TABLE Expression;
DROP TABLE FormalExpression;
DROP TABLE HumanPerformer;
DROP TABLE Performer;
DROP TABLE ResourceRole;
DROP TABLE ThrowEvent;
DROP TABLE Activity;
DROP TABLE SequenceFlow;
DROP TABLE MessageFlow;
DROP TABLE ConversationLink;
DROP TABLE Association;
DROP TABLE MessageFlowAssociation;
DROP TABLE ScriptTask;
DROP TABLE ParticipantAssociation;
DROP TABLE IoSpecification;
DROP TABLE TimerEventDefinition;
DROP TABLE TextAnnotation;
DROP TABLE ComplexGateway;
DROP TABLE ManualTask;
DROP TABLE Extension;
DROP TABLE DataState;
DROP TABLE IntermediateThrowEvent;
DROP TABLE TerminateEventDefinition;
DROP TABLE CompletionCondition;
DROP TABLE ENDPOINT;
DROP TABLE Relationship;
DROP TABLE CallActivity;
DROP TABLE BoundaryEvent;
DROP TABLE InputSet;
DROP TABLE SignalEventDefinition;
DROP TABLE Script;
DROP TABLE LinkEventDefinition;
DROP TABLE OutputDataItem;
DROP TABLE CorrelationProperty;
DROP TABLE Interface;
DROP TABLE LoopCardinality;
DROP TABLE EventBasedGateway;
DROP TABLE BusinessRuleTask;
DROP TABLE ERROR;
DROP TABLE SubConversation;
DROP TABLE ExclusiveGateway;
DROP TABLE InclusiveGateway;
DROP TABLE GlobalConversation;
DROP TABLE Signal;
DROP TABLE CONDITION;
DROP TABLE IoBinding;
DROP TABLE ParticipantMultiplicity;
DROP TABLE Auditing;
DROP TABLE MESSAGE;
DROP TABLE Rendering;
DROP TABLE IntermediateCatchEvent;
DROP TABLE CorrelationKey;
DROP TABLE ConditionalEventDefinition;
DROP TABLE ParallelGateway;
DROP TABLE Escalation;
DROP TABLE MultiInstanceLoopCharacteristics;
DROP TABLE MessageEventDefinition;
DROP TABLE StartEvent;
DROP TABLE TimeDate;
DROP TABLE Monitoring;
DROP TABLE ItemDefinition;
DROP TABLE OutputSet;
DROP TABLE CorrelationPropertyRetrievalExpression;
DROP TABLE ReceiveTask;
DROP TABLE TimeDuration;
DROP TABLE DataObject;
DROP TABLE PotentialOwner;
DROP TABLE Assignment;
DROP TABLE ComplexBehaviorDefinition;
DROP TABLE ResourceParameter;
DROP TABLE CorrelationPropertyBinding;
DROP TABLE ExtensionElements;
DROP TABLE ConditionExpression;
DROP TABLE UserTask;
DROP TABLE Task;
DROP TABLE DataObjectReference;
DROP TABLE ResourceParameterBinding;
DROP TABLE CancelEventDefinition;
DROP TABLE DataOutputAssociation;
DROP TABLE DataInputAssociation;
DROP TABLE CompensateEventDefinition;
DROP TABLE ActivationCondition;
DROP TABLE TimeCycle;
DROP TABLE ServiceTask;
DROP TABLE EndEvent;
DROP TABLE TEXT;
DROP TABLE CONVERSATION;
DROP TABLE RESOURCE;
DROP TABLE SendTask;
DROP TABLE CallConversation;
DROP TABLE Import;
DROP TABLE CategoryValue;
DROP TABLE PROPERTY;
DROP TABLE ConversationAssociation;
DROP TABLE CorrelationSubscription;
DROP TABLE InputDataItem;
DROP TABLE ResourceAssignmentExpression;
DROP TABLE Operation;
DROP TABLE Documentation;
DROP TABLE EscalationEventDefinition;
DROP TABLE ErrorEventDefinition;
DROP TABLE Artifact;
DROP TABLE Participant;
DROP TABLE SubProcess;
DROP TABLE Lane;
DROP TABLE LaneSet;
DROP TABLE PROCESS;
DROP TABLE Definitions;
DROP TABLE matching_pair_delete;
DROP TABLE matching_pair_add;
DROP TABLE matching_pair_match;
DROP TABLE result;
DROP TABLE model;
DROP TABLE document;
DROP TABLE storage;
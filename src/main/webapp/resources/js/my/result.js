$(document).ready(function () {

    var BpmnViewer = window.BpmnJS;

    // create viewer
    var bpmnViewer1 = new BpmnViewer({
        container: '#canvas1'
    });

    var bpmnViewer2 = new BpmnViewer({
        container: '#canvas2'
    });

    // import function
    function importXML1(xml) {

        // import diagram
        bpmnViewer1.importXML(xml, function (err) {

            var overlays = bpmnViewer1.get('overlays');

            //$('.match1').each(function () {
            //    try {
            //        var canvas = bpmnViewer1.get('canvas');
            //        canvas.addMarker($(this).val(), 'myClassMatch');
            //        canvas.addMarker($(this).val() + '_label', 'myClassMatch');
            //    } catch (e) {
            //
            //    }
            //});

            $('.del').each(function () {
                try {
                    var canvas = bpmnViewer1.get('canvas');
                    canvas.addMarker($(this).val(), 'myClassDelete');
                    canvas.addMarker($(this).val() + '_label', 'myClassDelete');
                } catch (e) {

                }
            });

            // attach an overlay to a node
            //overlays.add('sid-52EB1772-F36E-433E-8F5B-D5DFD26E6F26', 'note', {
            //position: {
            //    top: -5,
            //    left: -5
            //},
            //html: '<div style="width: 10px; background: fuchsia; color: white;">0</div>'
            //});
        });
    }

    // import function
    function importXML2(xml) {

        // import diagram
        bpmnViewer2.importXML(xml, function (err) {

                var overlays = bpmnViewer2.get('overlays');

                //$('.match2').each(function () {
                //    try {
                //        var canvas = bpmnViewer2.get('canvas');
                //        canvas.addMarker($(this).val(), 'myClassMatch');
                //        canvas.addMarker($(this).val() + '_label', 'myClassMatch');
                //    } catch (e) {
                //
                //    }
                //});

                $('.add').each(function () {
                    try {
                        var canvas = bpmnViewer2.get('canvas');
                        canvas.addMarker($(this).val(), 'myClassAdd');
                        canvas.addMarker($(this).val() + '_label', 'myClassAdd');
                    }
                    catch (e) {

                    }
                });

                // attach an overlay to a node
                //overlays.add('sid-52EB1772-F36E-433E-8F5B-D5DFD26E6F26', 'note', {
                //position: {
                //    top: -5,
                //    left: -5
                //},
                //html: '<div style="width: 10px; background: fuchsia; color: white;">0</div>'
                //});

            }
        )
        ;
    }

    // import bpmn from server
    $.get($("#root").val() + '/resources/model?id=' + $("#id1").val(), importXML1, 'text');
    $.get($("#root").val() + '/resources/model?id=' + $("#id2").val(), importXML2, 'text');

    $('.alert-danger').each(function () {
        $(this).css("background-color", "ebccd1");
        $(this).click(function () {
            if ($(this).css("background-color") == "rgb(242, 222, 222)") {
                $(this).css("background-color", "ebccd1");
                try {
                    var canvas1 = bpmnViewer1.get('canvas');
                    canvas1.addMarker($(this).find("input.del[type=hidden]").val(), 'myClassDelete');
                    canvas1.addMarker($(this).find("input.del[type=hidden]").val() + '_label', 'myClassDelete');
                } catch (e) {

                }
            } else {
                $(this).css("background-color", "rgb(242, 222, 222)");
                try {
                    var canvas1 = bpmnViewer1.get('canvas');
                    canvas1.removeMarker($(this).find("input.del[type=hidden]").val(), 'myClassDelete');
                    canvas1.removeMarker($(this).find("input.del[type=hidden]").val() + '_label', 'myClassDelete');
                } catch (e) {

                }
            }

            var flag = true;
            $('.alert-danger').each(function () {
                if ($(this).css("background-color") != "rgb(235, 204, 209)") {
                    flag = false;
                }
            });
            $('.alert-success').each(function () {
                if ($(this).css("background-color") != "rgb(214, 233, 198)") {
                    flag = false;
                }
            });
            $('.alert-info').each(function () {
                if ($(this).css("background-color") != "rgb(188, 232, 241)") {
                    flag = false;
                }
            });
            if (flag) {
                $('#showAllMarkers').prop('checked', true);
            } else {
                $('#showAllMarkers').prop('checked', false);
            }

            try {
                var element = $("g[data-element-id='" + $(this).find("input.del[type=hidden]").val() + "']:first");
                if (jQuery.inArray("djs-shape", element.attr("class").split(' ')) > 0) {
                    var transformElement = element.attr("transform").replace('matrix', '').replace('(', '').replace(')', '');
                    var arrayElement = transformElement.split(',');

                    var viewport = $("#canvas1").find("svg g.viewport");

                    var zoom = "1";
                    try {
                        zoom = viewport.attr("transform").replace('matrix', '').replace('(', '').replace(')', '').split(',')[0];
                    } catch (e) {

                    }

                    var elementSizeWidth = 0.0;
                    var elementSizeHeight = 0.0;
                    try {
                        elementSizeWidth = Number(element.find("rect:last-child").attr("width"));
                        elementSizeHeight = Number(element.find("rect:last-child").attr("height"));
                    } catch (e) {

                    }

                    arrayElement[0] = zoom;
                    arrayElement[3] = zoom;
                    var width = $("#canvas1").width();
                    arrayElement[4] = String(Number(zoom) * (Number(arrayElement[4]) * (-1) - elementSizeWidth / 2) + width / 2);
                    var height = $("#canvas1").height();
                    arrayElement[5] = String(Number(zoom) * (Number(arrayElement[5]) * (-1) - elementSizeHeight / 2) + height / 2);

                    viewport.attr("transform", "matrix(" + arrayElement + ")");
                } else {
                    var x = element.find("rect:first-child").attr("x");
                    var y = element.find("rect:first-child").attr("y");
                    var widthE = element.find("rect:first-child").attr("width");
                    var heightE = element.find("rect:first-child").attr("height");

                    var viewport = $("#canvas1").find("svg g.viewport");

                    var zoom = "1";
                    try {
                        zoom = viewport.attr("transform").replace('matrix', '').replace('(', '').replace(')', '').split(',')[0];
                    } catch (e) {

                    }

                    var width = $("#canvas1").width();
                    var xN = String(Number(zoom) * (Number(x) * (-1) - Number(widthE) / 2) + width / 2);
                    var height = $("#canvas1").height();
                    var yN = String(Number(zoom) * (Number(y) * (-1) - Number(heightE) / 2) + height / 2);

                    viewport.attr("transform", "matrix(" + zoom + ",0," + "0," + zoom + "," + xN + "," + yN + ")");
                }
            } catch (e) {

            }
        })
    });

    $('.alert-success').each(function () {
        $(this).css("background-color", "d6e9c6");
        $(this).click(function () {
            if ($(this).css("background-color") == "rgb(223, 240, 216)") {
                $(this).css("background-color", "d6e9c6");
                try {
                    var canvas2 = bpmnViewer2.get('canvas');
                    canvas2.addMarker($(this).find("input.add[type=hidden]").val(), 'myClassAdd');
                    canvas2.addMarker($(this).find("input.add[type=hidden]").val() + '_label', 'myClassAdd');
                } catch (e) {

                }
            } else {
                $(this).css("background-color", "rgb(223, 240, 216)");
                try {
                    var canvas2 = bpmnViewer2.get('canvas');
                    canvas2.removeMarker($(this).find("input.add[type=hidden]").val(), 'myClassAdd');
                    canvas2.removeMarker($(this).find("input.add[type=hidden]").val() + '_label', 'myClassAdd');
                } catch (e) {

                }
            }

            var flag = true;
            $('.alert-danger').each(function () {
                if ($(this).css("background-color") != "rgb(235, 204, 209)") {
                    flag = false;
                }
            });
            $('.alert-success').each(function () {
                if ($(this).css("background-color") != "rgb(214, 233, 198)") {
                    flag = false;
                }
            });
            $('.alert-info').each(function () {
                if ($(this).css("background-color") != "rgb(188, 232, 241)") {
                    flag = false;
                }
            });
            if (flag) {
                $('#showAllMarkers').prop('checked', true);
            } else {
                $('#showAllMarkers').prop('checked', false);
            }

            try {
                var element = $("g[data-element-id='" + $(this).find("input.add[type=hidden]").val() + "']:last");
                if (jQuery.inArray("djs-shape", element.attr("class").split(' ')) > 0) {
                    var transformElement = element.attr("transform").replace('matrix', '').replace('(', '').replace(')', '');
                    var arrayElement = transformElement.split(',');

                    var viewport = $("#canvas2").find("svg g.viewport");

                    var zoom = "1";
                    try {
                        zoom = viewport.attr("transform").replace('matrix', '').replace('(', '').replace(')', '').split(',')[0];
                    } catch (e) {

                    }

                    var elementSizeWidth = 0.0;
                    var elementSizeHeight = 0.0;
                    try {
                        elementSizeWidth = Number(element.find("rect:last-child").attr("width"));
                        elementSizeHeight = Number(element.find("rect:last-child").attr("height"));
                    } catch (e) {

                    }

                    arrayElement[0] = zoom;
                    arrayElement[3] = zoom;
                    var width = $("#canvas2").width();
                    arrayElement[4] = String(Number(zoom) * (Number(arrayElement[4]) * (-1) - elementSizeWidth / 2) + width / 2);
                    var height = $("#canvas2").height();
                    arrayElement[5] = String(Number(zoom) * (Number(arrayElement[5]) * (-1) - elementSizeHeight / 2) + height / 2);

                    viewport.attr("transform", "matrix(" + arrayElement + ")");
                } else {
                    var x = element.find("rect:first-child").attr("x");
                    var y = element.find("rect:first-child").attr("y");
                    var widthE = element.find("rect:first-child").attr("width");
                    var heightE = element.find("rect:first-child").attr("height");

                    var viewport = $("#canvas2").find("svg g.viewport");

                    var zoom = "1";
                    try {
                        zoom = viewport.attr("transform").replace('matrix', '').replace('(', '').replace(')', '').split(',')[0];
                    } catch (e) {

                    }

                    var width = $("#canvas2").width();
                    var xN = String(Number(zoom) * (Number(x) * (-1) - Number(widthE) / 2) + width / 2);
                    var height = $("#canvas2").height();
                    var yN = String(Number(zoom) * (Number(y) * (-1) - Number(heightE) / 2) + height / 2);

                    viewport.attr("transform", "matrix(" + zoom + ",0," + "0," + zoom + "," + xN + "," + yN + ")");
                }
            } catch (e) {

            }
        })
    });

    $('.alert-info').each(function () {
        //$(this).css("background-color", "bce8f1");
        $(this).click(function () {
            if ($(this).css("background-color") == "rgb(217, 237, 247)") {
                $(this).css("background-color", "bce8f1");
                var canvas1 = bpmnViewer1.get('canvas');
                var canvas2 = bpmnViewer2.get('canvas');
                try {
                    canvas1.addMarker($(this).find("input.match1[type=hidden]").val(), 'myClassMatch');
                    canvas1.addMarker($(this).find("input.match1[type=hidden]").val() + '_label', 'myClassMatch');
                } catch (e) {

                }
                try {
                    canvas2.addMarker($(this).find("input.match2[type=hidden]").val(), 'myClassMatch');
                    canvas2.addMarker($(this).find("input.match2[type=hidden]").val() + '_label', 'myClassMatch');
                } catch (e) {

                }
            } else {
                $(this).css("background-color", "rgb(217, 237, 247)");
                var canvas1 = bpmnViewer1.get('canvas');
                var canvas2 = bpmnViewer2.get('canvas');
                try {
                    canvas1.removeMarker($(this).find("input.match1[type=hidden]").val(), 'myClassMatch');
                    canvas1.removeMarker($(this).find("input.match1[type=hidden]").val() + '_label', 'myClassMatch');
                } catch (e) {
                }

                try {
                    canvas2.removeMarker($(this).find("input.match2[type=hidden]").val(), 'myClassMatch');
                    canvas2.removeMarker($(this).find("input.match2[type=hidden]").val() + '_label', 'myClassMatch');
                } catch (e) {
                }
            }

            var flag = true;
            $('.alert-danger').each(function () {
                if ($(this).css("background-color") != "rgb(235, 204, 209)") {
                    flag = false;
                }
            });
            $('.alert-success').each(function () {
                if ($(this).css("background-color") != "rgb(214, 233, 198)") {
                    flag = false;
                }
            });
            $('.alert-info').each(function () {
                if ($(this).css("background-color") != "rgb(188, 232, 241)") {
                    flag = false;
                }
            });
            if (flag) {
                $('#showAllMarkers').prop('checked', true);
            } else {
                $('#showAllMarkers').prop('checked', false);
            }

            try {
                var element = $("g[data-element-id='" + $(this).find("input.match1[type=hidden]").val() + "']:first");
                if (jQuery.inArray("djs-shape", element.attr("class").split(' ')) > 0) {
                    var transformElement = element.attr("transform").replace('matrix', '').replace('(', '').replace(')', '');
                    var arrayElement = transformElement.split(',');

                    var viewport = $("#canvas1").find("svg g.viewport");

                    var zoom = "1";
                    try {
                        zoom = viewport.attr("transform").replace('matrix', '').replace('(', '').replace(')', '').split(',')[0];
                    } catch (e) {

                    }

                    var elementSizeWidth = 0.0;
                    var elementSizeHeight = 0.0;
                    try {
                        elementSizeWidth = Number(element.find("rect:last-child").attr("width"));
                        elementSizeHeight = Number(element.find("rect:last-child").attr("height"));
                    } catch (e) {

                    }

                    arrayElement[0] = zoom;
                    arrayElement[3] = zoom;
                    var width = $("#canvas1").width();
                    arrayElement[4] = String(Number(zoom) * (Number(arrayElement[4]) * (-1) - elementSizeWidth / 2) + width / 2);
                    var height = $("#canvas1").height();
                    arrayElement[5] = String(Number(zoom) * (Number(arrayElement[5]) * (-1) - elementSizeHeight / 2) + height / 2);

                    viewport.attr("transform", "matrix(" + arrayElement + ")");
                } else {
                    var x = element.find("rect:first-child").attr("x");
                    var y = element.find("rect:first-child").attr("y");
                    var widthE = element.find("rect:first-child").attr("width");
                    var heightE = element.find("rect:first-child").attr("height");

                    var viewport = $("#canvas1").find("svg g.viewport");

                    var zoom = "1";
                    try {
                        zoom = viewport.attr("transform").replace('matrix', '').replace('(', '').replace(')', '').split(',')[0];
                    } catch (e) {

                    }

                    var width = $("#canvas1").width();
                    var xN = String(Number(zoom) * (Number(x) * (-1) - Number(widthE) / 2) + width / 2);
                    var height = $("#canvas1").height();
                    var yN = String(Number(zoom) * (Number(y) * (-1) - Number(heightE) / 2) + height / 2);

                    viewport.attr("transform", "matrix(" + zoom + ",0," + "0," + zoom + "," + xN + "," + yN + ")");
                }
            } catch (e) {

            }

            try {
                var element = $("g[data-element-id='" + $(this).find("input.match2[type=hidden]").val() + "']:last");
                if (jQuery.inArray("djs-shape", element.attr("class").split(' ')) > 0) {
                    var transformElement = element.attr("transform").replace('matrix', '').replace('(', '').replace(')', '');
                    var arrayElement = transformElement.split(',');

                    var viewport = $("#canvas2").find("svg g.viewport");

                    var zoom = "1";
                    try {
                        zoom = viewport.attr("transform").replace('matrix', '').replace('(', '').replace(')', '').split(',')[0];
                    } catch (e) {

                    }

                    var elementSizeWidth = 0.0;
                    var elementSizeHeight = 0.0;
                    try {
                        elementSizeWidth = Number(element.find("rect:last-child").attr("width"));
                        elementSizeHeight = Number(element.find("rect:last-child").attr("height"));
                    } catch (e) {

                    }

                    arrayElement[0] = zoom;
                    arrayElement[3] = zoom;
                    var width = $("#canvas2").width();
                    arrayElement[4] = String(Number(zoom) * (Number(arrayElement[4]) * (-1) - elementSizeWidth / 2) + width / 2);
                    var height = $("#canvas2").height();
                    arrayElement[5] = String(Number(zoom) * (Number(arrayElement[5]) * (-1) - elementSizeHeight / 2) + height / 2);

                    viewport.attr("transform", "matrix(" + arrayElement + ")");
                } else {
                    var x = element.find("rect:first-child").attr("x");
                    var y = element.find("rect:first-child").attr("y");
                    var widthE = element.find("rect:first-child").attr("width");
                    var heightE = element.find("rect:first-child").attr("height");

                    var viewport = $("#canvas2").find("svg g.viewport");

                    var zoom = "1";
                    try {
                        zoom = viewport.attr("transform").replace('matrix', '').replace('(', '').replace(')', '').split(',')[0];
                    } catch (e) {

                    }

                    var width = $("#canvas2").width();
                    var xN = String(Number(zoom) * (Number(x) * (-1) - Number(widthE) / 2) + width / 2);
                    var height = $("#canvas2").height();
                    var yN = String(Number(zoom) * (Number(y) * (-1) - Number(heightE) / 2) + height / 2);

                    viewport.attr("transform", "matrix(" + zoom + ",0," + "0," + zoom + "," + xN + "," + yN + ")");
                }
            } catch (e) {

            }
        })
    });

    //$('#showAllMarkers').prop('checked', true);
    $('#showAllMarkers').change(function () {
        if ($(this).is(':checked')) {

            $('.alert-info').each(function () {
                $(this).css("background-color", "bce8f1");
            });

            $('.alert-success').each(function () {
                $(this).css("background-color", "d6e9c6");
            });

            $('.alert-danger').each(function () {
                $(this).css("background-color", "ebccd1");
            });

            var canvas1 = bpmnViewer1.get('canvas');
            $('.match1').each(function () {
                try {
                    canvas1.addMarker($(this).val(), 'myClassMatch');
                    canvas1.addMarker($(this).val() + '_label', 'myClassMatch');
                } catch (e) {
                }
            });
            $('.del').each(function () {
                try {
                    canvas1.addMarker($(this).val(), 'myClassDelete');
                    canvas1.addMarker($(this).val() + '_label', 'myClassDelete');
                } catch (e) {
                }
            });

            var canvas2 = bpmnViewer2.get('canvas');
            $('.match2').each(function () {
                try {
                    canvas2.addMarker($(this).val(), 'myClassMatch');
                    canvas2.addMarker($(this).val() + '_label', 'myClassMatch');
                } catch (e) {
                }
            });

            $('.add').each(function () {
                try {
                    canvas2.addMarker($(this).val(), 'myClassAdd');
                    canvas2.addMarker($(this).val() + '_label', 'myClassAdd');
                }
                catch (e) {
                }
            });
        } else {

            $('.alert-info').each(function () {
                $(this).css("background-color", "rgb(217, 237, 247)");
            });

            $('.alert-success').each(function () {
                $(this).css("background-color", "rgb(223, 240, 216)");
            });

            $('.alert-danger').each(function () {
                $(this).css("background-color", "rgb(242, 222, 222)");
            });

            var canvas1 = bpmnViewer1.get('canvas');
            $('.match1').each(function () {
                try {
                    canvas1.removeMarker($(this).val(), 'myClassMatch');
                    canvas1.removeMarker($(this).val() + '_label', 'myClassMatch');
                } catch (e) {
                }
            });
            $('.del').each(function () {
                try {
                    canvas1.removeMarker($(this).val(), 'myClassDelete');
                    canvas1.removeMarker($(this).val() + '_label', 'myClassDelete');
                } catch (e) {
                }
            });

            var canvas2 = bpmnViewer2.get('canvas');
            $('.match2').each(function () {
                try {
                    canvas2.removeMarker($(this).val(), 'myClassMatch');
                    canvas2.removeMarker($(this).val() + '_label', 'myClassMatch');
                } catch (e) {
                }
            });

            $('.add').each(function () {
                try {
                    canvas2.removeMarker($(this).val(), 'myClassAdd');
                    canvas2.removeMarker($(this).val() + '_label', 'myClassAdd');
                }
                catch (e) {
                }
            });
        }
    });

    $('#easypiechart-teal').easyPieChart({
        scaleColor: false,
        barColor: '#1ebfae'
    });

    $('#easypiechart-orange').easyPieChart({
        scaleColor: false,
        barColor: '#ffb53e'
    });

    $('#easypiechart-red').easyPieChart({
        scaleColor: false,
        barColor: '#f9243f'
    });

    $('#easypiechart-blue').easyPieChart({
        scaleColor: false,
        barColor: '#30a5ff'
    });

})
;
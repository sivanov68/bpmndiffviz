package ru.pais.vkr.comparator.custom_comparators

import org.camunda.bpm.model.bpmn.BpmnModelInstance
import org.camunda.bpm.model.bpmn.instance.MessageFlow
import ru.pais.vkr.comparator.entities.BPMNComparator
import ru.pais.vkr.comparator.entities.BPMNElement
import ru.pais.vkr.comparator.entities.BPMNElementType
import ru.pais.vkr.comparator.entities.ComparisonResult
import ru.pais.vkr.comparator.entities.MatchingPair

class MessageFlowComparator : BPMNComparator(BPMNElementType.MessageFlow) {

    override fun compare(
        comparisonResult: ComparisonResult,
        firstBpmnModel: BpmnModelInstance,
        secondBpmnModel: BpmnModelInstance,
        firstElement: BPMNElement,
        secondElement: BPMNElement
    ): MatchingPair {
        val firstRaw = firstElement.raw as MessageFlow
        val secondRaw = secondElement.raw as MessageFlow

        return compareWithBorders(
            comparisonResult,
            firstElement,
            secondElement,
            firstRaw.source,
            firstRaw.target,
            secondRaw.source,
            secondRaw.target
        )
    }
}


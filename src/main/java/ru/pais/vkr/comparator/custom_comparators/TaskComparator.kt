package ru.pais.vkr.comparator.custom_comparators

import ru.pais.vkr.comparator.entities.BPMNComparator
import ru.pais.vkr.comparator.entities.BPMNElementType

class TaskComparator : BPMNComparator(10.0, 10.0, BPMNElementType.Task)

package ru.pais.vkr.comparator.custom_comparators

import org.camunda.bpm.model.bpmn.BpmnModelInstance
import org.camunda.bpm.model.bpmn.instance.ConversationLink
import ru.pais.vkr.comparator.entities.BPMNComparator
import ru.pais.vkr.comparator.entities.BPMNElement
import ru.pais.vkr.comparator.entities.BPMNElementType
import ru.pais.vkr.comparator.entities.ComparisonResult
import ru.pais.vkr.comparator.entities.MatchingPair

class ConversationLinkComparator : BPMNComparator(BPMNElementType.ConversationLink) {

    override fun compare(
        comparisonResult: ComparisonResult,
        firstBpmnModel: BpmnModelInstance,
        secondBpmnModel: BpmnModelInstance,
        firstElement: BPMNElement,
        secondElement: BPMNElement
    ): MatchingPair {
        val firstRaw = firstElement.raw as ConversationLink
        val secondRaw = secondElement.raw as ConversationLink

        return compareWithBorders(
            comparisonResult,
            firstElement,
            secondElement,
            firstRaw.source,
            firstRaw.target,
            secondRaw.source,
            secondRaw.target
        )
    }
}


package ru.pais.vkr.comparator.entities

enum class PairType {
    DELETE,
    INSERT,
    MATCH
}

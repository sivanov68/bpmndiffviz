package ru.pais.vkr.comparator.entities

import org.camunda.bpm.model.bpmn.instance.*
import org.camunda.bpm.model.xml.instance.ModelElementInstance
import ru.pais.vkr.services.BPMNElementToClassMatcher

open class BPMNElement {
    /**
     * Returns the raw bpmn object associated with current element
     */
    open val raw: ModelElementInstance

    /**
     * Returns current element type
     */
    open val type: BPMNElementType

    /**
     * Returns the name of the current object based on "getName()"
     * result or taken from the "name" attribute
     */
    open val name: String?

    /**
     * Returns id for the current element
     */
    open val id: String?

    @Suppress("ConvertSecondaryConstructorToPrimary")
    constructor(raw: ModelElementInstance) {
        this.raw = raw

        type = BPMNElementToClassMatcher.elementForClass(raw.javaClass)
        name = getNameForElement(raw)
        id = raw.getAttributeValue("id")
    }

    override fun equals(other: Any?): Boolean {
        return other is BPMNElement && raw === other.raw
    }

    override fun hashCode(): Int {
        return type.hashCode() xor (name?.hashCode() ?: 0) xor (id?.hashCode() ?: 0)
    }

    private fun getNameForElement(innerElement: ModelElementInstance): String? {
        return when (innerElement) {
            is FlowElement -> innerElement.name
            is ConversationNode -> innerElement.name
            is CallableElement -> innerElement.name
            is CorrelationKey -> innerElement.name
            is CorrelationProperty -> innerElement.name
            is DataInput -> innerElement.name
            is DataOutput -> innerElement.name
            is DataState -> innerElement.name
            is Definitions -> innerElement.name
            is Error -> innerElement.name
            is Escalation -> innerElement.name
            is Signal -> innerElement.name
            is Collaboration -> innerElement.name
            is ResourceRole -> innerElement.name
            is InputSet -> innerElement.name
            is Interface -> innerElement.name
            is Lane -> innerElement.name
            is LaneSet -> innerElement.name
            is LinkEventDefinition -> innerElement.name
            is Message -> innerElement.name
            is Operation -> innerElement.name
            is OutputSet -> innerElement.name
            is Participant -> innerElement.name
            is Property -> innerElement.name
            is Resource -> innerElement.name
            is ResourceParameter -> innerElement.name
            else -> innerElement.getAttributeValue("name")
        }
    }

    override fun toString(): String {
        return "$name: $type"
    }
}

package ru.pais.vkr.comparator.entities

import ru.pais.vkr.comparator.custom_comparators.*
import java.util.*
import kotlin.collections.ArrayList

class CostConfig {
    var factorLevenshtein = 1.0
    val comparators: List<BPMNComparator>

    private val elementToComparatorMap = EnumMap<BPMNElementType, BPMNComparator>(BPMNElementType::class.java)

    fun getDeleteCostFor(modelElementType: BPMNElementType): Double {
        return getValidComparator(modelElementType).delete
    }

    fun getInsertCostFor(modelElementType: BPMNElementType): Double {
        return getValidComparator(modelElementType).insert
    }

    fun setCosts(configType: String, delete: Double?, insert: Double?) {
        val elementType = BPMNElementType.valueOf(configType)
        val comparator = getValidComparator(elementType)

        delete?.let { comparator.delete = it }
        insert?.let { comparator.insert = it }
    }

    fun getValidComparator(element: BPMNElementType): BPMNComparator {
        return getComparatorOrNull(element)
            ?: throw IllegalArgumentException("Comparator for `$element` not found")
    }

    fun getComparatorOrNull(element: BPMNElementType): BPMNComparator? {
        return elementToComparatorMap[element]
    }

    init {
        // elements that are compared with the default costs and comparison function
        val compareWithDefaults = arrayOf(
            BPMNElementType.Activity,
            BPMNElementType.Artifact,
            BPMNElementType.Assignment,
            BPMNElementType.Auditing,
            BPMNElementType.BoundaryEvent,
            BPMNElementType.BusinessRuleTask,
            BPMNElementType.CallableElement,
            BPMNElementType.CallActivity,
            BPMNElementType.CallConversation,
            BPMNElementType.CancelEventDefinition,
            BPMNElementType.CategoryValue,
            BPMNElementType.Collaboration,
            BPMNElementType.CompensateEventDefinition,
            BPMNElementType.ComplexBehaviorDefinition,
            BPMNElementType.ComplexGateway,
            BPMNElementType.ConditionalEventDefinition,
            BPMNElementType.ConversationAssociation,
            BPMNElementType.Conversation,
            BPMNElementType.CorrelationKey,
            BPMNElementType.CorrelationPropertyBinding,
            BPMNElementType.CorrelationProperty,
            BPMNElementType.CorrelationPropertyRetrievalExpression,
            BPMNElementType.CorrelationSubscription,
            BPMNElementType.DataAssociation,
            BPMNElementType.DataInputAssociation,
            BPMNElementType.DataInput,
            BPMNElementType.DataObject,
            BPMNElementType.DataObjectReference,
            BPMNElementType.DataOutputAssociation,
            BPMNElementType.DataOutput,
            BPMNElementType.DataState,
            BPMNElementType.Definitions,
            BPMNElementType.Documentation,
            BPMNElementType.EndEvent,
            BPMNElementType.EndPoint,
            BPMNElementType.Error,
            BPMNElementType.ErrorEventDefinition,
            BPMNElementType.Escalation,
            BPMNElementType.EscalationEventDefinition,
            BPMNElementType.EventBasedGateway,
            BPMNElementType.ExclusiveGateway,
            BPMNElementType.Expression,
            BPMNElementType.Extension,
            BPMNElementType.ExtensionElements,
            BPMNElementType.FormalExpression,
            BPMNElementType.GlobalConversation,
            BPMNElementType.HumanPerformer,
            BPMNElementType.InclusiveGateway,
            BPMNElementType.InputSet,
            BPMNElementType.Interface,
            BPMNElementType.IntermediateCatchEvent,
            BPMNElementType.IntermediateThrowEvent,
            BPMNElementType.IoBinding,
            BPMNElementType.IoSpecification,
            BPMNElementType.ItemDefinition,
            BPMNElementType.Lane,
            BPMNElementType.LaneSet,
            BPMNElementType.LinkEventDefinition,
            BPMNElementType.ManualTask,
            BPMNElementType.Message,
            BPMNElementType.MessageEventDefinition,
            BPMNElementType.MessageFlowAssociation,
            BPMNElementType.Monitoring,
            BPMNElementType.MultiInstanceLoopCharacteristics,
            BPMNElementType.Operation,
            BPMNElementType.OutputSet,
            BPMNElementType.ParallelGateway,
            BPMNElementType.ParticipantAssociation,
            BPMNElementType.Participant,
            BPMNElementType.ParticipantMultiplicity,
            BPMNElementType.Performer,
            BPMNElementType.PotentialOwner,
            BPMNElementType.Process,
            BPMNElementType.Property,
            BPMNElementType.ReceiveTask,
            BPMNElementType.Relationship,
            BPMNElementType.Rendering,
            BPMNElementType.ResourceAssignmentExpression,
            BPMNElementType.Resource,
            BPMNElementType.ResourceParameterBinding,
            BPMNElementType.ResourceParameter,
            BPMNElementType.ResourceRole,
            BPMNElementType.Script,
            BPMNElementType.ScriptTask,
            BPMNElementType.SendTask,
            BPMNElementType.ServiceTask,
            BPMNElementType.Signal,
            BPMNElementType.SignalEventDefinition,
            BPMNElementType.StartEvent,
            BPMNElementType.SubConversation,
            BPMNElementType.SubProcess,
            BPMNElementType.TerminateEventDefinition,
            BPMNElementType.TextAnnotation,
            BPMNElementType.Text,
            BPMNElementType.ThrowEvent,
            BPMNElementType.TimerEventDefinition,
            BPMNElementType.UserTask
        )

        val inner = ArrayList<BPMNComparator>()
        // adding default comparators
        for (defaultComparisonType in compareWithDefaults) {
            inner.add(BPMNComparator(defaultComparisonType))
        }

        // adding custom comparators
        inner.addAll(
            listOf(
                AssociationComparator(),
                ConversationLinkComparator(),
                MessageFlowComparator(),
                SequenceFlowComparator(),
                TaskComparator()
            )
        )

        for (comparator in inner) {
            elementToComparatorMap[comparator.comparedType] = comparator
        }

        comparators = inner
    }
}

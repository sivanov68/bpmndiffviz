package ru.pais.vkr.comparator.entities

import java.util.logging.Level
import java.util.logging.Logger

open class MatchingPair {
    var first: BPMNElement? = null
    var second: BPMNElement? = null

    var firstID: String? = null
    var secondID: String? = null
    // todo: probably need to change this
    var definition: String
        get() = definitionInner ?: createDefinition().apply { definitionInner = this }
        set(value) { definitionInner = value }
    var type: PairType? = null

    var differentLabel: Double = 0.0

    var isLinkSave = true

    private var definitionInner: String? = null

    constructor()

    constructor(first: BPMNElement?, second: BPMNElement?, type: PairType?) {
        this.first = first
        this.second = second
        this.type = type
    }

    constructor(first: BPMNElement?, second: BPMNElement?) {
        this.first = first
        this.second = second
    }

    private fun createDefinition(): String {
        val firstTypeName: String? = first?.raw?.elementType?.typeName
        val firstName: String? = first?.name
        val formattedFirstName = if (firstName != null) "name \"$firstName\"" else "no name"

        return when (type) {
            PairType.DELETE -> "Delete $firstTypeName with $formattedFirstName"
            PairType.INSERT -> "Add $firstTypeName with $formattedFirstName"
            PairType.MATCH -> {
                val secondTypeName = second!!.raw.elementType.typeName
                val secondName = second!!.name
                val formattedSecondName = if (secondName != null) "name \"$secondName\"" else "no name"

                "Match " + firstTypeName +
                " with " + formattedFirstName +
                " with " + secondTypeName +
                " with " + formattedSecondName
            }
            else -> {
                Logger.getGlobal().log(
                    Level.SEVERE,
                    "Unknown type: \"$type\" falling back to delete"
                )

                "Delete $firstTypeName with $formattedFirstName"
            }
        }
    }
}

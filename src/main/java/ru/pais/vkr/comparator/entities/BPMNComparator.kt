package ru.pais.vkr.comparator.entities

import org.camunda.bpm.model.bpmn.BpmnModelInstance
import org.camunda.bpm.model.xml.instance.ModelElementInstance
import ru.pais.vkr.utils.StringUtil

open class BPMNComparator {
    val comparedType: BPMNElementType

    var insert: Double = 1.0
    var delete: Double = 1.0

    constructor(comparedType: BPMNElementType) {
        this.comparedType = comparedType
    }

    constructor(
        insert: Double,
        delete: Double,
        comparedType: BPMNElementType
    ): this(comparedType) {
        this.insert = insert
        this.delete = delete
    }

    open fun compare(
        comparisonResult: ComparisonResult,
        firstBpmnModel: BpmnModelInstance,
        secondBpmnModel: BpmnModelInstance,
        firstElement: BPMNElement,
        secondElement: BPMNElement
    ): MatchingPair {
        return computeWithLevenshtein(
            comparisonResult,
            firstElement,
            secondElement
        )
    }

    protected fun computeWithLevenshtein(
        comparisonResult: ComparisonResult,
        firstElement: BPMNElement,
        secondElement: BPMNElement
    ): MatchingPair {
        val differentLabel = StringUtil.computeLevenshteinDistance(firstElement.name ?: "", secondElement.name ?: "") * comparisonResult.factorLevenshtein
        val matchingPair = MatchingPair(
            firstElement,
            secondElement
        )

        matchingPair.differentLabel = differentLabel

        return matchingPair
    }

    protected fun compareWithBorders(
        comparisonResult: ComparisonResult,
        firstElement: BPMNElement,
        secondElement: BPMNElement,
        firstSource: ModelElementInstance,
        firstTarget: ModelElementInstance,
        secondSource: ModelElementInstance,
        secondTarget: ModelElementInstance
    ): MatchingPair {
        val alreadyMatchedFirstSource = comparisonResult.matchingSet.stream()
            .filter { it -> it.first!!.raw == firstSource }
            .map { it -> it.second!!.raw }
            .findFirst()

        val alreadyMatchedFirstTarget = comparisonResult.matchingSet.stream()
            .filter { it -> it.first!!.raw == firstTarget }
            .map { it -> it.second!!.raw }
            .findFirst()

        val pair = computeWithLevenshtein(
            comparisonResult,
            firstElement,
            secondElement
        )

        //Sources and targets are not equals
        if (!alreadyMatchedFirstSource.isPresent ||
            !alreadyMatchedFirstTarget.isPresent ||
            alreadyMatchedFirstSource.get() != secondSource ||
            alreadyMatchedFirstTarget.get() != secondTarget) {
            pair.isLinkSave = false
        }

        return pair
    }
}

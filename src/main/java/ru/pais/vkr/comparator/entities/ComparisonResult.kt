package ru.pais.vkr.comparator.entities

import org.camunda.bpm.model.bpmn.BpmnModelInstance
import ru.pais.vkr.utils.getElementsByType
import java.util.*

open class ComparisonResult : Comparable<ComparisonResult> {
    var id: Long? = null
    var date: Date? = null

    var firstBpmnModel: BpmnModelInstance? = null
    var secondBpmnModel: BpmnModelInstance? = null

    var firstID: Long? = null
    var secondID: Long? = null

    var firstName: String? = null
    var secondName: String? = null

    var costConfig: CostConfig? = null
    var heuristic: Int = 0
    var penalty: Double = 0.0

    val statistics: MutableMap<String, Any> = mutableMapOf()

    val comparingClassList = ArrayList<BPMNElementType>()
    val addedClassList = ArrayList<BPMNElementType>()

    var matchingSet: MutableSet<MatchingPair> = HashSet()
    var addedSet: MutableSet<MatchingPair> = HashSet()
    var deletedSet: MutableSet<MatchingPair> = HashSet()

    var factorLevenshtein = 10.0

    val keysFromDeletedSet: Set<BPMNElement?>
        get() = getKeysFromSet(deletedSet)

    val keysFromMatchingSet: Set<BPMNElement?>
        get() = getKeysFromSet(matchingSet)

    val keysFromAddedSet: Set<BPMNElement?>
        get() = getKeysFromSet(addedSet)

    val valuesFromDeletedSet: Set<BPMNElement?>
        get() = getValuesFromSet(deletedSet)

    val valuesFromMatchingSet: Set<BPMNElement?>
        get() = getValuesFromSet(matchingSet)

    val valuesFromAddedSet: Set<BPMNElement?>
        get() = getValuesFromSet(addedSet)

    constructor()

    constructor(firstBpmnModel: BpmnModelInstance, secondBpmnModel: BpmnModelInstance, costConfig: CostConfig) {
        this.costConfig = costConfig
        this.firstBpmnModel = firstBpmnModel
        this.secondBpmnModel = secondBpmnModel
        this.factorLevenshtein = costConfig.factorLevenshtein
    }

    constructor(comparisonResultOld: ComparisonResult, costConfig: CostConfig) {
        matchingSet.addAll(comparisonResultOld.matchingSet)
        addedSet.addAll(comparisonResultOld.addedSet)
        deletedSet.addAll(comparisonResultOld.deletedSet)
        comparingClassList.addAll(comparisonResultOld.comparingClassList)
        addedClassList.addAll(comparisonResultOld.addedClassList)
        penalty = comparisonResultOld.penalty
        this.costConfig = costConfig
        this.firstBpmnModel = comparisonResultOld.firstBpmnModel
        this.secondBpmnModel = comparisonResultOld.secondBpmnModel
        this.factorLevenshtein = costConfig.factorLevenshtein
    }

    constructor(
        firstBpmnModel: BpmnModelInstance,
        secondBpmnModel: BpmnModelInstance,
        typesInBothModels: List<BPMNElementType>,
        typesInAnyModel: List<BPMNElementType>,
        costConfig: CostConfig
    ) {
        comparingClassList.addAll(typesInBothModels)
        addedClassList.addAll(typesInAnyModel)
        this.costConfig = costConfig
        this.firstBpmnModel = firstBpmnModel
        this.secondBpmnModel = secondBpmnModel
        this.factorLevenshtein = costConfig.factorLevenshtein
    }

    override fun compareTo(other: ComparisonResult): Int {
        return (penalty + heuristic).compareTo(other.penalty + other.heuristic)
    }

    fun updateHeuristic() {
        heuristic = 0

        for (elementType in addedClassList) {
            var size1 = firstBpmnModel!!.getElementsByType(elementType).size
            var size2 = secondBpmnModel!!.getElementsByType(elementType).size

            val ready1 = getKeysFromSet(deletedSet).count { it?.type == elementType} + getKeysFromSet(matchingSet).count { it?.type == elementType }

            val ready2 = getValuesFromSet(matchingSet).count { it?.type == elementType } +
                // todo: здесь, видимо, по ошибке было получение не значений, а ключей; надо бы убедиться в правильности
                getValuesFromSet(addedSet).count { it?.type == elementType }

            size1 -= ready1
            size2 -= ready2
            var resideAdd = 0.0
            var resideDelete = 0.0
            if (size1 > size2) {
                resideDelete = (size1 - size2).toDouble()
                resideDelete *= costConfig!!.getDeleteCostFor(elementType)
            }
            if (size2 > size1) {
                resideAdd = (size2 - size1).toDouble()
                resideAdd *= costConfig!!.getInsertCostFor(elementType)
            }
            heuristic += (resideAdd + resideDelete).toInt()
        }
    }

    private fun getKeysFromSet(matchingPairs: Set<MatchingPair>): Set<BPMNElement?> {
        return matchingPairs.asSequence().map { it.first }.toSet()
    }

    private fun getValuesFromSet(matchingPairs: Set<MatchingPair>): Set<BPMNElement?> {
        return matchingPairs.asSequence().map { it.second }.toSet()
    }

    fun updateMatching(matchingPair: MatchingPair) {
        penalty += matchingPair.differentLabel
        matchingSet.add(matchingPair)
        updateHeuristic()
    }

    fun updateDeleting(matchingPair: MatchingPair) {
        penalty += matchingPair.differentLabel
        deletedSet.add(matchingPair)
        updateHeuristic()
    }

    fun updateAdding(matchingPair: MatchingPair) {
        penalty += matchingPair.differentLabel
        addedSet.add(matchingPair)
        updateHeuristic()
    }

    fun containsInKeysDeletedOrKeysAddedOrValuesMatchingSets(bpmnElement: BPMNElement): Boolean {
        val rawItem = bpmnElement.raw

        return matchingSet.any { it.second!!.raw == rawItem } ||
            deletedSet.any { it.first!!.raw == rawItem } ||
            addedSet.any { it.first!!.raw == rawItem }
    }

    fun containsInKeysDeletedOrKeysAddedOrKeysMatchingSets(bpmnElement: BPMNElement): Boolean {
        val rawItem = bpmnElement.raw

        return matchingSet.any { it.first!!.raw == rawItem } ||
            deletedSet.any { it.first!!.raw == rawItem } ||
            addedSet.any { it.first!!.raw == rawItem }
    }
}

package ru.pais.vkr.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.pais.vkr.dao.DocumentDAO
import ru.pais.vkr.dao.ModelDAO
import ru.pais.vkr.models.Model

@Service
class ModelService @Autowired constructor(
    private val modelDAO: ModelDAO,
    private val documentDAO: DocumentDAO
) {

    val allModels: List<Model>
        get() = modelDAO.allModels

    fun saveModel(model: Model) {
        modelDAO.saveModel(model)
    }

    fun saveOrUpdateModel(model: Model) {
        modelDAO.saveOrUpdateModel(model)
    }

    fun updateModel(model: Model) {
        modelDAO.updateModel(model)
    }

    fun getModelByID(id: Long?): Model {
        return modelDAO.getModelByID(id)
    }
}

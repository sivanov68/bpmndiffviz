package ru.pais.vkr.services.model_comparators

enum class ModelComparatorType {
    Greedy,
    TabuSearch,
    Genetic,
    AStar,
    Ants,
    SimulatedAnnealing
}
package ru.pais.vkr.services.model_comparators.genetic;

/**
 * Just pair for Gene.
 */
public class Offspring {

    Gene son, daughter;
    public Offspring(Gene son, Gene daughter) {
        this.son = son;
        this.daughter = daughter;
    }
}

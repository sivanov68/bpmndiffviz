package ru.pais.vkr.services.model_comparators.genetic;

import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.jetbrains.annotations.NotNull;
import ru.pais.vkr.comparator.entities.BPMNElement;
import ru.pais.vkr.comparator.entities.BPMNElementType;
import ru.pais.vkr.comparator.entities.ComparisonResult;
import ru.pais.vkr.comparator.entities.CostConfig;
import ru.pais.vkr.services.model_comparators.ModelComparator;
import ru.pais.vkr.services.model_comparators.ModelComparatorType;
import ru.pais.vkr.utils.BPMNUtils;

import java.util.*;

public class GeneticModelComparator extends ModelComparator {

    public static class Parameters {
        public static final Parameter<Double> POPULATION_FACTOR = new Parameter<>("Population factor", 0.9);

        private Parameters() {}
    }

    @NotNull
    @Override
    public ModelComparatorType getType() {
        return ModelComparatorType.Genetic;
    }

    @NotNull
    @Override
    public List<ParameterRaw> getParameters() {
        ArrayList<ParameterRaw> parameters = new ArrayList<>();
        parameters.add(Parameters.POPULATION_FACTOR);
        return parameters;
    }

    @NotNull
    @Override
    public ComparisonResult compareModels(
        @NotNull BpmnModelInstance firstBpmnModel,
        @NotNull BpmnModelInstance secondBpmnModel,
        @NotNull CostConfig costConfig,
        @NotNull Map<String, ?> inputParameters,
        boolean useParallelization
    ) {
        double populationFactor = Parameters.POPULATION_FACTOR.valueFrom(inputParameters);
        List<BPMNElementType> modelMatchingElementTypes = new ArrayList<>();
        List<BPMNElementType> modelElementTypes = new ArrayList<>();

        generateModelElementTypes(
            firstBpmnModel,
            secondBpmnModel,
            modelMatchingElementTypes,
            modelElementTypes
        );

        PriorityQueue<Gene> prevGeneration = new PriorityQueue<>();
        Gene baseGene = new Gene(firstBpmnModel, secondBpmnModel, costConfig, modelMatchingElementTypes, modelElementTypes);
        prevGeneration.add(baseGene);
        //Compare elements
        for (BPMNElementType aClass : modelMatchingElementTypes) {
            if(isFlowElement(aClass))
                continue;
            Collection<BPMNElement> modelElementInstances1 = getModelElementsByType(firstBpmnModel, aClass);
            Collection<BPMNElement> modelElementInstances2 = getModelElementsByType(secondBpmnModel, aClass);
            for (BPMNElement modelElementInstance1 : modelElementInstances1) {
                for (BPMNElement modelElementInstance2 : modelElementInstances2) {
                    Gene gene = new Gene(baseGene, modelElementInstance1, modelElementInstance2);
                    if(!gene.isEmpty()){
                        prevGeneration.add(gene);
                    }
                }
            }
        }
        PriorityQueue<Gene> nextGeneration = new PriorityQueue<>();
        Gene ans = null;
        int size = prevGeneration.size()/2;
        List<Gene> dads = new ArrayList<>();
        List<Gene> moms = new ArrayList<>();
        while (prevGeneration.size() > 0){
            ans = prevGeneration.peek();
            for(int i = 0; i < size; ++i)
                dads.add(prevGeneration.poll());
            for(int i = 0; i < size; ++i)
                moms.add(prevGeneration.poll());
            for(int j = 0; j < size; ++j){
                Gene dad = dads.get(j);
                Gene mom = moms.get(j);
                Offspring offspring = Gene.crossingover(mom, dad);
                nextGeneration.add(offspring.son);
                nextGeneration.add(offspring.daughter);
            }
            dads.clear();
            moms.clear();
            prevGeneration.clear();
            prevGeneration.addAll(nextGeneration);
            size = (int) (prevGeneration.size() * populationFactor)/2;
            nextGeneration.clear();
        }
        return ans;
    }

    private static List<BPMNElementType> BPMN_COMPARING_TYPES = new ArrayList<BPMNElementType>() {
        {
            //Containers
            add(BPMNElementType.Process);
            add(BPMNElementType.Lane);
            add(BPMNElementType.SubProcess);
            add(BPMNElementType.Participant);
            add(BPMNElementType.Task);
            add(BPMNElementType.ExclusiveGateway);
            add(BPMNElementType.ParallelGateway);
            add(BPMNElementType.InclusiveGateway);
            add(BPMNElementType.EventBasedGateway);
            add(BPMNElementType.ComplexGateway);
            add(BPMNElementType.EndEvent);
            add(BPMNElementType.StartEvent);
            //With source and target elements
            add(BPMNElementType.Association);
            add(BPMNElementType.ConversationLink);
            add(BPMNElementType.MessageFlow);
            add(BPMNElementType.SequenceFlow);
        }
    };

    public static Collection<BPMNElement> getAllModelElements(BpmnModelInstance bpmnModelInstance){
        Collection<BPMNElement> elementInstances = new ArrayList<>();
        Collection<BPMNElement> aClassElements;
        for (BPMNElementType aClass : BPMN_COMPARING_TYPES) {
            aClassElements = getModelElementsByType(bpmnModelInstance, aClass);
            if (!aClassElements.isEmpty())
                elementInstances.addAll(aClassElements);
        }
        return elementInstances;
    }

    public static boolean isFlowElement(BPMNElementType elementType) {
        return elementType == BPMNElementType.Association ||
            elementType == BPMNElementType.ConversationLink ||
            elementType == BPMNElementType.MessageFlow ||
            elementType == BPMNElementType.SequenceFlow;
    }

    private void generateModelElementTypes(
        BpmnModelInstance bpmnModelInstance1,
        BpmnModelInstance bpmnModelInstance2,
        List<BPMNElementType> modelMatchingElementTypes,
        List<BPMNElementType> modelElementTypes
    ) {
        for (BPMNElementType aClass : BPMN_COMPARING_TYPES) {
            Collection<BPMNElement> aClassElements1 = getModelElementsByType(bpmnModelInstance1, aClass);
            Collection<BPMNElement> aClassElements2 = getModelElementsByType(bpmnModelInstance2, aClass);

            if (!aClassElements1.isEmpty() && !aClassElements2.isEmpty())
                modelMatchingElementTypes.add(aClass);

            if (!aClassElements1.isEmpty() || !aClassElements2.isEmpty())
                modelElementTypes.add(aClass);
        }
    }

    private static Collection<BPMNElement> getModelElementsByType(BpmnModelInstance bpmnModelInstance, BPMNElementType aClass) {
        return BPMNUtils.getElementsByType(bpmnModelInstance, aClass);
    }
}
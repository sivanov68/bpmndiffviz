package ru.pais.vkr.services.model_comparators.tabu_search

import org.camunda.bpm.model.bpmn.BpmnModelInstance
import ru.pais.vkr.comparator.entities.ComparisonResult
import ru.pais.vkr.comparator.entities.CostConfig
import ru.pais.vkr.comparison_changes.EdgeChange
import ru.pais.vkr.comparison_changes.VertexChange
import ru.pais.vkr.graph_elements.Edge
import ru.pais.vkr.graph_elements.Graph
import ru.pais.vkr.services.model_comparators.ModelComparator
import ru.pais.vkr.services.model_comparators.ModelComparatorType
import ru.pais.vkr.services.model_comparators.greedy.GreedyModelComparator
import ru.pais.vkr.utils.GraphUtils
import ru.pais.vkr.utils.ModelComparatorUtils
import ru.pais.vkr.utils.assertCustom
import ru.pais.vkr.utils.toGraph
import kotlin.collections.ArrayList

object TabuSearchModelComparator : ModelComparator() {
    data class ComparisonOutput(
        val vertices: List<VertexChange>,
        val edges: List<EdgeChange>,
        val expansionsPerformed: Int,
        val greedyTotalTimeMs: Int,
        val expansionAverageTimeMs: Int,
        val expansionsTotalTimeMs: Int,
        val algorithmTotalTimeMs: Int
    )

    @Suppress("RemoveExplicitTypeArguments")
    object Parameters {
        val MAX_EXPANSIONS = Parameter<Int>("Maximum expansions", 100)
        val TABU_LIST_SIZE = Parameter<Int>("Tabu list size", 100)
        val USE_GREEDY_TYPE_PRIORITY = Parameter<Boolean>("Greedy: ${GreedyModelComparator.Parameters.USE_TYPE_PRIORITY.name}", false)
        val USE_GREEDY_ALREADY_MATCHED_PRIORITY = Parameter<Boolean>("Greedy: ${GreedyModelComparator.Parameters.USE_ALREADY_MATCHED_PRIORITY.name}", false)
    }

    object Statistics {
        val SOURCE_GRAPH_VERTICES = CommonStatistics.SOURCE_GRAPH_VERTICES
        val TARGET_GRAPH_VERTICES = CommonStatistics.TARGET_GRAPH_VERTICES
        val SOURCE_GRAPH_EDGES = CommonStatistics.SOURCE_GRAPH_EDGES
        val TARGET_GRAPH_EDGES = CommonStatistics.TARGET_GRAPH_EDGES
        val GREEDY_TOTAL_TIME_MS = CommonStatistics.GREEDY_TOTAL_TIME_MS
        val EXPANSIONS_COUNT = Statistic<Int>("Expansions performed")
        val EXPANSION_AVERAGE_TIME_MS = Statistic<Int>("Expansion average time (ms)")
        val EXPANSIONS_TOTAL_TIME_MS = Statistic<Int>("Expansions total time (ms)")
        val ALGORITHM_TOTAL_TIME_MS = CommonStatistics.TOTAL_TIME_MILLIS
    }

    override val type: ModelComparatorType = ModelComparatorType.TabuSearch

    override val parameters: List<ParameterRaw> = parametersFrom(Parameters)

    override fun compareModels(
        firstBpmnModel: BpmnModelInstance,
        secondBpmnModel: BpmnModelInstance,
        costConfig: CostConfig,
        inputParameters: Map<String, Any>,
        useParallelization: Boolean
    ): ComparisonResult {
        val sourceGraph: Graph = firstBpmnModel.toGraph()
        val targetGraph: Graph = secondBpmnModel.toGraph()

        val sourceVerticesCount: Int = sourceGraph.vertices.size
        val sourceEdgesCount: Int = sourceGraph.edges.size
        val targetVerticesCount: Int = targetGraph.vertices.size
        val targetEdgesCount: Int = targetGraph.edges.size

        val result: ComparisonOutput = compareNew(
            sourceGraph = sourceGraph,
            targetGraph = targetGraph,
            costConfig = costConfig,
            maxExpansionsCount = Parameters.MAX_EXPANSIONS.valueFrom(inputParameters),
            tabuListSize = Parameters.TABU_LIST_SIZE.valueFrom(inputParameters),
            useGreedyTypePriority = Parameters.USE_GREEDY_TYPE_PRIORITY.valueFrom(inputParameters),
            useGreedyAlreadyMatchedPriority = Parameters.USE_GREEDY_ALREADY_MATCHED_PRIORITY.valueFrom(inputParameters)
        )

        return ModelComparatorUtils.constructComparisonResult(
            firstBpmnModel,
            secondBpmnModel,
            result.vertices,
            result.edges,
            costConfig
        ).apply {
            with (Statistics) {
                SOURCE_GRAPH_VERTICES.fill(statistics, sourceVerticesCount)
                SOURCE_GRAPH_EDGES.fill(statistics, sourceEdgesCount)
                TARGET_GRAPH_VERTICES.fill(statistics, targetVerticesCount)
                TARGET_GRAPH_EDGES.fill(statistics, targetEdgesCount)
                GREEDY_TOTAL_TIME_MS.fill(statistics, result.greedyTotalTimeMs)
                EXPANSIONS_COUNT.fill(statistics, result.expansionsPerformed)
                EXPANSION_AVERAGE_TIME_MS.fill(statistics, result.expansionAverageTimeMs)
                EXPANSIONS_TOTAL_TIME_MS.fill(statistics, result.expansionsTotalTimeMs)
                ALGORITHM_TOTAL_TIME_MS.fill(statistics, result.algorithmTotalTimeMs)
            }
        }
    }

    /**
     * Алгоритм:
     *
     * 1. Находим решение жадным алгоритмом.
     * 2. Решение представляем в виде сопоставления всех вершин - называем это "состоянием".
     * 3. Пишем отдельную функцию для нахождения стоимости состояния. Для этого находим сумму всех замен вершин и сумму замен всех рёбер (минимальная сумма замен рёбер находится однозначно по сопоставленным вершинам)
     * 4. Создаём список запретов, элементами которого являются состояния
     * 5. Находим все возможные состояния, которые можно получить из текущего, проведя одну из операций:
     *      1. сопоставить удаляемую вершину с добавляемой: (v1, e), (e, v2') -> (v1, v2')
 *          2. сопоставить удаляемую вершину с сопоставленной, а исходную из сопоставленной удалить: (v1, e), (v2, v2') -> (v1, v2'), (v2, e)
     *      3. сопоставить сопоставленную вершину с новой, а заменяемую из исходной добавлять как новую: 3: (v1, v1'), (e, v2') -> (v1, v2'), (e, v2)
     *      4. сопоставить сопоставленную вершину с сопоставленной, заменяемую из исходной добавить как новую а заменяемую из новой удалить: (v1, v1'), (v2, v2') -> (v1, v2'), (v2, v1')
     *      5. удалить исходную вершину из сопоставленной, а конечную из неё же добавить: (v, v') -> (v1, e), (e, v')
     *      6. удалить исходную вершину из сопоставленной, заменяемую из сопоставленной добавить, а исходную из сопоставленной сопоставить с заменяемой из исходной: (v1, v1'), (v2, v2') -> (v1, e), (v2, v1'), (e, v2')
     * 6. Находим из этих состояний состояние с минимальной стоимостью, не входящее в список запретов, добавляем его в список запретов и переходим в него
     * Повторяем пункты 5-6 до тех пор, пока у нас все состояния не добавятся в список запретов или не будет превышено количество раскрытий вершин, что по сути является количеством выполнений пунктов 5-6
     */

    fun compareNew(
        sourceGraph: Graph,
        targetGraph: Graph,
        costConfig: CostConfig,
        maxExpansionsCount: Int,
        tabuListSize: Int,
        useGreedyTypePriority: Boolean,
        useGreedyAlreadyMatchedPriority: Boolean
    ): ComparisonOutput {
        if (maxExpansionsCount < 0)
            throw IllegalArgumentException("`maxExpansionsCount` must be non negative")
        if (tabuListSize < 0)
            throw IllegalArgumentException("`tabuListSize` must be non negative")

        val tabuList: MutableList<Set<VertexChange>> = mutableListOf()
        val tabuListSet: MutableSet<Set<VertexChange>> = mutableSetOf()

        fun addStateToTabu(state: Set<VertexChange>) {
            if (tabuListSize == 0)
                return

            if (tabuList.size == tabuListSize)
                tabuListSet -= tabuList.removeAt(0)

            tabuList.add(state)
            tabuListSet.add(state)
        }

        val greedyResult: GreedyModelComparator.ComparisonOutput = GreedyModelComparator.compareNew(
            sourceGraph.clone(),
            targetGraph.clone(),
            costConfig,
            useTypePriority = useGreedyTypePriority,
            useAlreadyMatchedPriority = useGreedyAlreadyMatchedPriority
        )

        var state: Set<VertexChange> = greedyResult.changes.map { it.vertex }.toSet()
        addStateToTabu(state)

        var minState: Pair<Set<VertexChange>, Double> =
            state to computeStateCost(state, sourceGraph.edges, targetGraph.edges, costConfig)

        val expansionTimes: MutableList<Long> = mutableListOf()

        var expansionsCount = 0
        while (expansionsCount < maxExpansionsCount) {
            val expansionStartMillis: Long = System.currentTimeMillis()

            var minimumVariantInner: Pair<Set<VertexChange>, Double>? = null
            loopThroughStateVariants(state, costConfig) {
                if (it !in tabuList) {
                    val stateCost = computeStateCost(it, sourceGraph.edges, targetGraph.edges, costConfig)
                    if (minimumVariantInner == null || stateCost < minimumVariantInner!!.second) {
                        minimumVariantInner = it to stateCost
                    }
                }
            }

            val minimumVariant = minimumVariantInner

            @Suppress("FoldInitializerAndIfToElvis")
            if (minimumVariant == null)
                break

            addStateToTabu(minimumVariant.first)
            state = minimumVariant.first

            if (minimumVariant.second < minState.second)
                minState = minimumVariant

            expansionsCount++

            val expansionTimeMillis: Long = System.currentTimeMillis() - expansionStartMillis
            expansionTimes.add(expansionTimeMillis)
        }

        val stateVertexChanges: List<VertexChange> = minState.first.toList()

        if (stateVertexChanges.isEmpty()) {
            return ComparisonOutput(
                    emptyList(), emptyList(), 0, 0, 0, 0, 0
            )
        }

        val stateEdgeChanges: List<EdgeChange> = generateStateEdgeChangesList(
                minState.first,
                sourceGraph.edges,
                targetGraph.edges,
                costConfig
        )

        val totalTimeMillis: Long = expansionTimes.sum()

        return ComparisonOutput(
            vertices = stateVertexChanges,
            edges = stateEdgeChanges,
            expansionsPerformed = expansionsCount,
            greedyTotalTimeMs = greedyResult.totalTimeMillis,
            expansionAverageTimeMs = if (expansionsCount > 0) (totalTimeMillis / expansionsCount).toInt() else 0,
            expansionsTotalTimeMs = totalTimeMillis.toInt(),
            algorithmTotalTimeMs = (greedyResult.totalTimeMillis + totalTimeMillis).toInt()
        )
    }

    fun generateStateEdgeChangesList(
        state: Set<VertexChange>,
        sourceEdges: List<Edge>,
        targetEdges: List<Edge>,
        costConfig: CostConfig
    ): List<EdgeChange> {
        val result = ArrayList<EdgeChange>()
        loopThroughStateEdgeChanges(
            state = state,
            sourceEdges = sourceEdges,
            targetEdges = targetEdges
        ) {
            // looks quite awful but helps to get rid of unnecessary list creations through inlining all the function calls
            source: Edge?, target: Edge? ->
            GraphUtils.minEdgeReplacement(source, target, costConfig) {
                source: Edge?, target: Edge?, replacementCost: Double ->
                result.add(EdgeChange(source, target, replacementCost))
            }
        }
        return result
    }

    fun computeStateEdgeChangesCost(
        state: Set<VertexChange>,
        sourceEdges: List<Edge>,
        targetEdges: List<Edge>,
        costConfig: CostConfig
    ): Double {
        var totalCost = 0.0
        loopThroughStateEdgeChanges(state, sourceEdges, targetEdges) {
            source: Edge?, target: Edge? ->
            totalCost += GraphUtils.minEdgeReplacementCost(source, target, costConfig)
        }
        return totalCost
    }

    inline fun loopThroughStateEdgeChanges(
        state: Set<VertexChange>,
        sourceEdges: List<Edge>,
        targetEdges: List<Edge>,
        consumer: (source: Edge?, target: Edge?) -> Unit
    ) {
        assertCustom { sourceEdges.toSet().size == sourceEdges.size }
        assertCustom { targetEdges.toSet().size == targetEdges.size }

        val remainedTarget: MutableList<Edge> = targetEdges.toMutableList()

        val mapping = state.map { it.source to it }.toMap()
        for (sourceEdge: Edge in sourceEdges) {
            val sourceVertexChange: VertexChange = mapping.getValue(sourceEdge.source)
            val targetVertexChange: VertexChange = mapping.getValue(sourceEdge.target)

            assertCustom { state.count { it.source == sourceEdge.source } == 1 }
            assertCustom { state.count { it.source == sourceEdge.target } == 1 }

            if (sourceVertexChange.target == null || targetVertexChange.target == null) {
                consumer(sourceEdge, null)
            } else {
                val targetEdge: Edge? = remainedTarget.firstOrNull {
                    it.source == sourceVertexChange.target && it.target == targetVertexChange.target && sourceEdge.element.type == it.element.type
                }

                consumer(sourceEdge, targetEdge)

                if (targetEdge != null) {
                    remainedTarget.remove(targetEdge)
                }
            }
        }

        for (remainedEdge: Edge in remainedTarget) {
            consumer(null, remainedEdge)
        }
    }

    private inline fun loopThroughStateVariants(
        state: Set<VertexChange>,
        costConfig: CostConfig,
        consumer: (state: Set<VertexChange>) -> Unit
    ) {
        loopThroughStateReplacements(state, costConfig, {true}) {
            _: Int, changesToRemove: List<VertexChange>?, changesToAdd: List<VertexChange>? ->

            val mutable = state.toMutableSet()
            mutable -= changesToRemove!!
            mutable += changesToAdd!!
            consumer(mutable)
            return@loopThroughStateReplacements true
        }
    }

    fun numberOfStateReplacements(state: Collection<VertexChange>, costConfig: CostConfig): Int {
        var totalVariants = 0
        loopThroughStateReplacements(state, costConfig, {false}) {
            variantNumber: Int, _: List<VertexChange>?, _: List<VertexChange>? ->
            if (variantNumber > totalVariants)
                totalVariants = variantNumber
            return@loopThroughStateReplacements true
        }
        return totalVariants
    }

    inline fun loopThroughStateReplacements(
        state: Collection<VertexChange>,
        costConfig: CostConfig,
        calculateChanges: (variantNumber: Int) -> Boolean,
        consumer: (variantNumber: Int, changesToRemove: List<VertexChange>?, changesToAdd: List<VertexChange>?) -> Boolean
    ) {
        var variantNumber = 1

        for (sourceChange: VertexChange in state) {
            if (/*sourceChange.source != null &&*/ sourceChange.target == null) {
                assertCustom { sourceChange.source != null }

                // исходная вершина удаляется (v, e)

                for (targetChange: VertexChange in state) {
                    if (sourceChange == targetChange)
                        continue
                    if (targetChange.target == null)
                        continue
                    if (sourceChange.source!!.element.type != targetChange.target.element.type)
                        continue

                    if (targetChange.source == null) {
                        // 1: (v1, e), (e, v2') -> (v1, v2')
                        val calcChanges = calculateChanges(variantNumber)
                        val `continue` = consumer(
                            variantNumber++,
                            if (calcChanges) listOf(sourceChange, targetChange) else null,
                            if (calcChanges) listOf(VertexChange.of(sourceChange.source, targetChange.target, costConfig)) else null
                        )
                        if (!`continue`)
                            return
                    } else {
                        // 2: (v1, e), (v2, v2') -> (v1, v2'), (v2, e)
                        val calcChanges = calculateChanges(variantNumber)
                        val `continue` = consumer(
                            variantNumber++,
                            if (calcChanges) listOf(sourceChange, targetChange) else null,
                            if (calcChanges)
                                listOf(
                                    VertexChange.of(sourceChange.source, targetChange.target, costConfig),
                                    VertexChange.of(targetChange.source, null, costConfig)
                                )
                            else null
                        )
                        if (!`continue`)
                            return
                    }
                }
            } else if (sourceChange.source != null/* && sourceChange.target != null*/) {
                // исходная вершина сопоставляется (v, v')

                for (targetChange: VertexChange in state) {
                    if (sourceChange == targetChange)
                        continue
                    if (targetChange.target == null)
                        continue
                    if (sourceChange.source.element.type != targetChange.target.element.type)
                        continue

                    if (targetChange.source == null) {
                        // 3: (v1, v1'), (e, v2') -> (v1, v2'), (e, v2)
                        val calcChanges = calculateChanges(variantNumber)
                        val `continue` = consumer(
                            variantNumber++,
                            if (calcChanges) listOf(sourceChange, targetChange) else null,
                            if (calcChanges)
                                listOf(
                                    VertexChange.of(sourceChange.source, targetChange.target, costConfig),
                                    VertexChange.of(null, sourceChange.target, costConfig)
                                )
                            else null
                        )
                        if (!`continue`)
                            return
                    } else {
                        // 4: (v1, v1'), (v2, v2') -> (v1, v2'), (v2, v1')
                        var calcChanges = calculateChanges(variantNumber)
                        var `continue` = consumer(
                            variantNumber++,
                            if (calcChanges) listOf(sourceChange, targetChange) else null,
                            if (calcChanges)
                                listOf(
                                    VertexChange.of(sourceChange.source, targetChange.target, costConfig),
                                    VertexChange.of(targetChange.source, sourceChange.target, costConfig)
                                )
                            else null
                        )
                        if (!`continue`)
                            return

                        // 6: (v1, v1'), (v2, v2') -> (v1, e), (v2, v1'), (e, v2')
                        calcChanges = calculateChanges(variantNumber)
                        `continue` = consumer(
                            variantNumber++,
                            if (calcChanges) listOf(sourceChange, targetChange) else null,
                            if (calcChanges)
                                listOf(
                                    VertexChange.of(sourceChange.source, null, costConfig),
                                    VertexChange.of(targetChange.source, sourceChange.target, costConfig),
                                    VertexChange.of(null, targetChange.target, costConfig)
                                )
                            else null
                        )
                        if (!`continue`)
                            return
                    }
                }

                // 5: (v, v') -> (v1, e), (e, v')
                val calcChanges = calculateChanges(variantNumber)
                val `continue` = consumer(
                    variantNumber++,
                    if (calcChanges) listOf(sourceChange) else null,
                    if (calcChanges)
                        listOf(
                            VertexChange.of(sourceChange.source, null, costConfig),
                            VertexChange.of(null, sourceChange.target, costConfig)
                        )
                    else null
                )
                if (!`continue`)
                    return
            }
        }
    }

    fun computeStateCost(
        state: Set<VertexChange>,
        sourceEdges: List<Edge>,
        targetEdges: List<Edge>,
        costConfig: CostConfig
    ): Double {
        return state.sumByDouble { it.differenceCost } + computeStateEdgeChangesCost(state, sourceEdges, targetEdges, costConfig)
    }
}
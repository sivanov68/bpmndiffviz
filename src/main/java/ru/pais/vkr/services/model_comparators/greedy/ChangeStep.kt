package ru.pais.vkr.services.model_comparators.greedy

import ru.pais.vkr.comparison_changes.EdgeChange
import ru.pais.vkr.comparison_changes.VertexChange

data class ChangeStep(
    val vertex: VertexChange,
    val edges: List<EdgeChange>
) {
    val totalCost by lazy { vertex.differenceCost + edges.sumByDouble { it.differenceCost } }
}
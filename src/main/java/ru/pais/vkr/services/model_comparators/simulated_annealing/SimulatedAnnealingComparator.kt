package ru.pais.vkr.services.model_comparators.simulated_annealing

import org.camunda.bpm.model.bpmn.BpmnModelInstance
import ru.pais.vkr.comparator.entities.ComparisonResult
import ru.pais.vkr.comparator.entities.CostConfig
import ru.pais.vkr.comparison_changes.EdgeChange
import ru.pais.vkr.comparison_changes.VertexChange
import ru.pais.vkr.graph_elements.Graph
import ru.pais.vkr.services.model_comparators.ModelComparator
import ru.pais.vkr.services.model_comparators.ModelComparatorType
import ru.pais.vkr.services.model_comparators.greedy.GreedyModelComparator
import ru.pais.vkr.services.model_comparators.tabu_search.TabuSearchModelComparator
import ru.pais.vkr.utils.ModelComparatorUtils
import ru.pais.vkr.utils.toGraph
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.exp

object SimulatedAnnealingComparator : ModelComparator() {
    @Suppress("RemoveExplicitTypeArguments")
    object Parameters {
        val USE_GREEDY_TYPE_PRIORITY = Parameter<Boolean>("Greedy: ${GreedyModelComparator.Parameters.USE_TYPE_PRIORITY.name}", false)
        val USE_GREEDY_ALREADY_MATCHED_PRIORITY = Parameter<Boolean>("Greedy: ${GreedyModelComparator.Parameters.USE_ALREADY_MATCHED_PRIORITY.name}", false)
        val CHOOSE_THE_BEST = Parameter<Boolean>("Choose the best variant from the observed ones", true)
        val INITIAL_TEMPERATURE = Parameter<Double>("Initial temperature of the system", 100.0)
        val MINIMAL_TEMPERATURE = Parameter<Double>("Minimal temperature of the system", 0.0)
        val TEMPERATURE_DECREASE = Parameter<Double>("Temperature decrease on each iteration", 0.001)
    }

    object Statistics {
        val SOURCE_GRAPH_VERTICES = CommonStatistics.SOURCE_GRAPH_VERTICES
        val TARGET_GRAPH_VERTICES = CommonStatistics.TARGET_GRAPH_VERTICES
        val SOURCE_GRAPH_EDGES = CommonStatistics.SOURCE_GRAPH_EDGES
        val TARGET_GRAPH_EDGES = CommonStatistics.TARGET_GRAPH_EDGES
        val GREEDY_TOTAL_TIME_MS = CommonStatistics.GREEDY_TOTAL_TIME_MS
        val ITERATION_AVERAGE_TIME_MS = Statistic<Int>("Iteration average time (ms)")
        val ITERATIONS_PERFORMED = Statistic<Int>("Iterations performed")
        val TOTAL_TIME_MILLIS = CommonStatistics.TOTAL_TIME_MILLIS
    }

    override val type: ModelComparatorType = ModelComparatorType.SimulatedAnnealing
    override val parameters: List<ParameterRaw> = parametersFrom(Parameters)

    private val random = Random()

    override fun compareModels(
        firstBpmnModel: BpmnModelInstance,
        secondBpmnModel: BpmnModelInstance,
        costConfig: CostConfig,
        inputParameters: Map<String, Any>,
        useParallelization: Boolean
    ): ComparisonResult {
        val sourceGraph = firstBpmnModel.toGraph()
        val targetGraph = secondBpmnModel.toGraph()

        val innerResult: ComparisonOutput = compareNew(
            sourceGraph = sourceGraph,
            targetGraph = targetGraph,
            costConfig = costConfig,
            useParallelization = useParallelization,
            initialTemperature = Parameters.INITIAL_TEMPERATURE.valueFrom(inputParameters),
            minimalTemperature = Parameters.MINIMAL_TEMPERATURE.valueFrom(inputParameters),
            temperatureDecrease = Parameters.TEMPERATURE_DECREASE.valueFrom(inputParameters),
            chooseTheBest = Parameters.CHOOSE_THE_BEST.valueFrom(inputParameters),
            useGreedyTypePriority = Parameters.USE_GREEDY_TYPE_PRIORITY.valueFrom(inputParameters),
            useGreedyAlreadyMatchedPriority = Parameters.USE_GREEDY_ALREADY_MATCHED_PRIORITY.valueFrom(inputParameters)
        )

        return ModelComparatorUtils.constructComparisonResult(
            sourceBpmnModel = firstBpmnModel,
            targetBpmnModel = secondBpmnModel,
            matchedVertices = innerResult.vertices,
            matchedEdges = innerResult.edges,
            costConfig = costConfig
        ).apply {
            with (Statistics) {
                SOURCE_GRAPH_VERTICES.fill(statistics, sourceGraph.vertices.size)
                TARGET_GRAPH_VERTICES.fill(statistics, targetGraph.vertices.size)
                SOURCE_GRAPH_EDGES.fill(statistics, sourceGraph.edges.size)
                TARGET_GRAPH_EDGES.fill(statistics, targetGraph.edges.size)
                GREEDY_TOTAL_TIME_MS.fill(statistics, innerResult.greedyTotalTimeMs)
                ITERATION_AVERAGE_TIME_MS.fill(statistics, innerResult.iterationAverageMs)
                ITERATIONS_PERFORMED.fill(statistics, innerResult.iterationsPerformed)
                TOTAL_TIME_MILLIS.fill(statistics, innerResult.totalTimeMillis)
            }
        }
    }

    private fun compareNew(
        sourceGraph: Graph,
        targetGraph: Graph,
        costConfig: CostConfig,
        useParallelization: Boolean,
        chooseTheBest: Boolean,
        initialTemperature: Double,
        minimalTemperature: Double,
        temperatureDecrease: Double,
        useGreedyTypePriority: Boolean,
        useGreedyAlreadyMatchedPriority: Boolean
    ): ComparisonOutput {
        // main loop with finding the best solution

        val greedyStart: Long = System.currentTimeMillis()

        // find the first state via the greedy algorithm
        val greedyResult = GreedyModelComparator.compareNew(
            sourceGraph = sourceGraph.clone(),
            targetGraph = targetGraph.clone(),
            costConfig = costConfig,
            useTypePriority = useGreedyTypePriority,
            useAlreadyMatchedPriority = useGreedyAlreadyMatchedPriority
        )

        val greedyTotalTimeMs: Int = (System.currentTimeMillis() - greedyStart).toInt()

        val iterationTimes = ArrayList<Int>()
        var iterationsPerformed = 0

        val currentState = State(
            stateMatches = greedyResult.changes.map { it.vertex }.toMutableSet(),
            stateCost = greedyResult.changes.sumByDouble { it.totalCost }
        )

        var bestState: Pair<Set<VertexChange>, Double>? = null
        if (chooseTheBest) {
            bestState = currentState.stateMatches.toSet() to currentState.stateCost
        }

        var currentTemperature: Double = initialTemperature
        while (currentTemperature >= minimalTemperature) {
            val iterationStart: Long = System.currentTimeMillis()

            // get the number of all variants
            val totalVariants = TabuSearchModelComparator.numberOfStateReplacements(currentState.stateMatches, costConfig)

            // select random variant from the generated
            val variantIndex: Int = random.nextInt(totalVariants) + 1
            var variantTemp: Pair<List<VertexChange>, List<VertexChange>>? = null
            TabuSearchModelComparator.loopThroughStateReplacements(currentState.stateMatches, costConfig,
                calculateChanges = {it == variantIndex}
            ) {
                index: Int, changesToRemove: List<VertexChange>?, changesToAdd: List<VertexChange>? ->
                if (index == variantIndex) {
                    variantTemp = changesToRemove!! to changesToAdd!!
                    return@loopThroughStateReplacements false
                }
                return@loopThroughStateReplacements true
            }

            val variantChange: Pair<List<VertexChange>, List<VertexChange>> = variantTemp!!
            currentState.stateMatches -= variantChange.first
            currentState.stateMatches += variantChange.second

            // choose whether to move to the new variant or stay on the previous one
            val variantCost: Double = TabuSearchModelComparator.computeStateCost(
                state = currentState.stateMatches,
                sourceEdges = sourceGraph.edges,
                targetEdges = targetGraph.edges,
                costConfig = costConfig
            )

            val costDifference: Double = variantCost - currentState.stateCost

            if (costDifference <= 0) {
                currentState.stateCost = variantCost
            } else {
                val changeStateProbability: Double = exp(-costDifference / currentTemperature)
                if (random.nextDouble() <= changeStateProbability) {
                    currentState.stateCost = variantCost
                } else {
                    currentState.stateMatches -= variantChange.second
                    currentState.stateMatches += variantChange.first
                }
            }

            if (chooseTheBest && variantCost < bestState!!.second) {
                bestState = currentState.stateMatches.toSet() to variantCost
            }

            // decrease environment temperature
            currentTemperature -= temperatureDecrease

            iterationTimes.add((System.currentTimeMillis() - iterationStart).toInt())
            iterationsPerformed++
        }

        val resultState = if (chooseTheBest) bestState!! else currentState.stateMatches to currentState.stateCost

        val vertexChanges: List<VertexChange> = resultState.first.toList()
        val edgeChanges: List<EdgeChange> = TabuSearchModelComparator.generateStateEdgeChangesList(
            state = resultState.first,
            sourceEdges = sourceGraph.edges,
            targetEdges = targetGraph.edges,
            costConfig = costConfig
        )

        return ComparisonOutput(
            vertices = vertexChanges,
            edges = edgeChanges,
            greedyTotalTimeMs = greedyTotalTimeMs,
            iterationAverageMs = if (iterationTimes.isNotEmpty()) iterationTimes.sum() / iterationTimes.size else 0,
            iterationsPerformed = iterationsPerformed,
            totalTimeMillis = iterationTimes.sum() + greedyTotalTimeMs
        )
    }

    private data class ComparisonOutput(
        val vertices: List<VertexChange>,
        val edges: List<EdgeChange>,
        val greedyTotalTimeMs: Int,
        val iterationAverageMs: Int,
        val iterationsPerformed: Int,
        val totalTimeMillis: Int
    )

    private data class State(
        val stateMatches: MutableSet<VertexChange>,
        var stateCost: Double
    )
}
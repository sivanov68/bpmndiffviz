package ru.pais.vkr.services.model_comparators.ants

import org.camunda.bpm.model.bpmn.BpmnModelInstance
import ru.pais.vkr.comparator.entities.ComparisonResult
import ru.pais.vkr.comparator.entities.CostConfig
import ru.pais.vkr.comparison_changes.EdgeChange
import ru.pais.vkr.comparison_changes.VertexChange
import ru.pais.vkr.graph_elements.Graph
import ru.pais.vkr.graph_elements.Vertex
import ru.pais.vkr.services.model_comparators.ModelComparator
import ru.pais.vkr.services.model_comparators.ModelComparatorType
import ru.pais.vkr.services.model_comparators.greedy.ChangeStep
import ru.pais.vkr.utils.ModelComparatorUtils
import ru.pais.vkr.utils.toGraph
import java.util.concurrent.CountDownLatch
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicInteger

object AntsModelComparator : ModelComparator() {
    @Suppress("RemoveExplicitTypeArguments")
    object Parameters {
        val INITIAL_PHEROMONE_LEVEL = Parameter<Double>("Initial pheromone on each connection", 1.0)
        val NUMBER_OF_ANTS_PER_ITERATION = Parameter<Int>("Number of ants creating of each iteration", 10)
        val NUMBER_OF_ITERATIONS = Parameter<Int>("Number of iterations performed", 10)
        val PHEROMONE_POWER = Parameter<Double>("Power for the pheromone level on the path", 1.0)
        val DISTANCE_POWER = Parameter<Double>("Power for the path distance", 1.0)
        val DISTANCE_COEFFICIENT = Parameter<Double>("Coefficient for converting distance to pheromone increase", 1.0)
        val PHEROMONE_EVAPORATION = Parameter<Double>("Part of the pheromone that evaporates every iteration", 1.0)
    }

    object Statistics {
        val SOURCE_GRAPH_VERTICES = CommonStatistics.SOURCE_GRAPH_VERTICES
        val TARGET_GRAPH_VERTICES = CommonStatistics.TARGET_GRAPH_VERTICES
        val SOURCE_GRAPH_EDGES = CommonStatistics.SOURCE_GRAPH_EDGES
        val TARGET_GRAPH_EDGES = CommonStatistics.TARGET_GRAPH_EDGES
        val ANT_SEARCH_AVERAGE_TIME_MS = Statistic<Int>("Average time of an ant finding path (ms)")
        val ITERATION_AVERAGE_TIME_MS = Statistic<Int>("Iteration average time (ms)")
        val TOTAL_TIME_MILLIS = CommonStatistics.TOTAL_TIME_MILLIS
    }

    override val type: ModelComparatorType = ModelComparatorType.Ants
    override val parameters: List<ParameterRaw> = parametersFrom(Parameters)

    override fun compareModels(
        firstBpmnModel: BpmnModelInstance,
        secondBpmnModel: BpmnModelInstance,
        costConfig: CostConfig,
        inputParameters: Map<String, Any>,
        useParallelization: Boolean
    ): ComparisonResult {
        val sourceGraph = firstBpmnModel.toGraph()
        val targetGraph = secondBpmnModel.toGraph()

        val innerResult: ComparisonOutput = compareNew(
            sourceGraph = sourceGraph,
            targetGraph = targetGraph,
            costConfig = costConfig,
            useParallelization = useParallelization,
            initialPheromoneValue = Parameters.INITIAL_PHEROMONE_LEVEL.valueFrom(inputParameters),
            numberOfAnts = Parameters.NUMBER_OF_ANTS_PER_ITERATION.valueFrom(inputParameters),
            numberOfIterations = Parameters.NUMBER_OF_ITERATIONS.valueFrom(inputParameters),
            pheromonePower = Parameters.PHEROMONE_POWER.valueFrom(inputParameters),
            distancePower = Parameters.DISTANCE_POWER.valueFrom(inputParameters),
            distanceCoeff = Parameters.DISTANCE_COEFFICIENT.valueFrom(inputParameters),
            pheromoneEvaporation = Parameters.PHEROMONE_EVAPORATION.valueFrom(inputParameters)
        )

        return ModelComparatorUtils.constructComparisonResult(
            sourceBpmnModel = firstBpmnModel,
            targetBpmnModel = secondBpmnModel,
            matchedVertices = innerResult.vertices,
            matchedEdges = innerResult.edges,
            costConfig = costConfig
        ).apply {
            with (Statistics) {
                SOURCE_GRAPH_VERTICES.fill(statistics, sourceGraph.vertices.size)
                TARGET_GRAPH_VERTICES.fill(statistics, targetGraph.vertices.size)
                SOURCE_GRAPH_EDGES.fill(statistics, sourceGraph.edges.size)
                TARGET_GRAPH_EDGES.fill(statistics, targetGraph.edges.size)
                ANT_SEARCH_AVERAGE_TIME_MS.fill(statistics, innerResult.antSearchAverageMillis)
                ITERATION_AVERAGE_TIME_MS.fill(statistics, innerResult.iterationAverageMillis)
                TOTAL_TIME_MILLIS.fill(statistics, innerResult.totalTimeMillis)
            }
        }
    }

    private fun compareNew(
        sourceGraph: Graph,
        targetGraph: Graph,
        costConfig: CostConfig,
        useParallelization: Boolean,
        initialPheromoneValue: Double,
        numberOfAnts: Int,
        numberOfIterations: Int,
        pheromonePower: Double,
        distancePower: Double,
        distanceCoeff: Double,
        pheromoneEvaporation: Double
    ): ComparisonOutput {
        // creating initial pheromone map
        val pheromoneMap: HashMap<Vertex?, HashMap<Vertex?, Double>> = hashMapOf()
        for (sourceVertex in (sourceGraph.vertices + arrayOf<Vertex?>(null))) {
            val vertexMap: HashMap<Vertex?, Double> = hashMapOf()
            for (targetVertex in targetGraph.vertices) {
                if (VertexChange.canBeReplaced(sourceVertex, targetVertex)) {
                    vertexMap[targetVertex] = initialPheromoneValue
                }
            }
            if (sourceVertex != null) {
                // deleting source vertex
                vertexMap[null] = initialPheromoneValue
            }
            pheromoneMap[sourceVertex] = vertexMap
        }

        // main loop with finding the best solution
        val antSearchTimes = AtomicInteger()
        val iterationTimes = ArrayList<Int>()
        val solutions: Array<List<ChangeStep>?> = arrayOfNulls(numberOfAnts)
        var minSolution: Pair<List<ChangeStep>, Double>? = null
        val executor: ExecutorService? = if (useParallelization) {
            Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors())
        } else {
            null
        }

        fun oneAntExecution(antIndex: Int, latch: CountDownLatch?) {
            val ant = Ant(
                sourceGraph.clone(),
                targetGraph.clone(),
                costConfig,
                pheromoneMap,
                pheromonePower,
                distancePower
            )

            val antSearchStart: Long = System.currentTimeMillis()
            val solution = ant.findSolution()
            val antTime = System.currentTimeMillis() - antSearchStart
            solutions[antIndex] = solution
            antSearchTimes.addAndGet(antTime.toInt())
            latch?.countDown()
        }

        for (iteration in 1..numberOfIterations) {
            val iterationStart: Long = System.currentTimeMillis()

            // find solutions using ants
            val latch: CountDownLatch? = if (useParallelization) CountDownLatch(numberOfAnts) else null
            for (antIndex in 0 until numberOfAnts) {
                if (useParallelization) {
                    executor!!.execute { oneAntExecution(antIndex, latch) }
                } else {
                    oneAntExecution(antIndex, null)
                }
            }
            if (useParallelization) {
                latch!!.await()
            }

            // evaporate pheromone
            val remainingPart = 1.0 - pheromoneEvaporation
            for (sourceMap in pheromoneMap) {
                for (targetKey in sourceMap.value.keys) {
                    sourceMap.value[targetKey] = remainingPart * sourceMap.value.getValue(targetKey)
                }
            }

            // increase pheromone for the used paths
            for (solution in solutions) {
                var solutionCost: Double = solution!!.sumByDouble { it.totalCost }
                if (minSolution == null || solutionCost < minSolution.second) {
                    minSolution = solution to solutionCost
                }
                if (solutionCost == 0.0) {
                    solutionCost = 0.00001
                }

                val pheromoneIncrease: Double = distanceCoeff / solutionCost
                for (item in solution) {
                    val sourceMap = pheromoneMap.getValue(item.vertex.source)
                    sourceMap[item.vertex.target] = sourceMap.getValue(item.vertex.target) + pheromoneIncrease
                }
            }

            iterationTimes.add((System.currentTimeMillis() - iterationStart).toInt())
        }
        if (useParallelization) {
            executor!!.shutdown()
        }

        // creating the result
        if (minSolution == null) {
            return ComparisonOutput(emptyList(), emptyList(), 0, 0, 0)
        }

        return ComparisonOutput(
            vertices = minSolution.first.map { it.vertex },
            edges = minSolution.first.flatMap { it.edges },
            antSearchAverageMillis = if (numberOfAnts > 0) antSearchTimes.get() / (numberOfAnts * numberOfIterations) else 0,
            iterationAverageMillis = if (numberOfIterations > 0) iterationTimes.sum() / numberOfIterations else 0,
            totalTimeMillis = iterationTimes.sum()
        )
    }

    private data class ComparisonOutput(
        val vertices: List<VertexChange>,
        val edges: List<EdgeChange>,
        val antSearchAverageMillis: Int,
        val iterationAverageMillis: Int,
        val totalTimeMillis: Int
    )
}
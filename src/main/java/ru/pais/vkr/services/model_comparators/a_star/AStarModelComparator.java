package ru.pais.vkr.services.model_comparators.a_star;

import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.SequenceFlow;
import org.camunda.bpm.model.xml.instance.ModelElementInstance;
import org.jetbrains.annotations.NotNull;
import ru.pais.vkr.comparator.entities.*;
import ru.pais.vkr.services.model_comparators.ModelComparator;
import ru.pais.vkr.services.model_comparators.ModelComparatorType;
import ru.pais.vkr.utils.BPMNUtils;
import ru.pais.vkr.utils.ModelComparatorUtils;
import ru.pais.vkr.utils.Safe;

import java.util.*;
import java.util.stream.Collectors;

public class AStarModelComparator extends ModelComparator {

    public static class Statistics {
        public static final Statistic<Integer> RECURSION_DEPTH = new Statistic<>("Recursion depth");
        public static final Statistic<Integer> COMPARISONS = new Statistic<>("Comparisons");
        public static final Statistic<Integer> TOTAL_TIME_MS = CommonStatistics.TOTAL_TIME_MILLIS;

        private Statistics() {}
    }

    private class InnerResult {
        public final int comparisons;
        public final ComparisonResult comparisonResult;

        public InnerResult(int comparisons, ComparisonResult comparisonResult) {
            this.comparisons = comparisons;
            this.comparisonResult = comparisonResult;
        }
    }

    @NotNull
    @Override
    public ModelComparatorType getType() {
        return ModelComparatorType.AStar;
    }

    @NotNull
    @Override
    public List<ParameterRaw> getParameters() {
        return Collections.emptyList();
    }

    /**
     * {@inheritDoc}
     */
    @NotNull
    @Override
    public ComparisonResult compareModels(
        @NotNull BpmnModelInstance firstBpmnModel,
        @NotNull BpmnModelInstance secondBpmnModel,
        @NotNull CostConfig costConfig,
        @NotNull Map<String, ?> inputParameters,
        boolean useParallelization
    ) {
        PriorityQueue<ComparisonResult> comparisonResultQueue = new PriorityQueue<>();

        // элементы, содержащиеся в обоих моделях
        final ArrayList<BPMNElementType> typesInBothModels = new ArrayList<>();
        // элементы, содержащиеся хотя бы в одной модели
        final ArrayList<BPMNElementType> typesInAnyModel = new ArrayList<>();

        long startMillis = System.currentTimeMillis();

        //Generate element types
        ModelComparatorUtils.fillModelElementTypes(
            firstBpmnModel,
            secondBpmnModel,
            typesInBothModels,
            typesInAnyModel
        );

        //Compare elements
        Collection<BPMNElement> modelElementInstances2 = BPMNUtils.getElementsByType(secondBpmnModel, typesInBothModels.get(0));

        BPMNElement modelElementInstance1 = BPMNUtils.getElementsByType(firstBpmnModel, typesInBothModels.get(0)).get(0);

        //Find correspondences
        for (BPMNElement modelElementInstance2 : modelElementInstances2) {
            ComparisonResult comparisonResult = new ComparisonResult(
                firstBpmnModel,
                secondBpmnModel,
                typesInBothModels,
                typesInAnyModel,
                costConfig
            );

            BPMNComparator bpmnComparator = costConfig.getValidComparator(typesInBothModels.get(0));
            MatchingPair matchingPair = bpmnComparator.compare(
                comparisonResult,
                firstBpmnModel,
                secondBpmnModel,
                modelElementInstance1,
                modelElementInstance2
            );
            matchingPair.setType(PairType.MATCH);
            if (Safe.contains(BPMNUtils.VERTEX_TYPES, typesInBothModels.get(0))) {
                if (matchingPair.isLinkSave()) {
                    comparisonResult.updateMatching(matchingPair);
                    comparisonResultQueue.add(comparisonResult);
                }
            } else {
                comparisonResult.updateMatching(matchingPair);
                comparisonResultQueue.add(comparisonResult);
            }
        }

        //Delete this element
        ComparisonResult comparisonResult = new ComparisonResult(firstBpmnModel, secondBpmnModel, typesInBothModels, typesInAnyModel, costConfig);
        double d = comparisonResult.getCostConfig().getDeleteCostFor(modelElementInstance1.getType());
        MatchingPair matchingPair = new MatchingPair(modelElementInstance1, null, PairType.DELETE);
        matchingPair.setDifferentLabel(d);
        comparisonResult.updateDeleting(matchingPair);
        comparisonResultQueue.add(comparisonResult);

        InnerResult innerResult = compareElementsRecursive(
            comparisonResultQueue,
            comparisonResultQueue.peek(),
            firstBpmnModel,
            secondBpmnModel
        );

        ComparisonResult result = innerResult.comparisonResult;

        int totalTimeMillis = (int)(System.currentTimeMillis() - startMillis);

        Map<String, Object> statistics = result.getStatistics();
        Statistics.RECURSION_DEPTH.fill(statistics, innerResult.comparisons - 1);
        Statistics.COMPARISONS.fill(statistics, innerResult.comparisons);
        Statistics.TOTAL_TIME_MS.fill(statistics, totalTimeMillis);

        return result;
    }

    private InnerResult compareElementsRecursive(
        @NotNull PriorityQueue<ComparisonResult> comparisonResultQueue,
        @NotNull ComparisonResult comparisonResult,
        @NotNull BpmnModelInstance bpmnModelInstance1,
        @NotNull BpmnModelInstance bpmnModelInstance2
    ) {
        int comparisons = 0;

        while (true) {
            comparisons++;
            if (comparisonResult.getComparingClassList().isEmpty()) {
                BPMNElement modelElementInstanceInner1 = null;
                BPMNElement modelElementInstanceInner2 = null;

                for (Iterator<BPMNElementType> iterator = comparisonResult.getAddedClassList().iterator(); iterator.hasNext(); ) {
                    BPMNElementType aClass1 = iterator.next();
                    Collection<BPMNElement> collection1 = BPMNUtils.getElementsByType(bpmnModelInstance1, aClass1);
                    Collection<BPMNElement> collection2 = BPMNUtils.getElementsByType(bpmnModelInstance2, aClass1);

                    for (BPMNElement mi : collection1) {
                        if (!comparisonResult.containsInKeysDeletedOrKeysAddedOrKeysMatchingSets(mi)) {
                            modelElementInstanceInner1 = mi;
                        }
                    }
                    for (BPMNElement mi : collection2) {
                        if (!comparisonResult.containsInKeysDeletedOrKeysAddedOrValuesMatchingSets(mi)) {
                            modelElementInstanceInner2 = mi;
                        }
                    }

                    if ((modelElementInstanceInner1 == null) && (modelElementInstanceInner2 == null)) {
                        iterator.remove();
                    } else {
                        break;
                    }
                }
                //All elements are matched, deleted or added
                if (comparisonResult.getAddedClassList().isEmpty()) {
                    if (comparisonResult == comparisonResultQueue.peek()) {
                        return new InnerResult(comparisons, comparisonResult);
                    }

                    comparisonResult = comparisonResultQueue.peek();
                    continue;
                }

                assert modelElementInstanceInner1 == null || modelElementInstanceInner2 == null;

                ComparisonResult comparisonResultNew = new ComparisonResult(comparisonResult, comparisonResult.getCostConfig());

                if (modelElementInstanceInner1 != null) {
                    //Delete this element
                    double d = comparisonResultNew.getCostConfig().getDeleteCostFor(modelElementInstanceInner1.getType());
                    MatchingPair matchingPair = new MatchingPair(modelElementInstanceInner1, null, PairType.DELETE);
                    matchingPair.setDifferentLabel(d);
                    comparisonResultNew.updateDeleting(matchingPair);
                } else {
                    //Add this element
                    double d = comparisonResultNew.getCostConfig().getInsertCostFor(modelElementInstanceInner2.getType());
                    MatchingPair matchingPair = new MatchingPair(modelElementInstanceInner2, null, PairType.INSERT);
                    matchingPair.setDifferentLabel(d);
                    comparisonResultNew.updateAdding(matchingPair);
                }

                comparisonResultNew.updateHeuristic();

                comparisonResultQueue.remove(comparisonResult);
                comparisonResultQueue.add(comparisonResultNew);

                comparisonResult = comparisonResultQueue.peek();
            }
            //getComparingClassList is not empty
            else {

                BPMNElementType aClass = null;
                BPMNElement modelElementInstance1 = null;
                // ------------------------------------First compare tasks

                if (!comparisonResult.getComparingClassList().contains(BPMNElementType.Task)) {

                    if (aClass == null) {
                        Collection<SequenceFlow> sequenceFlows = comparisonResult.getFirstBpmnModel().getModelElementsByType(SequenceFlow.class);
                        Collection<BPMNElement> matchedElements = comparisonResult.getKeysFromMatchingSet();
                        Collection<BPMNElement> deletedElements = comparisonResult.getKeysFromDeletedSet();
                        Collection<BPMNElement> addedElements = comparisonResult.getKeysFromAddedSet();

                        //-------------------------------------------------------------------

                        if (modelElementInstance1 == null) {
                            // -------------------------------------Match a node connected with already matched
                            for (BPMNElement element : matchedElements) {
                                for (SequenceFlow sf : sequenceFlows) {
                                    final ModelElementInstance sourceNode = sf.getSource();
                                    final ModelElementInstance targetNode = sf.getTarget();

                                    if (sourceNode.equals(element.getRaw())) {
                                        if (deletedElements.stream().noneMatch(it -> it.getRaw().equals(targetNode)) &&
                                            addedElements.stream().noneMatch(it -> it.getRaw().equals(targetNode)) &&
                                            matchedElements.stream().noneMatch(it -> it.getRaw().equals(targetNode))
                                        ) {
                                            modelElementInstance1 = new BPMNElement(sf.getTarget());
                                            aClass = modelElementInstance1.getType();
                                        }
                                    }

                                    if (targetNode.equals(element.getRaw())) {
                                        if (deletedElements.stream().noneMatch(it -> it.getRaw().equals(sourceNode)) &&
                                            addedElements.stream().noneMatch(it -> it.getRaw().equals(sourceNode)) &&
                                            matchedElements.stream().noneMatch(it -> it.getRaw().equals(sourceNode))
                                        ) {
                                            modelElementInstance1 = new BPMNElement(sf.getSource());
                                            aClass = modelElementInstance1.getType();
                                        }
                                    }
                                }
                            }
                            //--------------------------------------------------------------------------------
                        }
                    }

                }

                if (aClass == null) {
                    aClass = comparisonResult.getComparingClassList().get(0);
                }
                if (modelElementInstance1 == null) {
                    Collection<BPMNElement> collection = BPMNUtils.getElementsByType(bpmnModelInstance1, aClass);

                    //Delete full compared classes
                    boolean isEmpty = true;
                    while (isEmpty) {
                        for (BPMNElement mi : collection) {
                            if (!comparisonResult.containsInKeysDeletedOrKeysAddedOrKeysMatchingSets(mi)) {
                                isEmpty = false;
                            }
                        }
                        if (isEmpty) {
                            comparisonResult.getComparingClassList().remove(aClass);
                            //Comparing class list is empty
                            if (comparisonResult.getComparingClassList().isEmpty()) {
                                break;
                            } else {
                                aClass = comparisonResult.getComparingClassList().get(0);
                                collection = BPMNUtils.getElementsByType(bpmnModelInstance1, aClass);
                            }
                        }
                    }

                    if (comparisonResult.getComparingClassList().isEmpty()) {
                        comparisonResult = comparisonResultQueue.peek();
                        continue;
                    }

                    //Compare elements

                    // find any element that have not been matched
                    Collection<BPMNElement> modelElementInstances1 = BPMNUtils.getElementsByType(bpmnModelInstance1, aClass);
                    for (BPMNElement mi : modelElementInstances1) {
                        if (!comparisonResult.containsInKeysDeletedOrKeysAddedOrKeysMatchingSets(mi)) {
                            modelElementInstance1 = mi;
                        }
                    }
                }

                Collection<BPMNElement> modelElementInstances2 = BPMNUtils.getElementsByType(bpmnModelInstance2, aClass);
                //Find correspondences
                for (BPMNElement modelElementInstance2 : modelElementInstances2) {
                    if (comparisonResult.getValuesFromMatchingSet().stream().noneMatch(it -> it.getRaw().equals(modelElementInstance2.getRaw()))) {
                        ComparisonResult comparisonResultNew = new ComparisonResult(comparisonResult, comparisonResult.getCostConfig());
                        MatchingPair matchingPair = comparisonResult.getCostConfig().getValidComparator(aClass).compare(
                            comparisonResultNew,
                            bpmnModelInstance1,
                            bpmnModelInstance2,
                            modelElementInstance1,
                            modelElementInstance2
                        );
                        matchingPair.setType(PairType.MATCH);
                        if (Safe.contains(BPMNUtils.EDGE_TYPES, aClass)) {
                            if (matchingPair.isLinkSave()) {
                                comparisonResultNew.updateMatching(matchingPair);
                                comparisonResultQueue.add(comparisonResultNew);
                            }
                        } else {
                            comparisonResultNew.updateMatching(matchingPair);
                            updateSequenceFlows(comparisonResultNew, bpmnModelInstance1, bpmnModelInstance2);
                            comparisonResultQueue.add(comparisonResultNew);
                        }
                    }
                }

                //Delete this element
                ComparisonResult comparisonResultNew = new ComparisonResult(comparisonResult, comparisonResult.getCostConfig());
                double d = comparisonResultNew.getCostConfig().getDeleteCostFor(modelElementInstance1.getType());
                MatchingPair matchingPair = new MatchingPair(modelElementInstance1, null, PairType.DELETE);
                matchingPair.setDifferentLabel(d);
                comparisonResultNew.updateDeleting(matchingPair);
                comparisonResultNew.updateHeuristic();
                updateSequenceFlows(comparisonResultNew, bpmnModelInstance1, bpmnModelInstance2);
                comparisonResultQueue.add(comparisonResultNew);

                //Delete the exhaust comparisonResult
                comparisonResultQueue.remove(comparisonResult);

                comparisonResult = comparisonResultQueue.peek();
            }
        }
    }

    private void updateSequenceFlows(
        @NotNull ComparisonResult comparisonResult,
        @NotNull BpmnModelInstance firstBpmnModel,
        @NotNull BpmnModelInstance secondBpmnModel
    ) {
        final Collection<SequenceFlow> sequenceFlows = comparisonResult.getFirstBpmnModel().getModelElementsByType(SequenceFlow.class);

        final Collection<BPMNElement> matchedElements = comparisonResult.getKeysFromMatchingSet();
        final Collection<BPMNElement> deletedElements = comparisonResult.getKeysFromDeletedSet();
        final Collection<BPMNElement> addedElements = comparisonResult.getKeysFromAddedSet();

        final Set<ModelElementInstance> matchedRawElements = matchedElements.stream().map(BPMNElement::getRaw).collect(Collectors.toSet());
        final Set<ModelElementInstance> deletedRawElements = deletedElements.stream().map(BPMNElement::getRaw).collect(Collectors.toSet());
        final Set<ModelElementInstance> addedRawElements = addedElements.stream().map(BPMNElement::getRaw).collect(Collectors.toSet());

        for (SequenceFlow sf : sequenceFlows) {
            final BPMNElement sfElement = new BPMNElement(sf);

            final ModelElementInstance source = sf.getSource();
            final ModelElementInstance target = sf.getTarget();

            if (
                (
                    Safe.contains(matchedRawElements, source) ||
                        Safe.contains(deletedRawElements, source) ||
                        Safe.contains(addedRawElements, source)
                ) &&
                    (
                        Safe.contains(matchedRawElements, target) ||
                            Safe.contains(deletedRawElements, target) ||
                            Safe.contains(addedRawElements, target)
                    ) &&
                    !Safe.contains(matchedRawElements, sf) &&
                    !Safe.contains(deletedRawElements, sf) &&
                    !Safe.contains(addedRawElements, sf)
            ) {
                boolean pairFound = false;

                if (
                    Safe.contains(matchedRawElements, source) &&
                        Safe.contains(matchedRawElements, target)
                ) {
                    Collection<BPMNElement> secondModelElements = BPMNUtils.getElementsByType(secondBpmnModel, BPMNElementType.SequenceFlow);

                    for (BPMNElement secondModelElement : secondModelElements) {
                        if (comparisonResult.getValuesFromMatchingSet().stream().noneMatch(it -> it.getRaw() == secondModelElement.getRaw())) {
                            MatchingPair matchingPair = comparisonResult.getCostConfig().getValidComparator(BPMNElementType.SequenceFlow).compare(
                                comparisonResult,
                                firstBpmnModel,
                                secondBpmnModel,
                                sfElement,
                                secondModelElement
                            );

                            matchingPair.setType(PairType.MATCH);

                            if (matchingPair.isLinkSave()) {
                                comparisonResult.updateMatching(matchingPair);
                                pairFound = true;
                                break;
                            }
                        }
                    }
                }

                if (!pairFound) {
                    double d = comparisonResult.getCostConfig().getDeleteCostFor(BPMNElementType.SequenceFlow);
                    MatchingPair matchingPair = new MatchingPair(new BPMNElement(sf), null, PairType.DELETE);
                    matchingPair.setDifferentLabel(d);
                    comparisonResult.updateDeleting(matchingPair);
                    comparisonResult.updateHeuristic();
                }
            }
        }
    }

}

package ru.pais.vkr.services.model_comparators

import org.camunda.bpm.model.bpmn.BpmnModelInstance
import ru.pais.vkr.comparator.entities.ComparisonResult
import ru.pais.vkr.comparator.entities.CostConfig

abstract class ModelComparator {
    open class ParameterRaw(
        val name: String,
        val defaultValue: Any
    )

    class Parameter<T : Any>(name: String, defaultValue: T) : ParameterRaw(name, defaultValue) {
        fun valueFrom(inputs: Map<String, Any>): T {
            @Suppress("UNCHECKED_CAST")
            return (inputs[name] ?: defaultValue) as T
        }

        fun pairWith(value: T): Pair<String, T> {
            return name to value
        }
    }

    class Statistic<T : Any>(val name: String) {
        fun fill(map: MutableMap<String, Any>, value: T) {
            map[name] = value
        }

        fun valueFrom(map: Map<String, Any>): T {
            return map.getValue(name) as T
        }
    }

    /**
     * Returns comparator type
     */
    abstract val type: ModelComparatorType

    /**
     * Returns a list of all the input parameters with the default values
     */
    abstract val parameters: List<ParameterRaw>

    /**
     * @param firstBpmnModel the first model
     * @param secondBpmnModel the second model
     * @return list of differences
     */
    abstract fun compareModels(
        firstBpmnModel: BpmnModelInstance,
        secondBpmnModel: BpmnModelInstance,
        costConfig: CostConfig,
        inputParameters: Map<String, Any>,
        useParallelization: Boolean
    ): ComparisonResult

    protected object CommonStatistics {
        @JvmField
        val SOURCE_GRAPH_VERTICES = Statistic<Int>("Number of vertices in the source graph")
        @JvmField
        val TARGET_GRAPH_VERTICES = Statistic<Int>("Number of vertices in the target graph")
        @JvmField
        val SOURCE_GRAPH_EDGES = Statistic<Int>("Number of edges in the source graph")
        @JvmField
        val TARGET_GRAPH_EDGES = Statistic<Int>("Number of edges in the target graph")
        @JvmField
        val TOTAL_TIME_MILLIS = Statistic<Int>("Total time (ms)")
        @JvmField
        val GREEDY_TOTAL_TIME_MS = Statistic<Int>("Greedy total time (ms)")
    }

    protected fun parametersFrom(parametersObject: Any): List<ParameterRaw> {
        return parametersObject.javaClass.declaredMethods.map { it.invoke(parametersObject) as ParameterRaw }
    }
}
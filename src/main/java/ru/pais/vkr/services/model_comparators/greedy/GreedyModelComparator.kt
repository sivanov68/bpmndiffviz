package ru.pais.vkr.services.model_comparators.greedy

import org.camunda.bpm.model.bpmn.BpmnModelInstance
import ru.pais.vkr.comparator.entities.BPMNElementType
import ru.pais.vkr.comparator.entities.ComparisonResult
import ru.pais.vkr.comparator.entities.CostConfig
import ru.pais.vkr.comparison_changes.EdgeChange
import ru.pais.vkr.comparison_changes.VertexChange
import ru.pais.vkr.graph_elements.Edge
import ru.pais.vkr.graph_elements.Graph
import ru.pais.vkr.graph_elements.Vertex
import ru.pais.vkr.services.model_comparators.ModelComparator
import ru.pais.vkr.services.model_comparators.ModelComparatorType
import ru.pais.vkr.utils.*
import kotlin.coroutines.experimental.buildSequence

object GreedyModelComparator : ModelComparator() {
    data class ComparisonOutput(
        val changes: List<ChangeStep>,
        val stepsCount: Int,
        val averageStepTimeMillis: Int,
        val totalTimeMillis: Int
    )

    @Suppress("RemoveExplicitTypeArguments")
    object Parameters {
        val USE_TYPE_PRIORITY = Parameter<Boolean>("Match not gateway elements first", true)
        val USE_ALREADY_MATCHED_PRIORITY = Parameter<Boolean>("Expand elements that has more matched neighbors first", false)
    }

    object Statistics {
        val SOURCE_GRAPH_VERTICES = CommonStatistics.SOURCE_GRAPH_VERTICES
        val TARGET_GRAPH_VERTICES = CommonStatistics.TARGET_GRAPH_VERTICES
        val SOURCE_GRAPH_EDGES = CommonStatistics.SOURCE_GRAPH_EDGES
        val TARGET_GRAPH_EDGES = CommonStatistics.TARGET_GRAPH_EDGES
        val STEPS_COUNT = Statistic<Int>("Steps performed")
        val AVERAGE_STEP_TIME_MILLIS = Statistic<Int>("Average step time (ms)")
        val TOTAL_TIME_MILLIS = CommonStatistics.TOTAL_TIME_MILLIS
    }

    override val type: ModelComparatorType = ModelComparatorType.Greedy

    override val parameters: List<ParameterRaw> = parametersFrom(Parameters)

    /*
        Операции:
            * Сопоставление вершины
            * Удаление вершины из исходного графа
            * Добавление вершины в целевой граф
            * Сопоставление ребра
            * Удаление ребра из исходного графа
            * Добавление ребра в целевой граф
        Условия:
            Вершины отличаются друг от друга типом и названием. Сопоставлять можно только вершины с одинаковыми типами. Стоимость сопоставления состоит из различия в именах и суммы различий в сопутствующих рёбрах. Стоимость удаления и добавления отдельной вершины задаётся константой.
            Рёбра отличаются друг от друга типом и названием. Сопоставлять можно только рёбра с одинаковыми типами. Стоимость сопоставления рёбер состоит из различия в их именах. Стоимость удаления и добавления отдельного ребра задаётся константой.
            Поддерживаются только однонаправленные и циклические рёбра. При этом у отдельно взятой вершины может быть максимум одна циклическая ссылка. Между двумя сопостовляемыми вершинами может быть максимум по одному однонаправленному ребру, исходящему из каждой вершины. Другими словами, при наличии вершин 1 и 2, максимальный набор рёбер, который может существовать это { (1 -> 1), (2 -> 2), (1 -> 2), (2 -> 1) }
            Некоторые части алгоритма всё же поддерживают некоторые из неподдерживаемых в общем случае условий, таких как множество цикличных ссылок, двунаправленные рёбра и многочисленные однонаправленные рёбра, но эта поодержка ограничивается лишь отдельными частями алгоритма, и в общем случае приводит к неопределённому поведению.
     */
    override fun compareModels(
        firstBpmnModel: BpmnModelInstance,
        secondBpmnModel: BpmnModelInstance,
        costConfig: CostConfig,
        inputParameters: Map<String, Any>,
        useParallelization: Boolean
    ): ComparisonResult {
        val sourceGraph: Graph = firstBpmnModel.toGraph()
        val targetGraph: Graph = secondBpmnModel.toGraph()

        val sourceVerticesCount: Int = sourceGraph.vertices.size
        val sourceEdgesCount: Int = sourceGraph.edges.size
        val targetVerticesCount: Int = targetGraph.vertices.size
        val targetEdgesCount: Int = targetGraph.edges.size

        val output: ComparisonOutput = compareNew(
            sourceGraph,
            targetGraph,
            costConfig,
            Parameters.USE_TYPE_PRIORITY.valueFrom(inputParameters),
            Parameters.USE_ALREADY_MATCHED_PRIORITY.valueFrom(inputParameters)
        )

        return ModelComparatorUtils.constructComparisonResult(
            firstBpmnModel,
            secondBpmnModel,
            output.changes.map { it.vertex },
            output.changes.flatMap { it.edges },
            costConfig
        ).apply {
            with (Statistics) {
                SOURCE_GRAPH_VERTICES.fill(statistics, sourceVerticesCount)
                SOURCE_GRAPH_EDGES.fill(statistics, sourceEdgesCount)
                TARGET_GRAPH_VERTICES.fill(statistics, targetVerticesCount)
                TARGET_GRAPH_EDGES.fill(statistics, targetEdgesCount)
                STEPS_COUNT.fill(statistics, output.stepsCount)
                AVERAGE_STEP_TIME_MILLIS.fill(statistics, output.averageStepTimeMillis)
                TOTAL_TIME_MILLIS.fill(statistics, output.totalTimeMillis)
            }
        }
    }

    fun compareNew(
        sourceGraph: Graph,
        targetGraph: Graph,
        costConfig: CostConfig,
        useTypePriority: Boolean,
        useAlreadyMatchedPriority: Boolean
    ): ComparisonOutput {
        var sourceBackup: Graph? = null
        var targetBackup: Graph? = null

        executeIfAssert {
            sourceBackup = sourceGraph.clone()
            targetBackup = targetGraph.clone()
        }

        val changesHistory: MutableList<ChangeStep> = mutableListOf()

        val stepTimes: MutableList<Long> = mutableListOf()

        while (sourceGraph.vertices.isNotEmpty() || targetGraph.vertices.isNotEmpty()) {
            val stepStartMillis: Long = System.currentTimeMillis()

            // contains all possible transformations from the current state to the next one
            // after it is filled the elements with the smallest penalty will be added to the `previousChangesHistory`
            val changesVariants: Sequence<ChangeStep> = generateAllVariants(
                sourceGraph,
                targetGraph,
                changesHistory,
                costConfig
            )

            // has to be not null as we must have at least one match when both collections are not empty
            val minChange: ChangeStep

            if (!useTypePriority && !useAlreadyMatchedPriority) {
                minChange = changesVariants.minBy { it.totalCost }!!
            } else if (!useTypePriority /*&& useAlreadyMatchedPriority*/) {
                var maxMatched: Int = -1
                var maxMatchedMinCost: ChangeStep? = null
                var addingVariant: ChangeStep? = null
                for (variant: ChangeStep in changesVariants) {
                    if (variant.vertex.source != null) {
                        // deleting vertex or matching
                        val matchedVertices: Int = matchedNeighborsMetric(changesHistory, variant)

                        if (matchedVertices == maxMatched) {
                            if (variant.totalCost < maxMatchedMinCost!!.totalCost) {
                                maxMatchedMinCost = variant
                            }
                        } else if (matchedVertices > maxMatched) {
                            maxMatchedMinCost = variant
                            maxMatched = matchedVertices
                        }
                    } else /*if (variant.vertex.source == null && variant.vertex.target != null)*/ {
                        // adding vertex
                        if (addingVariant == null || variant.totalCost < addingVariant.totalCost) {
                            addingVariant = variant
                        }
                    }
                }

                minChange = maxMatchedMinCost ?: addingVariant!!
            } else if (/*useTypePriority &&*/ !useAlreadyMatchedPriority) {
                // processing items from the source graph first
                // choosing the variant with the highest type priority
                // even if have a variant with the lower priority but with the lower total cost

                var bestSourceVertexMatch: ChangeStep? = null
                var bestSourceVertexPriority: Int = -1
                var bestTargetVertexMatch: ChangeStep? = null
                var bestTargetVertexPriority: Int = -1
                for (variant in changesVariants) {
                    val priority: Int = variantTypePriority(variant)
                    if (variant.vertex.source != null) {
                        // processing source graph's vertex
                        if (bestSourceVertexMatch == null ||
                            priority > bestSourceVertexPriority ||
                            (priority == bestSourceVertexPriority && variant.totalCost < bestSourceVertexMatch.totalCost)
                        ) {
                            bestSourceVertexMatch = variant
                            bestSourceVertexPriority = priority
                        }
                    } else {
                        // processing target graph's vertex adding
                        if (bestTargetVertexMatch == null ||
                            priority > bestTargetVertexPriority ||
                            (priority == bestTargetVertexPriority && variant.totalCost < bestTargetVertexMatch.totalCost)
                        ) {
                            bestTargetVertexMatch = variant
                            bestTargetVertexPriority = priority
                        }
                    }
                }

                minChange = bestSourceVertexMatch ?: bestTargetVertexMatch!!
            } else /*if (useTypePriority && useAlreadyMatchedPriority)*/ {
                // processing items from the source graph first
                // choosing the variant with the highest type priority
                // even if have a variant with the lower priority but with the lower total cost

                var bestSourceVertexMatch: ChangeStep? = null
                var bestSourceVertexPriority: Int = -1
                var bestSourceVertexNeighbors: Int = -1
                var bestTargetVertexMatch: ChangeStep? = null
                var bestTargetVertexPriority: Int = -1

                for (variant in changesVariants) {
                    val typePriority: Int = variantTypePriority(variant)
                    if (variant.vertex.source != null) {
                        // processing source graph's vertex
                        val neighborsMetric: Int = matchedNeighborsMetric(changesHistory, variant)

                        if (
                            bestSourceVertexMatch == null ||
                            typePriority > bestSourceVertexPriority ||
                            (
                                typePriority == bestSourceVertexPriority &&
                                (
                                    neighborsMetric > bestSourceVertexNeighbors ||
                                    (neighborsMetric == bestSourceVertexNeighbors && variant.totalCost < bestSourceVertexMatch.totalCost)
                                )
                            )
                        ) {
                            bestSourceVertexMatch = variant
                            bestSourceVertexPriority = typePriority
                            bestSourceVertexNeighbors = neighborsMetric
                        }
                    } else {
                        // processing target graph's vertex adding
                        if (bestTargetVertexMatch == null ||
                            typePriority > bestTargetVertexPriority ||
                            (typePriority == bestTargetVertexPriority && variant.totalCost < bestTargetVertexMatch.totalCost)
                        ) {
                            bestTargetVertexMatch = variant
                            bestTargetVertexPriority = typePriority
                        }
                    }
                }

                minChange = bestSourceVertexMatch ?: bestTargetVertexMatch!!
            }

            // removing matched vertices and edges from the graphs
            with(minChange.vertex) {
                source?.let { sourceGraph.vertices.remove(it) }
                target?.let { targetGraph.vertices.remove(it) }
                return@with
            }

            for (edgeChange: EdgeChange in minChange.edges) {
                with(edgeChange) {
                    source?.let { sourceGraph.edges.remove(it) }
                    target?.let { targetGraph.edges.remove(it) }
                    return@with
                }
            }

            changesHistory.add(minChange)

            val stepTime: Long = System.currentTimeMillis() - stepStartMillis
            stepTimes.add(stepTime)
        }

        executeIfAssert {
            val sourceMatched = changesHistory.mapNotNull { it.vertex.source }.toSet()
            val targetMatched = changesHistory.mapNotNull { it.vertex.target }.toSet()

            assertCustom { sourceBackup!!.vertices.all { it in sourceMatched } }
            assertCustom { targetBackup!!.vertices.all { it in targetMatched } }

            if (sourceGraph.edges.isNotEmpty() || targetGraph.edges.isNotEmpty()) {
                if (sourceGraph.edges.isNotEmpty()) {
                    assertCustom { sourceGraph.edges.all { it.source in sourceMatched && it.target in sourceMatched } }
                }
                if (targetGraph.edges.isNotEmpty()) {
                    assertCustom { targetGraph.edges.all { it.source in targetMatched && it.target in targetMatched } }
                }

                throw IllegalStateException("there must be no edges after comparison")
            }
        }

        val totalTimeMillis: Long = stepTimes.sum()

        return ComparisonOutput(
            changes = changesHistory,
            stepsCount = stepTimes.size,
            averageStepTimeMillis = if (stepTimes.isNotEmpty()) (totalTimeMillis / stepTimes.size).toInt() else 0,
            totalTimeMillis = totalTimeMillis.toInt()
        )
    }

    fun generateAllVariants(
        sourceGraph: Graph,
        targetGraph: Graph,
        changesHistory: List<ChangeStep>,
        costConfig: CostConfig
    ): Sequence<ChangeStep> = buildSequence {
        // deleting one vertex from the first graph
        // features:
        //      [+] cyclic edges
        //      [+] bidirectional edges
        //      [+] multiple edges of the same type with equal side vertices
        for (sourceVertex: Vertex in sourceGraph.vertices) {
            val vertexChange: VertexChange = VertexChange.of(sourceVertex, null, costConfig)

            val allEdgeChanges = ArrayList<EdgeChange>()

            for (edge in sourceGraph.edges) {
                if (
                    /* cyclic */ (
                        edge.source == sourceVertex && edge.target == sourceVertex
                    ) ||
                    /* in */ (
                        edge.target == sourceVertex && edge.source != sourceVertex
                    ) ||
                    /* out */ (
                        edge.source == sourceVertex && edge.target != sourceVertex
                    )
                ) {
                    allEdgeChanges.add(EdgeChange.of(edge, null, costConfig))
                }
            }

            yield(ChangeStep(vertexChange, allEdgeChanges))
        }

        // matching one vertex from the first graph to the vertex of the same type in the second graph
        // features:
        //      [+] cyclic edges
        //      [-] bidirectional edges
        //      [+] multiple edges of the same type with equal side vertices
        for (sourceVertex: Vertex in sourceGraph.vertices) {
            for (targetVertex: Vertex in targetGraph.vertices) {
                if (sourceVertex.element.type != targetVertex.element.type)
                    continue

                val vertexChange: VertexChange = VertexChange.of(sourceVertex, targetVertex, costConfig)

                // features:
                //      [+] cyclic edges
                //      [-] bidirectional edges
                //      [+] multiple edges of the same type with equal side vertices
                val edgeChanges: List<EdgeChange> = minimumEdgeChanges(
                    sourceGraph,
                    targetGraph,
                    sourceVertex,
                    targetVertex,
                    changesHistory,
                    costConfig
                )

                yield(ChangeStep(vertexChange, edgeChanges))
            }
        }

        // adding one vertex to the second graph
        // handling only edges one side of which was already matched
        // features:
        //      [+] cyclic edges
        //      [+] bidirectional edges
        //      [+] multiple edges of the same type with equal side vertices
        val matchedVerticesFromTarget: Set<Vertex> = changesHistory.asSequence().mapNotNull { it.vertex.target }.toSet()

        for (targetVertex: Vertex in targetGraph.vertices) {
            val vertexChange: VertexChange = VertexChange.of(null, targetVertex, costConfig)

            val allEdgeChanges = ArrayList<EdgeChange>()

            for (edge in targetGraph.edges) {
                if (
                    /* cyclic */ (
                        edge.source == targetVertex && edge.target == targetVertex
                    ) ||
                    /* in */ (
                        edge.source != targetVertex && edge.target == targetVertex &&
                        edge.source in matchedVerticesFromTarget
                    ) ||
                    /* out */ (
                        edge.source == targetVertex && edge.target != targetVertex &&
                        edge.target in matchedVerticesFromTarget
                    )
                ) {
                    allEdgeChanges.add(EdgeChange.of(null, edge, costConfig))
                }
            }

            yield(ChangeStep(vertexChange, allEdgeChanges))
        }
    }

    // features:
    //      [+] cyclic edges
    //      [-] bidirectional edges
    //      [+] multiple edges of the same type with equal side vertices
    private fun minimumEdgeChanges(
        sourceGraph: Graph,
        targetGraph: Graph,
        sourceVertex: Vertex,
        targetVertex: Vertex,
        changesHistory: List<ChangeStep>,
        costConfig: CostConfig
    ): List<EdgeChange> {
        if (sourceVertex.element.type != targetVertex.element.type)
            throw IllegalArgumentException("can't process a match of elements of different types")

        // we don't have an operation that matches edges of different types
        // that's why we will be matching edges only inside their groups
        val sourceEdgesByType: Map<BPMNElementType, List<Edge>> = sourceGraph.edges.groupBy { it.element.type }
        val targetEdgesByType: Map<BPMNElementType, List<Edge>> = targetGraph.edges.groupBy { it.element.type }

        // this variable will contain the best possible variant of matching the edges
        val minimumEdgeChanges: MutableList<EdgeChange> = mutableListOf()

        val matchedVertices: List<VertexChange> = changesHistory.map { it.vertex }

        for (edgeType: BPMNElementType in sourceEdgesByType.keys + targetEdgesByType.keys) {
            val sourceEdges: List<Edge> = sourceEdgesByType.getOrElse(edgeType, ::emptyList)
            val targetEdges: List<Edge> = targetEdgesByType.getOrElse(edgeType, ::emptyList)

            // processing cyclic edges
            val cyclicSource: Edge? = sourceEdges.firstOrNull { it.source == sourceVertex && it.target == sourceVertex }
            val cyclicTarget: Edge? = targetEdges.firstOrNull { it.source == targetVertex && it.target == targetVertex }

            assertCustom(
                { sourceEdges.count { it.source == sourceVertex && it.target == sourceVertex } in 0..1 },
                { "we can handle no more than a one edge" }
            )
            assertCustom(
                { targetEdges.count { it.source == targetVertex && it.target == targetVertex } in 0..1 },
                { "we can handle no more than a one edge" }
            )

            minimumEdgeChanges.addAll(
                GraphUtils.minEdgeReplacementList(
                    cyclicSource,
                    cyclicTarget,
                    costConfig
                )
            )
            // ---

            val sourceNonCyclicEdges: List<Edge> = sourceEdges.filter { it.source != it.target }
            val targetNonCyclicEdges: List<Edge> = targetEdges.filter { it.source != it.target }

            val matchedSources: Set<Vertex> = matchedVertices.asSequence().mapNotNull { it.source }.toSet()
            val matchedTargets: Set<Vertex> = matchedVertices.asSequence().mapNotNull { it.target }.toSet()

            // processing incoming edges
            val inSource = sourceNonCyclicEdges.filter {
                it.target == sourceVertex && it.source in matchedSources
            }
            val inTarget = targetNonCyclicEdges.filter {
                it.target == targetVertex && it.source in matchedTargets
            }.toMutableList()

            for (sourceEdge: Edge in inSource) {
                val sourceMapped = matchedVertices.first { it.source == sourceEdge.source }.target
                assertCustom { matchedVertices.count { it.source == sourceEdge.source } == 1 }

                val toMatch: Edge? = inTarget.firstOrNull { it.source == sourceMapped }
                assertCustom { inTarget.count { it.source == sourceMapped } in 0..1 }

                minimumEdgeChanges.addAll(
                    GraphUtils.minEdgeReplacementList(sourceEdge, toMatch, costConfig)
                )

                toMatch?.let { inTarget.remove(it) }
            }

            inTarget.forEach { minimumEdgeChanges.add(EdgeChange.of(null, it, costConfig)) }
            // ---

            // processing outgoing edges
            val outSource = sourceNonCyclicEdges.filter {
                it.source == sourceVertex && it.target in matchedSources
            }
            val outTarget = targetNonCyclicEdges.filter {
                it.source == targetVertex && it.target in matchedTargets
            }.toMutableList()

            for (sourceEdge: Edge in outSource) {
                val targetMapped = matchedVertices.first { it.source == sourceEdge.target }.target
                assertCustom { matchedVertices.count { it.source == sourceEdge.target } == 1 }

                val toMatch: Edge? = outTarget.firstOrNull { it.target == targetMapped }
                assertCustom { outTarget.count { it.target == targetMapped } in 0..1 }

                minimumEdgeChanges.addAll(
                    GraphUtils.minEdgeReplacementList(sourceEdge, toMatch, costConfig)
                )

                toMatch?.let { outTarget.remove(it) }
            }

            outTarget.forEach { minimumEdgeChanges.add(EdgeChange.of(null, it, costConfig)) }
            // ---
        }

        return minimumEdgeChanges
    }

    private fun variantTypePriority(variant: ChangeStep): Int {
        return when (variant.vertex.source?.element?.type ?: variant.vertex.target!!.element.type) {
            BPMNElementType.ComplexGateway,
            BPMNElementType.EventBasedGateway,
            BPMNElementType.ExclusiveGateway,
            BPMNElementType.InclusiveGateway,
            BPMNElementType.ParallelGateway -> 0
            else -> 1
        }
    }

    private fun matchedNeighborsMetric(changesHistory: List<ChangeStep>, variant: ChangeStep): Int {
        return variant.edges.count { edgeChange: EdgeChange ->
            val edge: Edge = edgeChange.source ?: return@count false
            val otherVertex: Vertex = if (edge.source == variant.vertex.source) edge.target else edge.source
            return@count changesHistory.any { it.vertex.source == otherVertex }
        }
    }
}
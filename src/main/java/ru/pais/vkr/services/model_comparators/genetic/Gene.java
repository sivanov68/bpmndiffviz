package ru.pais.vkr.services.model_comparators.genetic;

import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import ru.pais.vkr.comparator.entities.*;

import java.util.*;

public class Gene extends ComparisonResult {

    private static final double crossingoverFactor = 0.5;

    private Chromosome[] chromosomesSource;
    private BPMNElement[] flowsSource;

    private Chromosome[] chromosomesTarget;
    private BPMNElement[] flowsTarget;

    public Gene(
        BpmnModelInstance bpmnModelInstance1,
        BpmnModelInstance bpmnModelInstance2,
        CostConfig costConfig,
        List<BPMNElementType> comparingClassList,
        List<BPMNElementType> allClasseList
    ) {
        super(bpmnModelInstance1, bpmnModelInstance2, comparingClassList, allClasseList, costConfig);
        Collection<BPMNElement> modelSourceElements = GeneticModelComparator.getAllModelElements(bpmnModelInstance1);
        int nodeLength = 0, flowLength = 0;
        for (BPMNElement modelSourceElement : modelSourceElements) {
            if (GeneticModelComparator.isFlowElement(modelSourceElement.getType()))
                ++flowLength;
            else
                ++nodeLength;
        }
        chromosomesSource = new Chromosome[nodeLength];
        flowsSource = new BPMNElement[flowLength];
        int nposition = 0, fposition = 0;
        for (Iterator<BPMNElement> iterator = modelSourceElements.iterator(); iterator.hasNext();) {
            BPMNElement modelElementInstance = iterator.next();
            if(GeneticModelComparator.isFlowElement(modelElementInstance.getType())) {
                flowsSource[fposition++] = modelElementInstance;
            }
            else{
                double d = costConfig.getDeleteCostFor(modelElementInstance.getType());
                MatchingPair matchingPair = new MatchingPair(modelElementInstance, null, PairType.DELETE);
                matchingPair.setDifferentLabel(d);
                chromosomesSource[nposition] = new Chromosome(matchingPair, nposition);
                ++nposition;
            }
        }
        nodeLength = 0; flowLength = 0;
        nposition = 0; fposition = 0;
        Collection<BPMNElement> modelTargetElements =  GeneticModelComparator.getAllModelElements(bpmnModelInstance2);
        for (BPMNElement modelTargetElement : modelTargetElements) {
            if (GeneticModelComparator.isFlowElement(modelTargetElement.getType()))
                ++flowLength;
            else
                ++nodeLength;
        }
        chromosomesTarget = new Chromosome[nodeLength];
        flowsTarget = new BPMNElement[flowLength];
        for (Iterator<BPMNElement> iterator = modelTargetElements.iterator(); iterator.hasNext(); ) {
            BPMNElement modelElementInstance = iterator.next();
            if(GeneticModelComparator.isFlowElement(modelElementInstance.getType())) {
                flowsTarget[fposition++] = modelElementInstance;
            }
            else {
                double d = costConfig.getInsertCostFor(modelElementInstance.getType());
                MatchingPair matchingPair = new MatchingPair(modelElementInstance, null, PairType.INSERT);
                matchingPair.setDifferentLabel(d);
                chromosomesTarget[nposition] = new Chromosome(matchingPair, nposition);
                ++nposition;
            }
        }
        updateCost();
    }

    public Gene(Gene prototype, BPMNElement elementSource, BPMNElement elementTarget){
        super(prototype.getFirstBpmnModel(), prototype.getSecondBpmnModel(), prototype.getComparingClassList(), prototype.getAddedClassList(), prototype.getCostConfig());
        chromosomesSource = null;
        chromosomesTarget = null;
        int sourceGenPosition = -1;
        int targetGenPosition = -1;
        flowsSource = new BPMNElement[prototype.flowsSource.length];
        System.arraycopy(prototype.flowsSource, 0, flowsSource, 0, flowsSource.length);
        chromosomesSource = new Chromosome[prototype.chromosomesSource.length];
        for (int i = 0; i < chromosomesSource.length; ++i) {
            chromosomesSource[i] = prototype.chromosomesSource[i];
            if (Objects.equals(chromosomesSource[i].getFirst(), elementSource))
                sourceGenPosition = i;
        }
        flowsTarget = new BPMNElement[prototype.flowsTarget.length];
        System.arraycopy(prototype.flowsTarget, 0, flowsTarget, 0, flowsTarget.length);
        chromosomesTarget = new Chromosome[prototype.chromosomesTarget.length];
        for (int i = 0; i < chromosomesTarget.length; ++i) {
            chromosomesTarget[i] = prototype.chromosomesTarget[i];
            if (Objects.equals(chromosomesTarget[i].getFirst(), elementTarget))
                targetGenPosition = i;
        }
        createPair(elementSource, elementTarget, sourceGenPosition, targetGenPosition);
        updateCost();
    }

    public Gene(Gene prototype){
        super(prototype.getFirstBpmnModel(), prototype.getSecondBpmnModel(), prototype.getComparingClassList(), prototype.getAddedClassList(), prototype.getCostConfig());
        flowsSource = new BPMNElement[prototype.flowsSource.length];
        System.arraycopy(prototype.flowsSource, 0, flowsSource, 0, flowsSource.length);
        chromosomesSource = new Chromosome[prototype.chromosomesSource.length];
        for (int i = 0; i < chromosomesSource.length; ++i)
            chromosomesSource[i] = null;
        flowsTarget = new BPMNElement[prototype.flowsTarget.length];
        System.arraycopy(prototype.flowsTarget, 0, flowsTarget, 0, flowsTarget.length);
        chromosomesTarget = new Chromosome[prototype.chromosomesTarget.length];
        for (int i = 0; i < chromosomesTarget.length; ++i)
            chromosomesTarget[i] = null;
    }

    private void createPair(BPMNElement elementSource, BPMNElement elementTarget, int sourceGenPosition, int targetGenPosition){
        if(sourceGenPosition == -1 || targetGenPosition == -1)
            return;
        MatchingPair matchingPair = getCostConfig().getValidComparator(elementSource.getType()).compare(
                this,
                getFirstBpmnModel(),
                getSecondBpmnModel(),
                elementSource,
                elementTarget);
        matchingPair.setType(PairType.MATCH);
        if(matchingPair.isLinkSave()) {
            Chromosome sourceChromosome = new Chromosome(matchingPair, sourceGenPosition);
            Chromosome targetChromosome = new Chromosome(matchingPair, targetGenPosition);
            sourceChromosome.setAssociation(targetChromosome);
            targetChromosome.setAssociation(sourceChromosome);
            targetChromosome.setDuplicate(true);
            chromosomesSource[sourceGenPosition] = sourceChromosome;
            chromosomesTarget[targetGenPosition] = targetChromosome;
        }
    }
    
    private boolean isMatching(BPMNElement element1, BPMNElement element2){
        return element1.getType() == element2.getType();
    }

    static public Offspring crossingover(Gene gene1, Gene gene2) {
        Random random = new Random();
        Gene child1 = new Gene(gene1);
        Gene child2 = new Gene(gene1);
        setBundles(child1, child2, gene1, gene2);
        Chromosome chromosome1, chromosome2;
        for (int i = 0; i < child1.chromosomesSource.length; ++i){
            if(child1.chromosomesSource[i] != null && child2.chromosomesSource[i] != null)
                continue;
            chromosome1 = gene1.chromosomesSource[i];
            chromosome2 = gene2.chromosomesSource[i];
            if (child1.chromosomesSource[i] == null && child2.chromosomesSource[i] != null)
                setChromosome(child1, chromosome1);
            else if (child1.chromosomesSource[i] != null && child2.chromosomesSource[i] == null)
                setChromosome(child2, chromosome2);
            else if (random.nextDouble() < crossingoverFactor)
                setFromChromosomes(child1, child2, chromosome1, chromosome2);
            else
                setFromChromosomes(child1, child2, chromosome2, chromosome1);
        }
        for (int i = 0; i < child1.chromosomesTarget.length; ++i) {
            chromosome1 = gene1.chromosomesTarget[i];
            chromosome2 = gene2.chromosomesTarget[i];
            setTargetChromosomes(child1, child2, chromosome1, chromosome2);
        }
        child1.updateCost();
        child2.updateCost();
        return new Offspring(child1, child2);
    }

    private static void setBundles(Gene child1, Gene child2, Gene gene1, Gene gene2){
        Chromosome chromosome1, chromosome2, bundle1, bundle2;
        for (int i = 0; i < child1.chromosomesSource.length; ++i) {
            chromosome1 = gene1.chromosomesSource[i];
            chromosome2 = gene2.chromosomesSource[i];
            if(chromosome1.isAssociated() && chromosome2.isAssociated()){
                bundle2 = gene2.chromosomesTarget[chromosome1.getAssociation().getGenPosition()].getAssociation();
                bundle1 = gene1.chromosomesTarget[chromosome2.getAssociation().getGenPosition()].getAssociation();
                if (bundle1 == null && bundle2 == null) continue;
                setChromosome(child1, chromosome1);
                setChromosome(child2, chromosome2);
                setChromosome(child1, bundle1);
                setChromosome(child2, bundle2);
            }
        }
    }

    private static boolean canSetChromosome(Gene gene, Chromosome chromosome) {
        return !chromosome.isAssociated() || gene.chromosomesTarget[chromosome.getAssociation().getGenPosition()] == null;
    }

    private static void setChromosome(Gene gene, Chromosome chromosome) {
        if (gene == null || chromosome == null) return;
        if (!chromosome.isDuplicate()) {
            if (chromosome.isAssociated() && gene.chromosomesSource[chromosome.getGenPosition()] == null) {
                gene.chromosomesTarget[chromosome.getAssociation().getGenPosition()] = chromosome.getAssociation();
                gene.chromosomesSource[chromosome.getGenPosition()] = chromosome;
            } else if (chromosome.getType() == PairType.DELETE && gene.chromosomesSource[chromosome.getGenPosition()] == null) {
                gene.chromosomesSource[chromosome.getGenPosition()] = chromosome;
            } else if (chromosome.getType() == PairType.INSERT && gene.chromosomesTarget[chromosome.getGenPosition()] == null) {
                gene.chromosomesTarget[chromosome.getGenPosition()] = chromosome;
            }
        }
    }

    private static void setFromChromosomes(Gene gene1, Gene gene2, Chromosome chromosome1, Chromosome chromosome2){
        if (canSetChromosome(gene1, chromosome1) && canSetChromosome(gene2, chromosome2)) {
            setChromosome(gene1,chromosome1);
            setChromosome(gene2,chromosome2);
        } else if (canSetChromosome(gene1, chromosome2) && canSetChromosome(gene2, chromosome1)) {
            setChromosome(gene1,chromosome2);
            setChromosome(gene2,chromosome1);
        }
    }

    private static void setTargetChromosomes(Gene gene1, Gene gene2, Chromosome chromosome1, Chromosome chromosome2){
        if (!chromosome1.isAssociated()) {
            setChromosome(gene1, chromosome1);
            setChromosome(gene2, chromosome1);
        } else if (!chromosome2.isAssociated()) {
            setChromosome(gene1, chromosome2);
            setChromosome(gene2, chromosome2);
        }
    }

    public boolean isEmpty(){
        return chromosomesSource == null || chromosomesTarget == null;
    }

    private void updateCost(){
        double penalty = 0;
        fillComparisonResult();
        for (int i = 0; i < chromosomesSource.length; ++i) {
            if(chromosomesSource[i].isAssociated()){
                createPair(chromosomesSource[i].getFirst(), chromosomesSource[i].getSecond(),
                        chromosomesSource[i].getGenPosition(), chromosomesSource[i].getAssociation().getGenPosition());
            }
            else{
                double d = getCostConfig().getDeleteCostFor(chromosomesSource[i].getFirst().getType());
                MatchingPair matchingPair = new MatchingPair(chromosomesSource[i].getFirst(), null, PairType.DELETE);
                matchingPair.setDifferentLabel(d);
                chromosomesSource[i] = new Chromosome(matchingPair, i);
            }
            penalty += chromosomesSource[i].isDuplicate() ? 0 : chromosomesSource[i].getCost();
        }
        for (int i = 0; i < chromosomesTarget.length; ++i) {
            if(!chromosomesTarget[i].isAssociated()) {
                double d = getCostConfig().getInsertCostFor(chromosomesTarget[i].getFirst().getType());
                MatchingPair matchingPair = new MatchingPair(chromosomesTarget[i].getFirst(), null, PairType.INSERT);
                matchingPair.setDifferentLabel(d);
                chromosomesTarget[i] = new Chromosome(matchingPair, i);
            }
            penalty += chromosomesTarget[i].isDuplicate() ? 0 : chromosomesTarget[i].getCost();
        }
        fillComparisonResult();
        penalty += getFlowCost();
        setPenalty(penalty);
        setHeuristic(0);
    }

    private void fillComparisonResult(){
        getAddedSet().clear();
        getMatchingSet().clear();
        getDeletedSet().clear();
        for (Chromosome chromosome : chromosomesSource) {
            if (chromosome.isAssociated())
                getMatchingSet().add(chromosome);
            else
                getDeletedSet().add(chromosome);
        }
        for (Chromosome chromosome : chromosomesTarget) {
            if (!chromosome.isDuplicate())
                getAddedSet().add(chromosome);
        }
    }

    private double getFlowCost(){
        double cost = 0;
        boolean[] matchedSource = new boolean[flowsSource.length];
        Arrays.fill(matchedSource, true);
        boolean[] matchedTarget = new boolean[flowsTarget.length];
        Arrays.fill(matchedTarget, true);
        for(int i = 0; i < flowsSource.length; ++i){
            for(int j = 0; j < flowsTarget.length; ++j){
                if(isMatching(flowsSource[i], flowsTarget[j]) && matchedTarget[j] && matchedSource[i]) {
                    MatchingPair matchingPair = getCostConfig().getValidComparator(flowsSource[i].getType()).compare(
                            this,
                            getFirstBpmnModel(),
                            getSecondBpmnModel(),
                            flowsSource[i],
                            flowsTarget[j]);
                    if(matchingPair.isLinkSave()){
                        cost += matchingPair.getDifferentLabel();
                        matchedSource[i] = false;
                        matchedTarget[j] = false;
                        getMatchingSet().add(matchingPair);
                        break;
                    }
                }
            }
        }
        for (int i = 0; i < matchedSource.length; ++i){
            if(matchedSource[i]){
                double d = getCostConfig().getDeleteCostFor(flowsSource[i].getType());
                MatchingPair matchingPair = new MatchingPair(flowsSource[i], null, PairType.DELETE);
                matchingPair.setDifferentLabel(d);
                cost += matchingPair.getDifferentLabel();
                getDeletedSet().add(matchingPair);
            }
        }
        for (int i = 0; i < matchedTarget.length; ++i){
            if(matchedTarget[i]){
                double d = getCostConfig().getDeleteCostFor(flowsTarget[i].getType());
                MatchingPair matchingPair = new MatchingPair(flowsTarget[i], null, PairType.INSERT);
                matchingPair.setDifferentLabel(d);
                cost += matchingPair.getDifferentLabel();
                getAddedSet().add(matchingPair);
            }
        }
        return cost;
    }
}
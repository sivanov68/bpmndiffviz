package ru.pais.vkr.services.model_comparators.ants

import ru.pais.vkr.comparator.entities.CostConfig
import ru.pais.vkr.graph_elements.Graph
import ru.pais.vkr.graph_elements.Vertex
import ru.pais.vkr.services.model_comparators.greedy.ChangeStep
import ru.pais.vkr.services.model_comparators.greedy.GreedyModelComparator
import java.util.*
import kotlin.math.pow

class Ant(
    private val sourceGraph: Graph,
    private val targetGraph: Graph,
    private val costConfig: CostConfig,
    private val pheromoneMap: HashMap<Vertex?, HashMap<Vertex?, Double>>,
    private val pheromonePower: Double,
    private val distancePower: Double
) {
    companion object {
        private const val MINIMUM_PATH_COST = 0.0001
        private val random = Random()
    }
    private var searchStarted = false

    fun findSolution(): List<ChangeStep>  {
        if (searchStarted) {
            throw IllegalStateException("you can find solution only once")
        }
        searchStarted = true

        val currentState: ArrayList<ChangeStep> = arrayListOf()
        while (sourceGraph.vertices.isNotEmpty() || targetGraph.vertices.isNotEmpty()) {
            val variants: Sequence<ChangeStep> = GreedyModelComparator.generateAllVariants(
                sourceGraph,
                targetGraph,
                currentState,
                costConfig
            )
            val changeCosts: ArrayList<ChangeCost> = arrayListOf()
            for (variant in variants) {
                val pathCost: Double = variant.vertex.differenceCost + variant.edges.sumByDouble { it.differenceCost }
                val pheromones: Double = pheromoneMap.getValue(variant.vertex.source).getValue(variant.vertex.target)
                val stateCost: Double = pheromones.pow(pheromonePower) * (1 / maxOf(pathCost, MINIMUM_PATH_COST)).pow(distancePower)
                changeCosts.add(ChangeCost(variant, stateCost))
            }

            val totalCost: Double = changeCosts.sumByDouble { it.totalCost }
            val randomValue = random.nextDouble() * totalCost
            var currentSum = 0.0
            var selectedChange: ChangeCost? = null
            for (change in changeCosts) {
                currentSum += change.totalCost
                if (currentSum >= randomValue) {
                    selectedChange = change
                    break
                }
            }
            if (selectedChange == null) {
                selectedChange = changeCosts.last()
            }

            selectedChange.variant.vertex.source?.let { sourceGraph.vertices.remove(it) }
            selectedChange.variant.vertex.target?.let { targetGraph.vertices.remove(it) }
            selectedChange.variant.edges.forEach {
                it.source?.let { sourceGraph.edges.remove(it) }
                it.target?.let { targetGraph.edges.remove(it) }
            }
            currentState.add(selectedChange.variant)
        }

        return currentState
    }

    private data class ChangeCost(
        val variant: ChangeStep,
        val totalCost: Double
    )
}
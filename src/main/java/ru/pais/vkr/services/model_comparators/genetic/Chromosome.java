package ru.pais.vkr.services.model_comparators.genetic;

import ru.pais.vkr.comparator.entities.MatchingPair;
import ru.pais.vkr.comparator.entities.PairType;

/**
 * Each element of the model is put in conformity with the chromosome.
 * The gene contains both compared models as a set of chromosomes for the first and second models.
 * If the elements match, the corresponding chromosomes are associated.
 */
public class Chromosome extends MatchingPair {

    private boolean isassociated;

    private Chromosome association;

    private boolean duplicate;

    private int genPosition;

    public Chromosome(MatchingPair matchingPair, int genPosition) {
        super(matchingPair.getFirst(), matchingPair.getSecond(), matchingPair.getType());

        setDifferentLabel((int) matchingPair.getDifferentLabel());

        isassociated = matchingPair.getType() == PairType.MATCH;
        this.genPosition = genPosition;
        this.duplicate = false;
        this.association = null;
    }

    public boolean isAssociated() {
        return isassociated;
    }

    public Chromosome getAssociation() {
        return association;
    }

    public boolean isDuplicate() {
        return duplicate;
    }

    public void setDuplicate(boolean duplicate) {
        this.duplicate = duplicate;
    }

    public int getGenPosition() {
        return genPosition;
    }

    public void setAssociation(Chromosome association) {
        this.association = association;
    }

    public double getCost() {
        return super.getDifferentLabel();
    }

    @Override
    public String toString() {
        return getDefinition();
    }
}
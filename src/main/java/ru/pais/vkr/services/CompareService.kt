package ru.pais.vkr.services

import org.camunda.bpm.model.bpmn.Bpmn
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.pais.vkr.comparator.entities.ComparisonResult
import ru.pais.vkr.comparator.entities.CostConfig
import ru.pais.vkr.dbmodels.SettingsModel
import ru.pais.vkr.services.model_comparators.ModelComparator
import ru.pais.vkr.services.model_comparators.ModelComparatorType
import ru.pais.vkr.services.model_comparators.a_star.AStarModelComparator
import ru.pais.vkr.services.model_comparators.ants.AntsModelComparator
import ru.pais.vkr.services.model_comparators.genetic.GeneticModelComparator
import ru.pais.vkr.services.model_comparators.greedy.GreedyModelComparator
import ru.pais.vkr.services.model_comparators.simulated_annealing.SimulatedAnnealingComparator
import ru.pais.vkr.services.model_comparators.tabu_search.TabuSearchModelComparator
import java.io.File
import java.util.*

@Service
open class CompareService @Autowired constructor(
    private val settingsService: SettingsService
) {

    /**
     * @param fileName1 file name of the first model
     * @param fileName2 file name of the second model
     * @param costConfig costs information for comparison
     * @param comparatorType comparator to use for comparison
     * @return list of differences
     */
    fun compareModels(
        fileName1: String,
        fileName2: String,
        costConfig: CostConfig,
        comparatorType: ModelComparatorType,
        comparatorParameters: Map<String, Any>
    ): ComparisonResult {
        val settings: SettingsModel = settingsService.getSettings()
        return MODEL_COMPARATORS.getValue(comparatorType).compareModels(
            Bpmn.readModelFromFile(File(fileName1)),
            Bpmn.readModelFromFile(File(fileName2)),
            costConfig,
            comparatorParameters,
            settings.useParallelization
        )
    }

    companion object {
        val MODEL_COMPARATORS = EnumMap<ModelComparatorType, ModelComparator>(ModelComparatorType::class.java)

        init {
            val comparators = arrayOf(
                AStarModelComparator(),
                GreedyModelComparator,
                TabuSearchModelComparator,
                GeneticModelComparator(),
                AntsModelComparator,
                SimulatedAnnealingComparator
            )

            for (comparator in comparators) {
                MODEL_COMPARATORS[comparator.type] = comparator
            }
        }
    }

}
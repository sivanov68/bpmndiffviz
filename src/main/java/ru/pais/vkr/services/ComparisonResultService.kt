package ru.pais.vkr.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.pais.vkr.comparator.entities.ComparisonResult
import ru.pais.vkr.dao.ResultDAO

@Service
class ComparisonResultService @Autowired constructor(
    private val resultDAO: ResultDAO
) {
    val allResults: List<ComparisonResult>
        get() = resultDAO.allResults

    fun saveResult(comparisonResult: ComparisonResult) {
        resultDAO.saveResult(comparisonResult)
    }

    fun getResultByID(id: Long?): ComparisonResult {
        return resultDAO.getResultByID(id)
    }
}

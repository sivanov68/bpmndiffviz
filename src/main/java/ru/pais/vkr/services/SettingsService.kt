package ru.pais.vkr.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.pais.vkr.dao.SettingsDAO
import ru.pais.vkr.dbmodels.SettingsModel

@Service
class SettingsService @Autowired constructor(
    private val settingsDAO: SettingsDAO
) {
    fun getSettings(): SettingsModel {
        val settingsItems: List<SettingsModel> = settingsDAO.query()
        if (settingsItems.isNotEmpty())
            return settingsItems[0]

        val model = SettingsModel(0, 0, "", false)
        settingsDAO.insert(model)
        return model
    }

    fun saveSettings(model: SettingsModel) {
        settingsDAO.update(model)
    }
}
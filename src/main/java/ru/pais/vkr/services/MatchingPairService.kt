package ru.pais.vkr.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.pais.vkr.comparator.entities.ComparisonResult
import ru.pais.vkr.comparator.entities.MatchingPair
import ru.pais.vkr.comparator.entities.PairType
import ru.pais.vkr.dao.MatchingPairDAO
import ru.pais.vkr.dbmodels.MatchingPairModel

@Service
class MatchingPairService @Autowired constructor(
    private val matchingPairDAO: MatchingPairDAO
) {
    fun saveMatchingPairsFrom(comparisonResult: ComparisonResult) {
        val id: Long = comparisonResult.id!!
        val items: Sequence<MatchingPair> = comparisonResult.addedSet.asSequence() +
            comparisonResult.deletedSet +
            comparisonResult.matchingSet
        items.forEach { saveMatchingPair(it, id) }
    }

    fun fillMatchingPairsFor(comparisonResult: ComparisonResult) {
        comparisonResult.addedSet = getMatchingPairsSet(comparisonResult, PairType.INSERT)
        comparisonResult.deletedSet = getMatchingPairsSet(comparisonResult, PairType.DELETE)
        comparisonResult.matchingSet = getMatchingPairsSet(comparisonResult, PairType.MATCH)
    }

    private fun saveMatchingPair(matchingPair: MatchingPair, result_id: Long) {
        matchingPairDAO.insert(
            MatchingPairModel(
                id = -1,
                pairType = matchingPair.type!!,
                firstName = matchingPair.firstID,
                secondName = matchingPair.secondID,
                description = matchingPair.definition,
                operationCost = matchingPair.differentLabel,
                resultId = result_id
            )
        )
    }

    private fun getMatchingPairsSet(comparisonResult: ComparisonResult, pairType: PairType): MutableSet<MatchingPair> {
        val queried = matchingPairDAO.queryByResultId(pairType, comparisonResult.id!!)
        return queried.map {
            MatchingPair().apply {
                type = it.pairType
                firstID = it.firstName
                secondID = it.secondName
                definition = it.description
                differentLabel = it.operationCost
            }
        }.toMutableSet()
    }
}

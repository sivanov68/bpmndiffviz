package ru.pais.vkr.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import ru.pais.vkr.dao.DocumentDAO
import ru.pais.vkr.dbmodels.SettingsModel
import ru.pais.vkr.models.Document
import ru.pais.vkr.utils.StringUtil
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

@Service
class DocumentService @Autowired constructor(
    private val documentDAO: DocumentDAO,
    private val settingsService: SettingsService
) {
    @Throws(IOException::class)
    fun saveFile(file: MultipartFile): Document {
        val settings: SettingsModel = settingsService.getSettings()
        if (settings.modelsDirectory.isEmpty()) {
            throw IOException("models directory is empty")
        }

        val currentDate = Date()
        val simpleDateFormat = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS")
        var systemName = StringUtil.replaceLast(file.originalFilename, ".bpmn", "")
        systemName = systemName + " " + simpleDateFormat.format(currentDate) + ".bpmn"
        file.transferTo(File(settings.modelsDirectory, systemName))

        val document = Document().apply {
            name = file.originalFilename
            this.systemName = systemName
            uploadDate = currentDate
        }

        documentDAO.saveDocument(document)

        return document
    }
}

package ru.pais.vkr.services

import org.camunda.bpm.model.bpmn.instance.*
import org.camunda.bpm.model.bpmn.instance.Error
import org.camunda.bpm.model.bpmn.instance.Process
import org.camunda.bpm.model.xml.instance.ModelElementInstance
import ru.pais.vkr.comparator.entities.BPMNElementType

import java.util.ArrayList
import java.util.EnumMap
import java.util.HashMap

class BPMNElementToClassMatcher private constructor() {

    companion object {
        private val elementsToClassesMap = EnumMap<BPMNElementType, Class<out ModelElementInstance>>(BPMNElementType::class.java)
        private val classesToElementsMap = HashMap<Class<out ModelElementInstance>, BPMNElementType>()

        init {
            elementsToClassesMap.putAll(
                sequenceOf(
                    BPMNElementType.Activity to Activity::class.java,
                    BPMNElementType.Artifact to Artifact::class.java,
                    BPMNElementType.Assignment to Assignment::class.java,
                    BPMNElementType.Association to Association::class.java,
                    BPMNElementType.Auditing to Auditing::class.java,
                    BPMNElementType.BoundaryEvent to BoundaryEvent::class.java,
                    BPMNElementType.BusinessRuleTask to BusinessRuleTask::class.java,
                    BPMNElementType.CallableElement to CallableElement::class.java,
                    BPMNElementType.CallActivity to CallActivity::class.java,
                    BPMNElementType.CallConversation to CallConversation::class.java,
                    BPMNElementType.CancelEventDefinition to CancelEventDefinition::class.java,
                    BPMNElementType.CategoryValue to CategoryValue::class.java,
                    BPMNElementType.Collaboration to Collaboration::class.java,
                    BPMNElementType.CompensateEventDefinition to CompensateEventDefinition::class.java,
                    BPMNElementType.ComplexBehaviorDefinition to ComplexBehaviorDefinition::class.java,
                    BPMNElementType.ComplexGateway to ComplexGateway::class.java,
                    BPMNElementType.ConditionalEventDefinition to ConditionalEventDefinition::class.java,
                    BPMNElementType.ConversationAssociation to ConversationAssociation::class.java,
                    BPMNElementType.Conversation to Conversation::class.java,
                    BPMNElementType.ConversationLink to ConversationLink::class.java,
                    BPMNElementType.CorrelationKey to CorrelationKey::class.java,
                    BPMNElementType.CorrelationPropertyBinding to CorrelationPropertyBinding::class.java,
                    BPMNElementType.CorrelationProperty to CorrelationProperty::class.java,
                    BPMNElementType.CorrelationPropertyRetrievalExpression to CorrelationPropertyRetrievalExpression::class.java,
                    BPMNElementType.CorrelationSubscription to CorrelationSubscription::class.java,
                    BPMNElementType.DataAssociation to DataAssociation::class.java,
                    BPMNElementType.DataInputAssociation to DataInputAssociation::class.java,
                    BPMNElementType.DataInput to DataInput::class.java,
                    BPMNElementType.DataObject to DataObject::class.java,
                    BPMNElementType.DataObjectReference to DataObjectReference::class.java,
                    BPMNElementType.DataOutputAssociation to DataOutputAssociation::class.java,
                    BPMNElementType.DataOutput to DataOutput::class.java,
                    BPMNElementType.DataState to DataState::class.java,
                    BPMNElementType.Definitions to Definitions::class.java,
                    BPMNElementType.Documentation to Documentation::class.java,
                    BPMNElementType.EndEvent to EndEvent::class.java,
                    BPMNElementType.EndPoint to EndPoint::class.java,
                    BPMNElementType.Error to Error::class.java,
                    BPMNElementType.ErrorEventDefinition to ErrorEventDefinition::class.java,
                    BPMNElementType.Escalation to Escalation::class.java,
                    BPMNElementType.EscalationEventDefinition to EscalationEventDefinition::class.java,
                    BPMNElementType.EventBasedGateway to EventBasedGateway::class.java,
                    BPMNElementType.ExclusiveGateway to ExclusiveGateway::class.java,
                    BPMNElementType.Expression to Expression::class.java,
                    BPMNElementType.Extension to Extension::class.java,
                    BPMNElementType.ExtensionElements to ExtensionElements::class.java,
                    BPMNElementType.FormalExpression to FormalExpression::class.java,
                    BPMNElementType.GlobalConversation to GlobalConversation::class.java,
                    BPMNElementType.HumanPerformer to HumanPerformer::class.java,
                    BPMNElementType.InclusiveGateway to InclusiveGateway::class.java,
                    BPMNElementType.InputSet to InputSet::class.java,
                    BPMNElementType.Interface to Interface::class.java,
                    BPMNElementType.IntermediateCatchEvent to IntermediateCatchEvent::class.java,
                    BPMNElementType.IntermediateThrowEvent to IntermediateThrowEvent::class.java,
                    BPMNElementType.IoBinding to IoBinding::class.java,
                    BPMNElementType.IoSpecification to IoSpecification::class.java,
                    BPMNElementType.ItemDefinition to ItemDefinition::class.java,
                    BPMNElementType.Lane to Lane::class.java,
                    BPMNElementType.LaneSet to LaneSet::class.java,
                    BPMNElementType.LinkEventDefinition to LinkEventDefinition::class.java,
                    BPMNElementType.ManualTask to ManualTask::class.java,
                    BPMNElementType.Message to Message::class.java,
                    BPMNElementType.MessageEventDefinition to MessageEventDefinition::class.java,
                    BPMNElementType.MessageFlowAssociation to MessageFlowAssociation::class.java,
                    BPMNElementType.MessageFlow to MessageFlow::class.java,
                    BPMNElementType.Monitoring to Monitoring::class.java,
                    BPMNElementType.MultiInstanceLoopCharacteristics to MultiInstanceLoopCharacteristics::class.java,
                    BPMNElementType.Operation to Operation::class.java,
                    BPMNElementType.OutputSet to OutputSet::class.java,
                    BPMNElementType.ParallelGateway to ParallelGateway::class.java,
                    BPMNElementType.ParticipantAssociation to ParticipantAssociation::class.java,
                    BPMNElementType.Participant to Participant::class.java,
                    BPMNElementType.ParticipantMultiplicity to ParticipantMultiplicity::class.java,
                    BPMNElementType.Performer to Performer::class.java,
                    BPMNElementType.PotentialOwner to PotentialOwner::class.java,
                    BPMNElementType.Process to Process::class.java,
                    BPMNElementType.Property to Property::class.java,
                    BPMNElementType.ReceiveTask to ReceiveTask::class.java,
                    BPMNElementType.Relationship to Relationship::class.java,
                    BPMNElementType.Rendering to Rendering::class.java,
                    BPMNElementType.ResourceAssignmentExpression to ResourceAssignmentExpression::class.java,
                    BPMNElementType.Resource to Resource::class.java,
                    BPMNElementType.ResourceParameterBinding to ResourceParameterBinding::class.java,
                    BPMNElementType.ResourceParameter to ResourceParameter::class.java,
                    BPMNElementType.ResourceRole to ResourceRole::class.java,
                    BPMNElementType.Script to Script::class.java,
                    BPMNElementType.ScriptTask to ScriptTask::class.java,
                    BPMNElementType.SendTask to SendTask::class.java,
                    BPMNElementType.SequenceFlow to SequenceFlow::class.java,
                    BPMNElementType.ServiceTask to ServiceTask::class.java,
                    BPMNElementType.Signal to Signal::class.java,
                    BPMNElementType.SignalEventDefinition to SignalEventDefinition::class.java,
                    BPMNElementType.StartEvent to StartEvent::class.java,
                    BPMNElementType.SubConversation to SubConversation::class.java,
                    BPMNElementType.SubProcess to SubProcess::class.java,
                    BPMNElementType.Task to Task::class.java,
                    BPMNElementType.TerminateEventDefinition to TerminateEventDefinition::class.java,
                    BPMNElementType.TextAnnotation to TextAnnotation::class.java,
                    BPMNElementType.Text to Text::class.java,
                    BPMNElementType.ThrowEvent to ThrowEvent::class.java,
                    BPMNElementType.TimerEventDefinition to TimerEventDefinition::class.java,
                    BPMNElementType.UserTask to UserTask::class.java
                )
            )

            for ((key, value) in elementsToClassesMap) {
                classesToElementsMap[value] = key
            }
        }

        @JvmStatic
        fun classForElement(element: BPMNElementType): Class<out ModelElementInstance> {
            val result: Class<out ModelElementInstance>? = elementsToClassesMap[element]

            if (result != null)
                return result

            throw IllegalArgumentException("Can't handle \"$element\" element")
        }

        @JvmStatic
        fun elementForClass(innerClass: Class<out ModelElementInstance>): BPMNElementType {
            val result = classesToElementsMap[innerClass]

            if (result != null)
                return result

            val parentEntries = ArrayList<MutableMap.MutableEntry<Class<out ModelElementInstance>, BPMNElementType>>()

            for (entry in classesToElementsMap) {
                if (entry.key.isAssignableFrom(innerClass))
                    parentEntries.add(entry)
            }

            // getting the most specific class of all;
            // the situation is: we have a number of classes in our map
            // that are parents of the actual type (implementation);
            // this can be illustrated as follows: actual class `TaskImpl` implements `Task`
            // and `Task` extends `Activity` |or| `TaskImpl` <- `Task` <- `Activity`;
            // we have both `Task` and `Activity` in our map
            // but we want the most specific interface selected (i.e. `Task`);
            // therefore, we are looping through the elements and if there are two elements
            // where the first one is a superclass for the second one the first one will be removed.
            for (i in parentEntries.indices) {
                var j = i + 1

                while (j < parentEntries.size) {
                    val first = parentEntries[i]
                    val second = parentEntries[j]

                    // the first one is a superclass for the second one
                    if (first.key.isAssignableFrom(second.key)) {
                        parentEntries.removeAt(i)
                        j = i + 1
                    }
                    // the second one is a superclass for the first one
                    else if (second.key.isAssignableFrom(first.key)) {
                        parentEntries.removeAt(j)
                    } else {
                        j++
                    }
                }
            }

            if (parentEntries.size == 1)
                return parentEntries[0].value

            throw IllegalArgumentException("Can't handle \"$innerClass\" class")
        }
    }
}

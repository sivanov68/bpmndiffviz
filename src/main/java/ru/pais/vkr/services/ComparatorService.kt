package ru.pais.vkr.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.pais.vkr.comparator.entities.CostConfig
import ru.pais.vkr.dao.ComparatorDAO
import ru.pais.vkr.dbmodels.HistoryComparatorModel

@Service
class ComparatorService @Autowired constructor(
    private val comparatorDAO: ComparatorDAO
) {
    fun saveComparatorsFor(costConfig: CostConfig, resultId: Long) {
        val models = costConfig.comparators.map {
            HistoryComparatorModel(
                id = -1,
                comparatorType = it.comparedType,
                insertCost = it.insert,
                deleteCost = it.delete,
                resultId = resultId
            )
        }
        comparatorDAO.insert(models)
    }

    fun fillComparatorsFor(costConfig: CostConfig, resultId: Long) {
        val dbModels = comparatorDAO.queryByResultId(resultId)
        for (comparator in costConfig.comparators) {
            val comparatorModel = dbModels.firstOrNull { it.comparatorType == comparator.comparedType }
            if (comparatorModel != null) {
                comparator.insert = comparatorModel.insertCost
                comparator.delete = comparatorModel.deleteCost
            }
        }
    }
}
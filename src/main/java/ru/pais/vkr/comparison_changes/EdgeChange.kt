package ru.pais.vkr.comparison_changes

import ru.pais.vkr.comparator.entities.CostConfig
import ru.pais.vkr.comparator.entities.MatchingPair
import ru.pais.vkr.comparator.entities.PairType
import ru.pais.vkr.graph_elements.Edge
import ru.pais.vkr.utils.StringUtil

data class EdgeChange(
    val source: Edge?,
    val target: Edge?,
    /**
     * Cost for changing `first` element to the `second`
     */
    val differenceCost: Double
) {
    fun toMatchingPair(): MatchingPair {
        if (source == null && target == null)
            throw IllegalStateException("`first` and `second` can't be null at the same time")

        val pairType: PairType = when {
            source == null -> PairType.INSERT
            target == null -> PairType.DELETE
            else -> PairType.MATCH
        }

        return when (pairType) {
            PairType.INSERT -> MatchingPair(target?.element, null, pairType)
            PairType.DELETE -> MatchingPair(source?.element, null, pairType)
            PairType.MATCH -> MatchingPair(source?.element, target?.element, pairType)
        }.apply {
            differentLabel = differenceCost
        }
    }

    companion object {
        fun of(source: Edge?, target: Edge?, costConfig: CostConfig): EdgeChange {
            return EdgeChange(source, target, costOf(source, target, costConfig))
        }

        fun costOf(source: Edge?, target: Edge?, costConfig: CostConfig): Double {
            if (source == null && target == null)
                throw IllegalArgumentException("either `sourceEdge` or `targetEdge` must be not null")

            if (source == null)
                return costConfig.getInsertCostFor(target!!.element.type)

            if (target == null)
                return costConfig.getDeleteCostFor(source.element.type)

            if (source.element.type != target.element.type)
                throw IllegalArgumentException("comparing elements with different types is not supported")

            return StringUtil.computeLevenshteinDistance(source.element.name, target.element.name) * costConfig.factorLevenshtein
        }
    }
}
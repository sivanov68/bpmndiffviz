package ru.pais.vkr.comparison_changes

import ru.pais.vkr.comparator.entities.CostConfig
import ru.pais.vkr.comparator.entities.MatchingPair
import ru.pais.vkr.comparator.entities.PairType
import ru.pais.vkr.graph_elements.Vertex
import ru.pais.vkr.utils.StringUtil

data class VertexChange(
    val source: Vertex?,
    val target: Vertex?,
    /**
     * Cost for changing `source` element to the `target`
     */
    val differenceCost: Double
) {
    fun toMatchingPair(): MatchingPair {
        if (source == null && target == null)
            throw IllegalArgumentException("`first` and `second` can't be null at the same time")

        val pairType: PairType = when {
            source == null -> PairType.INSERT
            target == null -> PairType.DELETE
            else -> PairType.MATCH
        }

        return when (pairType) {
            PairType.INSERT -> MatchingPair(target?.element, null, pairType)
            PairType.DELETE -> MatchingPair(source?.element, null, pairType)
            PairType.MATCH -> MatchingPair(source?.element, target?.element, pairType)
        }.apply {
            differentLabel = differenceCost
        }
    }

    companion object {
        fun of(source: Vertex?, target: Vertex?, costConfig: CostConfig): VertexChange {
            return VertexChange(source, target, costOf(source, target, costConfig))
        }

        fun costOf(source: Vertex?, target: Vertex?, costConfig: CostConfig): Double {
            if (source == null && target == null)
                throw IllegalArgumentException("either `sourceVertex` or `targetVertex` must be not null")

            if (source == null)
                return costConfig.getInsertCostFor(target!!.element.type)

            if (target == null)
                return costConfig.getDeleteCostFor(source.element.type)

            if (source.element.type != target.element.type)
                throw IllegalArgumentException("comparing elements with different types is not supported")

            return StringUtil.computeLevenshteinDistance(source.name, target.name) * costConfig.factorLevenshtein
        }

        fun canBeReplaced(source: Vertex?, target: Vertex?): Boolean {
            if (source == null) {
                return target != null
            }
            return target == null || source.element.type == target.element.type
        }
    }
}
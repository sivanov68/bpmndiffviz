package ru.pais.vkr.dbmodels

import ru.pais.vkr.comparator.entities.BPMNElementType

data class HistoryComparatorModel(
    var id: Long,
    var comparatorType: BPMNElementType,
    var insertCost: Double,
    var deleteCost: Double,
    // reference
    var resultId: Long
) {
    companion object {
        const val TABLE_NAME = "HistoryComparator"
    }

    class Columns private constructor() {
        companion object {
            const val ID = "id"
            const val COMPARATOR_TYPE = "comparator_type"
            const val INSERT_COST = "insert_cost"
            const val DELETE_COST = "delete_cost"
            // reference
            const val RESULT_ID = "result_id"
        }
    }
}
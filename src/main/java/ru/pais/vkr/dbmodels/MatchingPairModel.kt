package ru.pais.vkr.dbmodels

import ru.pais.vkr.comparator.entities.PairType

data class MatchingPairModel(
    var id: Long,
    var pairType: PairType,
    var firstName: String?,
    var secondName: String?,
    var description: String,
    var operationCost: Double,
    // reference
    var resultId: Long
) {
    companion object {
        const val TABLE_NAME = "matching_pair"
    }

    class Columns private constructor() {
        companion object {
            const val ID = "id"
            const val PAIR_TYPE = "pair_type"
            const val FIRST_NAME = "first_name"
            const val SECOND_NAME = "second_name"
            const val DESCRIPTION = "description"
            const val OPERATION_COST = "operation_cost"
            // reference
            const val RESULT_ID = "result_id"
        }
    }
}
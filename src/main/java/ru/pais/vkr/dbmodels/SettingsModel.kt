package ru.pais.vkr.dbmodels

data class SettingsModel(
    var id: Long,
    var databaseVersion: Int,
    var modelsDirectory: String,
    var useParallelization: Boolean
) {
    class Columns private constructor() {
        companion object {
            const val ID = "id"
            const val DATABASE_VERSION = "database_version"
            const val MODELS_DIRECTORY = "models_directory"
            const val USE_PARALLELIZATION = "use_parallelization"
        }
    }

    companion object {
        const val TABLE_NAME = "settings"
    }
}
package ru.pais.vkr.controllers

import com.google.common.base.Throwables
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.ModelAndView
import java.text.MessageFormat
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Controller
@RequestMapping("/")
class ErrorController {

    @RequestMapping("error")
    fun error(request: HttpServletRequest, response: HttpServletResponse): ModelAndView {
        val modelAndView = ModelAndView("error")

        val statusCode = request.getAttribute("javax.servlet.error.status_code") as Int?
        val throwable = request.getAttribute("javax.servlet.error.exception") as Throwable?

        val exceptionMessage = getExceptionMessage(throwable, statusCode)

        var requestUri: String? = request.getAttribute("javax.servlet.error.request_uri") as String
        if (requestUri == null) {
            requestUri = "Unknown"
        }

        val message = MessageFormat.format("{0} returned for {1} with message {2}",
            statusCode, requestUri, exceptionMessage
        )

        modelAndView.addObject("errorMessage", message)
        if (throwable != null) {
            throwable.printStackTrace()
            modelAndView.addObject("exp", throwable.message)
        } else {
            modelAndView.addObject("exp", "No error")
        }
        modelAndView.addObject("url", request.requestURL)
        return modelAndView
    }

    private fun getExceptionMessage(throwable: Throwable?, statusCode: Int?): String {
        if (throwable != null) {
            return Throwables.getRootCause(throwable).message ?: ""
        }
        if (statusCode == null) {
            return ""
        }
        val httpStatus = HttpStatus.valueOf(statusCode)
        return httpStatus.reasonPhrase
    }

}

package ru.pais.vkr.controllers

import org.apache.commons.io.IOUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import ru.pais.vkr.services.DocumentService
import ru.pais.vkr.services.ModelService
import ru.pais.vkr.services.SettingsService
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import javax.servlet.http.HttpServletResponse

@Controller
@RequestMapping("/resources/")
class ResourcesRESTController @Autowired constructor(
    private val modelService: ModelService,
    private val documentService: DocumentService,
    private val settingsService: SettingsService
) {
    @RequestMapping("model", method = [RequestMethod.GET, RequestMethod.POST])
    @ResponseBody
    @Throws(IOException::class)
    fun saveModel(response: HttpServletResponse,
                  @RequestParam(value = "id") id: Long?) {
        val model = modelService.getModelByID(id)
        val (_, _, modelsDirectory) = settingsService.getSettings()

        val file = File(modelsDirectory, model.document.systemName)
        val `is` = FileInputStream(file)
        IOUtils.copy(`is`, response.outputStream)
        response.flushBuffer()
    }

}

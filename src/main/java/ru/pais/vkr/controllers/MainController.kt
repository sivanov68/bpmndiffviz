package ru.pais.vkr.controllers

import org.camunda.bpm.model.bpmn.Bpmn
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import org.springframework.web.servlet.support.RequestContextUtils
import ru.pais.vkr.comparator.entities.BPMNElementType
import ru.pais.vkr.comparator.entities.ComparisonResult
import ru.pais.vkr.comparator.entities.CostConfig
import ru.pais.vkr.dbmodels.SettingsModel
import ru.pais.vkr.models.Model
import ru.pais.vkr.services.*
import ru.pais.vkr.services.model_comparators.ModelComparator
import ru.pais.vkr.services.model_comparators.ModelComparatorType
import ru.pais.vkr.utils.BPMNUtils
import ru.pais.vkr.utils.addAllObjects

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import java.io.File
import java.util.Date
import kotlin.math.round

@Controller
@RequestMapping("/")
class MainController @Autowired constructor(
    private val modelService: ModelService,
    private val compareService: CompareService,
    private val documentService: DocumentService,
    private val comparisonResultService: ComparisonResultService,
    private val matchingPairService: MatchingPairService,
    private val comparatorService: ComparatorService,
    private val settingsService: SettingsService
) {
    @RequestMapping
    fun mainPage(): ModelAndView {
        return ModelAndView("main").addAllObjects(
            "number_of_models" to modelService.allModels.size.toString(),
            "number_of_comparisons" to comparisonResultService.allResults.size.toString()
        )
    }

    @RequestMapping("models")
    fun models(): ModelAndView {
        return ModelAndView("models").apply {
            addObject(modelService.allModels)
        }
    }

    @RequestMapping("uploadModel")
    fun uploadModel(
        request: HttpServletRequest,
        response: HttpServletResponse
    ): ModelAndView {
        val inputFlashMap = RequestContextUtils.getInputFlashMap(request)

        var id: Long? = null
        var modelName: String? = null

        if (inputFlashMap != null) {
            id = inputFlashMap["id"] as Long?
            modelName = inputFlashMap["modelName"] as String
        }

        return ModelAndView("uploadModel").apply {
            if (id == null) {
                addObject("message", "This model can't be uploaded to our server. Please, contact to support.")
            } else {
                addObject("message", "Model was successfully uploaded to our server.")
                addObject("id", id)
                addObject("modelName", modelName)
            }
        }
    }

    @RequestMapping("saveModel")
    fun saveModel(
        @RequestParam(value = "name") name: String,
        @RequestParam(value = "file") file: MultipartFile,
        redirectAttributes: RedirectAttributes
    ): String {
        var id: Long = 0
        var modelName: String? = null

        if (!file.isEmpty) {
            val document = documentService.saveFile(file)
            val model = Model(
                id = -1,
                document = document,
                name = name
            )
            modelService.saveModel(model)
            id = model.id
            modelName = name
        }

        redirectAttributes.addFlashAttribute("id", id)
        redirectAttributes.addFlashAttribute("modelName", modelName)

        return "redirect:/uploadModel"
    }

    @RequestMapping("model")
    fun saveModel(
        @RequestParam(value = "id") id: Long
    ): ModelAndView {
        val model = modelService.getModelByID(id)
        val settings = settingsService.getSettings()
        val bpmnModelInstance = Bpmn.readModelFromFile(
            File(settings.modelsDirectory, model.document.systemName)
        )

        fun countOf(elementsType: BPMNElementType): Int {
            return BPMNUtils.getElementsByType(bpmnModelInstance, elementsType).size
        }

        return ModelAndView("model").addAllObjects(
            "taskCount" to countOf(BPMNElementType.Task),
            "sequenceFlowCount" to countOf(BPMNElementType.StartEvent),
            "startEventCount" to countOf(BPMNElementType.StartEvent),
            "endEventCount" to countOf(BPMNElementType.EndEvent),
            "exclusiveGatewayCount" to countOf(BPMNElementType.ExclusiveGateway),
            "parallelGatewayCount" to countOf(BPMNElementType.ParallelGateway),
            "processCount" to countOf(BPMNElementType.Process),
            "messageFlowCount" to countOf(BPMNElementType.MessageFlow),

            "model" to model
        )
    }

    @RequestMapping("comparison/first_step")
    fun comparisonFirstStep(): ModelAndView {
        return ModelAndView("comparison1").apply {
            addObject(modelService.allModels)
        }
    }

    @RequestMapping("comparison/second_step")
    fun comparisonSecondStep(
        request: HttpServletRequest,
        response: HttpServletResponse,
        @RequestParam(value = "firstModelID") id: Long
    ): ModelAndView {
        val costConfig = CostConfig()
        request.session.setAttribute("costConfig", costConfig)

        return ModelAndView("comparison2").apply {
            addObject(modelService.allModels)
            addObject("firstModel", modelService.getModelByID(id))
        }
    }

    @RequestMapping(value = ["comparison/third_step"])
    fun comparisonThirdStep(
        request: HttpServletRequest,
        response: HttpServletResponse,
        @RequestParam(value = "firstModelID") id1: Long,
        @RequestParam(value = "secondModelID") id2: Long
    ): ModelAndView {
        val costConfig = request.session.getAttribute("costConfig") as CostConfig

        return ModelAndView("comparison3").apply {
            addObject(modelService.allModels)

            addAllObjects(
                "firstModel" to modelService.getModelByID(id1),
                "secondModel" to modelService.getModelByID(id2),
                "configCost" to costConfig,
                "configs" to costConfig.comparators,
                "modelComparators" to ModelComparatorType.values(),
                "modelComparatorParameters" to ModelComparatorType.values().map {
                    it to CompareService.MODEL_COMPARATORS.getValue(it).parameters
                }.toMap()
            )
        }
    }

    @RequestMapping("comparison/fourth_step")
    fun comparisonFourthStep(
        request: HttpServletRequest,
        response: HttpServletResponse,
        @RequestParam(value = "firstModelID") id1: Long,
        @RequestParam(value = "secondModelID") id2: Long,
        @RequestParam(value = "Ldistance") Ldistance: Double,
        @RequestParam(value = "algorithm") comparator: ModelComparatorType
    ): ModelAndView {
        val algorithmParameters: MutableMap<String, Any> = mutableMapOf()
        for (parameter: ModelComparator.ParameterRaw in CompareService.MODEL_COMPARATORS.getValue(comparator).parameters) {
            val value: String? = request.getParameter("inputParam_${comparator}_${parameter.name}")

            if (value == null) {
                algorithmParameters[parameter.name] = parameter.defaultValue
            } else {
                algorithmParameters[parameter.name] = when (parameter.defaultValue) {
                    is Boolean -> value.toBoolean()
                    is Int -> value.toInt()
                    is Double -> value.toDouble()
                    else -> throw IllegalArgumentException("can't handle variable of type `${parameter.defaultValue.javaClass}`")
                }
            }
        }

        val costConfig = request.session.getAttribute("costConfig") as CostConfig
        costConfig.factorLevenshtein = Ldistance

        val model1: Model = modelService.getModelByID(id1)
        val model2: Model = modelService.getModelByID(id2)
        val settings = settingsService.getSettings()

        val file1: String = File(settings.modelsDirectory, model1.document.systemName).absolutePath
        val file2: String = File(settings.modelsDirectory, model2.document.systemName).absolutePath

        val comparisonResult: ComparisonResult = compareService.compareModels(
            file1, file2, costConfig, comparator, algorithmParameters
        ).apply {
            factorLevenshtein = Ldistance
            date = Date()
            firstID = model1.id
            secondID = model2.id
        }

        comparisonResultService.saveResult(comparisonResult)
        matchingPairService.saveMatchingPairsFrom(comparisonResult)
        comparatorService.saveComparatorsFor(comparisonResult.costConfig!!, comparisonResult.id!!)

        val sizeAll = comparisonResult.matchingSet.size + comparisonResult.deletedSet.size + comparisonResult.addedSet.size
        val percent2 = round(comparisonResult.matchingSet.size * 100.0 / sizeAll)
        val percent3 = round(comparisonResult.deletedSet.size * 100.0 / sizeAll)

        return ModelAndView("comparison4").addAllObjects(
            "firstModel" to model1,
            "secondModel" to model2,
            "result" to comparisonResult,
            "percent2" to percent2,
            "percent3" to percent3,
            "percent4" to 100 - percent2 - percent3
        )
    }

    @RequestMapping("results")
    fun results(): ModelAndView {
        val comparisonResults = comparisonResultService.allResults
        for (result in comparisonResults) {
            result.firstName = modelService.getModelByID(result.firstID).name
            result.secondName = modelService.getModelByID(result.secondID).name
        }

        return ModelAndView("results").addAllObjects(
            "results" to comparisonResults
        )
    }

    @RequestMapping("result")
    fun result(
        request: HttpServletRequest,
        response: HttpServletResponse,
        @RequestParam(value = "id") id: Long
    ): ModelAndView {
        val comparisonResult = comparisonResultService.getResultByID(id)
        val costConfig = CostConfig()
        comparatorService.fillComparatorsFor(costConfig, id)
        comparisonResult.costConfig = costConfig
        matchingPairService.fillMatchingPairsFor(comparisonResult)
        comparisonResult.firstName = modelService.getModelByID(comparisonResult.firstID).name
        comparisonResult.secondName = modelService.getModelByID(comparisonResult.secondID).name

        val sizeAll = comparisonResult.matchingSet.size + comparisonResult.deletedSet.size + comparisonResult.addedSet.size
        val percent2 = Math.round(comparisonResult.matchingSet.size.toDouble() / sizeAll * 100)
        val percent3 = Math.round(comparisonResult.deletedSet.size.toDouble() / sizeAll * 100)

        return ModelAndView("result").addAllObjects(
            "result" to comparisonResult,
            "percent2" to percent2,
            "percent3" to percent3,
            "percent4" to 100 - percent2 - percent3
        )
    }

    @RequestMapping("saveConfig", method = [RequestMethod.GET, RequestMethod.POST])
    @ResponseBody
    fun saveModel(
        request: HttpServletRequest,
        response: HttpServletResponse,
        @RequestParam(value = "delete") delete: Double,
        @RequestParam(value = "insert") insert: Double,
        @RequestParam(value = "configType") configType: String
    ): String {
        val costConfig = request.session.getAttribute("costConfig") as CostConfig
        costConfig.setCosts(configType, delete, insert)
        return configType
    }

    @RequestMapping("settings")
    fun settings(
        request: HttpServletRequest,
        response: HttpServletResponse
    ): ModelAndView {
        val settings: SettingsModel = settingsService.getSettings()
        return ModelAndView("settings").addAllObjects(
            "models_path" to settings.modelsDirectory,
            "use_parallelization" to settings.useParallelization
        )
    }

    @RequestMapping("saveSettings")
    fun saveSettings(
        request: HttpServletRequest,
        response: HttpServletResponse,
        @RequestParam(value = "models_path") modelsPath: String,
        @RequestParam(value = "use_parallelization", required = false, defaultValue = "off") useParallelization: String
    ): String {
        val settings: SettingsModel = settingsService.getSettings()
        settings.modelsDirectory = modelsPath
        settings.useParallelization = useParallelization == "on"
        settingsService.saveSettings(settings)
        return "redirect:/settings"
    }
}

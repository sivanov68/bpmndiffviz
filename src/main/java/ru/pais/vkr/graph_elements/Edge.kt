package ru.pais.vkr.graph_elements

import org.camunda.bpm.model.bpmn.instance.*
import org.camunda.bpm.model.xml.instance.ModelElementInstance
import ru.pais.vkr.comparator.entities.BPMNElement
import ru.pais.vkr.comparator.entities.BPMNElementType
import ru.pais.vkr.utils.BPMNUtils

class Edge private constructor(
    val element: BPMNElement,
    val source: Vertex,
    val target: Vertex
) {
    companion object {
        fun tryCreate(element: BPMNElement): Edge? {
            if (element.type !in BPMNUtils.EDGE_TYPES)
                return null

            var sourceRaw: ModelElementInstance? = null
            var targetRaw: ModelElementInstance? = null

            when (element.type) {
                BPMNElementType.Association -> with(element.raw as Association) {
                    sourceRaw = source
                    targetRaw = target
                }
                BPMNElementType.ConversationLink -> with(element.raw as ConversationLink) {
                    sourceRaw = source
                    targetRaw = target
                }
                BPMNElementType.MessageFlow -> with(element.raw as MessageFlow) {
                    sourceRaw = source
                    targetRaw = target
                }
                BPMNElementType.SequenceFlow -> with(element.raw as SequenceFlow) {
                    sourceRaw = source
                    targetRaw = target
                }
                else -> throw NotImplementedError("Have to implement all `EDGE_TYPES` handling")
            }

            if (sourceRaw == null)
                throw IllegalArgumentException("`source` element for an edge can not be null")
            if (targetRaw == null)
                throw IllegalArgumentException("`target` element for an edge can not be null")

            val sourceVertex: Vertex? = Vertex.tryCreate(BPMNElement(sourceRaw!!))
            val targetVertex: Vertex? = Vertex.tryCreate(BPMNElement(targetRaw!!))

            if (sourceVertex == null || targetVertex == null)
                return null

            return Edge(element, sourceVertex, targetVertex)
        }
    }

    override fun equals(other: Any?): Boolean {
        return other is Edge && element == other.element
    }

    override fun hashCode(): Int {
        return element.hashCode()
    }

    fun clone(): Edge {
        return Edge(element, source, target)
    }

    override fun toString(): String {
        return "($element): ($source) -> ($target)"
    }
}
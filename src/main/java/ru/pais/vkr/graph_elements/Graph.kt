package ru.pais.vkr.graph_elements

data class Graph(
    val vertices: MutableList<Vertex>,
    val edges: MutableList<Edge>
) {
    fun clone(): Graph {
        return Graph(
            vertices.map { it.clone() }.toMutableList(),
            edges.map { it.clone() }.toMutableList()
        )
    }
}
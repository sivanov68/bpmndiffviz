package ru.pais.vkr.graph_elements

import ru.pais.vkr.comparator.entities.BPMNElement
import ru.pais.vkr.utils.BPMNUtils

class Vertex {
    companion object {
        fun tryCreate(element: BPMNElement): Vertex? {
            if (element.type !in BPMNUtils.VERTEX_TYPES)
                return null

            return Vertex(element, false)
        }
    }

    val element: BPMNElement

    constructor(element: BPMNElement): this(element, true)

    private constructor(element: BPMNElement, checkType: Boolean) {
        if (checkType && element.type !in BPMNUtils.VERTEX_TYPES)
            throw IllegalArgumentException("element must be one of `BPMNUtils.VERTEX_TYPES`")

        this.element = element
    }

    val name: String?
        get() = element.name

    override fun equals(other: Any?): Boolean {
        return other is Vertex && element == other.element
    }

    override fun hashCode(): Int {
        return (name?.hashCode() ?: 0) xor element.hashCode()
    }

    fun clone(): Vertex {
        return Vertex(element)
    }

    override fun toString(): String {
        return element.toString()
    }
}
package ru.pais.vkr.setup

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.ResultSetExtractor
import org.springframework.stereotype.Component
import ru.pais.vkr.dbmodels.SettingsModel
import ru.pais.vkr.services.SettingsService
import java.io.File
import java.sql.ResultSet
import java.sql.SQLException
import java.util.concurrent.atomic.AtomicBoolean
import javax.servlet.ServletContext

@Component
class SetUpComponent @Autowired constructor(
    private val servletContext: ServletContext,
    private val jdbcTemplate: JdbcTemplate,
    private val settingsService: SettingsService
) : ApplicationListener<ContextRefreshedEvent> {
    companion object {
        private const val MIGRATIONS_PATH = "WEB-INF/resources/migrations"
        private const val MIGRATION_FORWARD = "forward"
        private const val MIGRATION_BACKWARD = "backward"
        private const val MIGRATION_REGEX_VERSION = "version"
        private const val MIGRATION_REGEX_TYPE = "type"
        private val migrationRegex = Regex("^(?<$MIGRATION_REGEX_VERSION>\\d{4})_(?<$MIGRATION_REGEX_TYPE>$MIGRATION_FORWARD|$MIGRATION_BACKWARD).sql\$")
        private val initialized = AtomicBoolean()
    }

    override fun onApplicationEvent(event: ContextRefreshedEvent?) {
        if (!initialized.compareAndSet(false, true)) {
            return
        }

        applyMigrations()
    }

    private fun applyMigrations() {
        val migrationsPath: String = servletContext.getRealPath(MIGRATIONS_PATH)
        val migrationFiles: Array<out File> = File(migrationsPath).listFiles { file -> migrationRegex.matches(file.name) }
        val migrations: List<MigrationFile> = migrationFiles.map {
            val match = migrationRegex.matchEntire(it.name)!!
            MigrationFile(
                match.groups[MIGRATION_REGEX_VERSION]!!.value.toInt(),
                match.groups[MIGRATION_REGEX_TYPE]!!.value == MIGRATION_FORWARD,
                it
            )
        }.filter { it.isForwarding }.sortedBy { it.version }

        val maximumVersion: Int? = migrations.lastOrNull()?.version
        if (maximumVersion == null) {
            // no available migrations, skip
            return
        }

        val currentVersion: Int = try {
            jdbcTemplate.query(
                "SELECT ${SettingsModel.Columns.DATABASE_VERSION} " +
                "FROM ${SettingsModel.TABLE_NAME}",
                ResultSetExtractor { resultSet ->
                    return@ResultSetExtractor if (resultSet.next()) resultSet.getInt(1) else -1
                }
            )
        } catch (_: Exception) {
            try {
                jdbcTemplate.query(
                    "SELECT id FROM Storage",
                    ResultSetExtractor { null }
                )
                0
            } catch (_: Exception) {
                -1
            }
        }

        if (currentVersion >= maximumVersion) {
            return
        }

        for (migration in migrations.filter { it.version > currentVersion }) {
            val sqlQuery: String = migration.file.readText()
            jdbcTemplate.update(sqlQuery)
        }
        val settings = settingsService.getSettings()
        settings.databaseVersion = maximumVersion
        settingsService.saveSettings(settings)
    }

    private data class MigrationFile(
        val version: Int,
        val isForwarding: Boolean,
        val file: File
    )
}
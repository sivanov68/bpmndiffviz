package ru.pais.vkr.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository
import ru.pais.vkr.comparator.entities.BPMNElementType
import ru.pais.vkr.dbmodels.HistoryComparatorModel
import ru.pais.vkr.utils.insertGenKeys
import java.sql.ResultSet
import java.sql.SQLException
import java.util.*
import kotlin.collections.HashMap

@Repository
class ComparatorDAO @Autowired constructor(
    private val jdbcTemplate: JdbcTemplate
) {
    companion object {
        private val TYPE_TO_INTEGER = EnumMap<BPMNElementType, Int>(BPMNElementType::class.java).apply {
            put(BPMNElementType.Activity, 0)
            put(BPMNElementType.Artifact, 1)
            put(BPMNElementType.Assignment, 2)
            put(BPMNElementType.Association, 3)
            put(BPMNElementType.Auditing, 4)
            put(BPMNElementType.BoundaryEvent, 5)
            put(BPMNElementType.BusinessRuleTask, 6)
            put(BPMNElementType.CallableElement, 7)
            put(BPMNElementType.CallActivity, 8)
            put(BPMNElementType.CatchEvent, 9)
            put(BPMNElementType.CallConversation, 10)
            put(BPMNElementType.CancelEventDefinition, 11)
            put(BPMNElementType.CategoryValue, 12)
            put(BPMNElementType.Collaboration, 13)
            put(BPMNElementType.CompensateEventDefinition, 14)
            put(BPMNElementType.ComplexBehaviorDefinition, 15)
            put(BPMNElementType.ComplexGateway, 16)
            put(BPMNElementType.ConditionalEventDefinition, 17)
            put(BPMNElementType.ConversationAssociation, 18)
            put(BPMNElementType.Conversation, 19)
            put(BPMNElementType.ConversationLink, 20)
            put(BPMNElementType.CorrelationKey, 21)
            put(BPMNElementType.CorrelationPropertyBinding, 22)
            put(BPMNElementType.CorrelationProperty, 23)
            put(BPMNElementType.CorrelationPropertyRetrievalExpression, 24)
            put(BPMNElementType.CorrelationSubscription, 25)
            put(BPMNElementType.DataAssociation, 26)
            put(BPMNElementType.DataInputAssociation, 27)
            put(BPMNElementType.DataInput, 28)
            put(BPMNElementType.DataObject, 29)
            put(BPMNElementType.DataObjectReference, 30)
            put(BPMNElementType.DataOutputAssociation, 31)
            put(BPMNElementType.DataOutput, 32)
            put(BPMNElementType.DataState, 33)
            put(BPMNElementType.Definitions, 34)
            put(BPMNElementType.Documentation, 35)
            put(BPMNElementType.EndEvent, 36)
            put(BPMNElementType.EndPoint, 37)
            put(BPMNElementType.Error, 38)
            put(BPMNElementType.ErrorEventDefinition, 39)
            put(BPMNElementType.Escalation, 40)
            put(BPMNElementType.EscalationEventDefinition, 41)
            put(BPMNElementType.EventBasedGateway, 42)
            put(BPMNElementType.ExclusiveGateway, 43)
            put(BPMNElementType.Expression, 44)
            put(BPMNElementType.Extension, 45)
            put(BPMNElementType.ExtensionElements, 46)
            put(BPMNElementType.FormalExpression, 47)
            put(BPMNElementType.GlobalConversation, 48)
            put(BPMNElementType.HumanPerformer, 49)
            put(BPMNElementType.InclusiveGateway, 50)
            put(BPMNElementType.InputSet, 51)
            put(BPMNElementType.Interface, 52)
            put(BPMNElementType.IntermediateCatchEvent, 53)
            put(BPMNElementType.IntermediateThrowEvent, 54)
            put(BPMNElementType.IoBinding, 55)
            put(BPMNElementType.IoSpecification, 56)
            put(BPMNElementType.ItemDefinition, 57)
            put(BPMNElementType.Lane, 58)
            put(BPMNElementType.LaneSet, 59)
            put(BPMNElementType.LinkEventDefinition, 60)
            put(BPMNElementType.ManualTask, 61)
            put(BPMNElementType.Message, 62)
            put(BPMNElementType.MessageEventDefinition, 63)
            put(BPMNElementType.MessageFlowAssociation, 64)
            put(BPMNElementType.MessageFlow, 65)
            put(BPMNElementType.Monitoring, 66)
            put(BPMNElementType.MultiInstanceLoopCharacteristics, 67)
            put(BPMNElementType.Operation, 68)
            put(BPMNElementType.OutputSet, 69)
            put(BPMNElementType.ParallelGateway, 70)
            put(BPMNElementType.ParticipantAssociation, 71)
            put(BPMNElementType.Participant, 72)
            put(BPMNElementType.ParticipantMultiplicity, 73)
            put(BPMNElementType.Performer, 74)
            put(BPMNElementType.PotentialOwner, 75)
            put(BPMNElementType.Process, 76)
            put(BPMNElementType.Property, 77)
            put(BPMNElementType.ReceiveTask, 78)
            put(BPMNElementType.Relationship, 79)
            put(BPMNElementType.Rendering, 80)
            put(BPMNElementType.ResourceAssignmentExpression, 81)
            put(BPMNElementType.Resource, 82)
            put(BPMNElementType.ResourceParameterBinding, 83)
            put(BPMNElementType.ResourceParameter, 84)
            put(BPMNElementType.ResourceRole, 85)
            put(BPMNElementType.Script, 86)
            put(BPMNElementType.ScriptTask, 87)
            put(BPMNElementType.SendTask, 88)
            put(BPMNElementType.SequenceFlow, 89)
            put(BPMNElementType.ServiceTask, 90)
            put(BPMNElementType.Signal, 91)
            put(BPMNElementType.SignalEventDefinition, 92)
            put(BPMNElementType.StartEvent, 93)
            put(BPMNElementType.SubConversation, 94)
            put(BPMNElementType.SubProcess, 95)
            put(BPMNElementType.Task, 96)
            put(BPMNElementType.TerminateEventDefinition, 97)
            put(BPMNElementType.TextAnnotation, 98)
            put(BPMNElementType.Text, 99)
            put(BPMNElementType.ThrowEvent, 100)
            put(BPMNElementType.TimerEventDefinition, 101)
            put(BPMNElementType.UserTask, 102)
        }
        private val INTEGER_TO_TYPE = HashMap<Int, BPMNElementType>()

        init {
            for ((key, value) in TYPE_TO_INTEGER) {
                INTEGER_TO_TYPE[value] = key
            }
        }
    }

    private val rowMapper = Mapper()

    fun queryByResultId(resultId: Long): List<HistoryComparatorModel> {
        return jdbcTemplate.query(
            "SELECT * FROM ${HistoryComparatorModel.TABLE_NAME} " +
            "WHERE ${HistoryComparatorModel.Columns.RESULT_ID} = ?",
            rowMapper,
            resultId
        )
    }

    fun insert(comparators: List<HistoryComparatorModel>) {
        comparators.forEach(this::insert)
    }

    fun insert(comparator: HistoryComparatorModel) {
        val keys = jdbcTemplate.insertGenKeys(
            "INSERT INTO ${HistoryComparatorModel.TABLE_NAME} (" +
            "   ${HistoryComparatorModel.Columns.COMPARATOR_TYPE}," +
            "   ${HistoryComparatorModel.Columns.INSERT_COST}," +
            "   ${HistoryComparatorModel.Columns.DELETE_COST}," +
            "   ${HistoryComparatorModel.Columns.RESULT_ID}" +
            ") VALUES (?, ?, ?, ?)"
        ) {
            setInt(1, TYPE_TO_INTEGER.getValue(comparator.comparatorType))
            setDouble(2, comparator.insertCost)
            setDouble(3, comparator.deleteCost)
            setLong(4, comparator.resultId)
        }
        comparator.id = keys.getValue(HistoryComparatorModel.Columns.ID) as Long
    }

    fun update(comparators: List<HistoryComparatorModel>) {
        comparators.forEach(this::update)
    }

    fun update(comparator: HistoryComparatorModel) {
        jdbcTemplate.update(
            "UPDATE ${HistoryComparatorModel.TABLE_NAME}" +
            "SET ${HistoryComparatorModel.Columns.COMPARATOR_TYPE} = ?," +
            "    ${HistoryComparatorModel.Columns.INSERT_COST} = ?," +
            "    ${HistoryComparatorModel.Columns.DELETE_COST} = ?," +
            "    ${HistoryComparatorModel.Columns.RESULT_ID} = ?" +
            "WHERE ${HistoryComparatorModel.Columns.ID} = ?",
            TYPE_TO_INTEGER.getValue(comparator.comparatorType),
            comparator.insertCost,
            comparator.deleteCost,
            comparator.resultId,
            comparator.id
        )
    }

    private class Mapper : RowMapper<HistoryComparatorModel> {
        @Throws(SQLException::class)
        override fun mapRow(resultSet: ResultSet, i: Int): HistoryComparatorModel {
            val id: Long = resultSet.getLong(HistoryComparatorModel.Columns.ID)
            val comparatorType: Int = resultSet.getInt(HistoryComparatorModel.Columns.COMPARATOR_TYPE)
            val insertCost: Double = resultSet.getDouble(HistoryComparatorModel.Columns.INSERT_COST)
            val deleteCost: Double = resultSet.getDouble(HistoryComparatorModel.Columns.DELETE_COST)
            val resultId: Long = resultSet.getLong(HistoryComparatorModel.Columns.RESULT_ID)

            return HistoryComparatorModel(
                id = id,
                comparatorType = INTEGER_TO_TYPE.getValue(comparatorType),
                insertCost = insertCost,
                deleteCost = deleteCost,
                resultId = resultId
            )
        }
    }
}


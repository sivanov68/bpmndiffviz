package ru.pais.vkr.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository
import ru.pais.vkr.comparator.entities.ComparisonResult
import ru.pais.vkr.utils.insertGenKeys
import java.sql.ResultSet
import java.sql.SQLException
import java.sql.Timestamp

@Repository
class ResultDAO @Autowired constructor(
    private val jdbcTemplate: JdbcTemplate
) {
    private val rowMapper = Mapper()

    val allResults: List<ComparisonResult>
        get() = jdbcTemplate.query("SELECT * FROM result", rowMapper)

    fun saveResult(comparisonResult: ComparisonResult) {
        val keys = jdbcTemplate.insertGenKeys(
            "INSERT INTO result (rdate, rec_counter, penalty, model_id_1, model_id_2, factor_levenshtein)" +
            " VALUES (?, ?, ?, ?, ?, ?)"
        ) {
            setTimestamp(1, Timestamp(comparisonResult.date!!.time))
            // todo: fill with statistics
            setInt(2, 0)
            setDouble(3, comparisonResult.penalty)
            setLong(4, comparisonResult.firstID!!)
            setLong(5, comparisonResult.secondID!!)
            setDouble(6, comparisonResult.factorLevenshtein)
        }
        comparisonResult.id = keys["id"] as Long
    }

    fun getResultByID(id: Long?): ComparisonResult {
        return jdbcTemplate.queryForObject(
            "SELECT * FROM result WHERE id = ?",
            rowMapper, id
        )
    }

    private class Mapper : RowMapper<ComparisonResult> {
        @Throws(SQLException::class)
        override fun mapRow(resultSet: ResultSet, i: Int): ComparisonResult {
            val comparisonResult = ComparisonResult()
            comparisonResult.id = resultSet.getLong(1)
            comparisonResult.date = resultSet.getTimestamp(2)
            // todo: fill with statistics
            //comparisonResult.setRecCounter(resultSet.getInt(3));
            comparisonResult.penalty = resultSet.getDouble(4)
            comparisonResult.firstID = resultSet.getLong(5)
            comparisonResult.secondID = resultSet.getLong(6)
            comparisonResult.factorLevenshtein = resultSet.getDouble(7)
            return comparisonResult
        }
    }
}

package ru.pais.vkr.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository
import ru.pais.vkr.dbmodels.SettingsModel
import ru.pais.vkr.utils.insertGenKeys
import java.sql.ResultSet
import java.sql.SQLException

@Repository
class SettingsDAO @Autowired constructor(
    private val jdbcTemplate: JdbcTemplate
) {
    private val rowMapper = Mapper()

    fun query(): List<SettingsModel> {
        return jdbcTemplate.query(
            "SELECT * FROM ${SettingsModel.TABLE_NAME}",
            rowMapper
        )
    }

    fun insert(models: List<SettingsModel>) {
        models.forEach(this::insert)
    }

    fun insert(model: SettingsModel) {
        val keys = jdbcTemplate.insertGenKeys(
            "INSERT INTO ${SettingsModel.TABLE_NAME} (" +
            "   ${SettingsModel.Columns.DATABASE_VERSION}," +
            "   ${SettingsModel.Columns.MODELS_DIRECTORY}," +
            "   ${SettingsModel.Columns.USE_PARALLELIZATION}" +
            ") VALUES (?, ?, ?)"
        ) {
            setInt(1, model.databaseVersion)
            setString(2, model.modelsDirectory)
            setBoolean(3, model.useParallelization)
        }
        model.id = keys.getValue(SettingsModel.Columns.ID) as Long
    }

    fun update(models: List<SettingsModel>) {
        models.forEach(this::update)
    }

    fun update(model: SettingsModel) {
        jdbcTemplate.update(
            "UPDATE ${SettingsModel.TABLE_NAME} " +
            "SET ${SettingsModel.Columns.DATABASE_VERSION} = ?, " +
            "    ${SettingsModel.Columns.MODELS_DIRECTORY} = ?, " +
            "    ${SettingsModel.Columns.USE_PARALLELIZATION} = ? " +
            "WHERE ${SettingsModel.Columns.ID} = ?",
            model.databaseVersion,
            model.modelsDirectory,
            model.useParallelization,
            model.id
        )
    }

    private class Mapper : RowMapper<SettingsModel> {
        @Throws(SQLException::class)
        override fun mapRow(resultSet: ResultSet, i: Int): SettingsModel {
            val id: Long = resultSet.getLong(SettingsModel.Columns.ID)
            val databaseVersion: Int = resultSet.getInt(SettingsModel.Columns.DATABASE_VERSION)
            val modelsDirectory: String = resultSet.getString(SettingsModel.Columns.MODELS_DIRECTORY)
            val useParallelization: Boolean = resultSet.getBoolean(SettingsModel.Columns.USE_PARALLELIZATION)

            return SettingsModel(
                id = id,
                databaseVersion = databaseVersion,
                modelsDirectory = modelsDirectory,
                useParallelization = useParallelization
            )
        }
    }
}

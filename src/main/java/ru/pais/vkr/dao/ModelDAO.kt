package ru.pais.vkr.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository
import ru.pais.vkr.models.Document
import ru.pais.vkr.models.Model
import ru.pais.vkr.utils.insertGenKeys
import java.sql.ResultSet
import java.sql.SQLException

@Repository
class ModelDAO @Autowired constructor(
    private val jdbcTemplate: JdbcTemplate,
    private val documentDAO: DocumentDAO
) {
    val allModels: List<Model>
        get() = jdbcTemplate.query(
            "SELECT * FROM model AS M JOIN document AS D ON M.document_id = D.id",
            Mapper()
        )

    fun getModelByID(id: Long?): Model {
        return jdbcTemplate.queryForObject(
            "SELECT * FROM model AS M JOIN document AS D ON M.document_id = D.id WHERE M.id = ?",
            Mapper(),
            id
        )
    }

    fun saveModel(model: Model) {
        val keys = jdbcTemplate.insertGenKeys(
            "INSERT INTO model (name, document_id) VALUES (?, ?)"
        ) {
            setString(1, model.name)
            setLong(2, model.document.id)
        }
        model.id = keys["id"] as Long
    }

    fun updateModel(model: Model) {
        documentDAO.saveOrUpdateDocument(model.document)
        jdbcTemplate.update(
            "UPDATE model SET name = ?, document_id = ? " + "WHERE id = ?",
            model.name,
            model.document.id,
            model.id
        )
    }

    fun saveOrUpdateModel(model: Model) {
        if (model.id == 0L) {
            saveModel(model)
        } else {
            updateModel(model)
        }
    }

    private class Mapper : RowMapper<Model> {
        @Throws(SQLException::class)
        override fun mapRow(resultSet: ResultSet, i: Int): Model {
            val document = Document()
            document.id = resultSet.getLong(4)
            document.uploadDate = resultSet.getDate(5)
            document.name = resultSet.getString(6)
            document.systemName = resultSet.getString(7)
            val model = Model(
                id = resultSet.getLong(1),
                document = document,
                name = resultSet.getString(2)
            )
            return model
        }
    }
}

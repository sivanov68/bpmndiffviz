package ru.pais.vkr.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository
import ru.pais.vkr.comparator.entities.PairType
import ru.pais.vkr.dbmodels.MatchingPairModel
import ru.pais.vkr.utils.insertGenKeys
import java.sql.ResultSet
import java.sql.SQLException
import java.util.*

@Repository
class MatchingPairDAO @Autowired constructor(
    private val jdbcTemplate: JdbcTemplate
) {
    companion object {
        private val TYPES_TO_INTEGERS = EnumMap<PairType, Int>(PairType::class.java).apply {
            put(PairType.DELETE, 0)
            put(PairType.INSERT, 1)
            put(PairType.MATCH, 2)
        }
        private val INTEGERS_TO_TYPES = HashMap<Int, PairType>()

        init {
           for ((key, value) in TYPES_TO_INTEGERS) {
               INTEGERS_TO_TYPES[value] = key
           }
        }
    }

    private val mapper = Mapper()

    fun queryByResultId(pairType: PairType, result_id: Long): List<MatchingPairModel> {
        return jdbcTemplate.query(
            "SELECT * FROM ${MatchingPairModel.TABLE_NAME} " +
            "WHERE ${MatchingPairModel.Columns.RESULT_ID} = ? " +
            "AND ${MatchingPairModel.Columns.PAIR_TYPE} = ?",
            mapper,
            result_id, TYPES_TO_INTEGERS.getValue(pairType)
        )
    }

    fun insert(matchingPair: MatchingPairModel) {
        val keys = jdbcTemplate.insertGenKeys(
            "INSERT INTO ${MatchingPairModel.TABLE_NAME} (" +
            "   ${MatchingPairModel.Columns.PAIR_TYPE}," +
            "   ${MatchingPairModel.Columns.FIRST_NAME}," +
            "   ${MatchingPairModel.Columns.SECOND_NAME}," +
            "   ${MatchingPairModel.Columns.DESCRIPTION}," +
            "   ${MatchingPairModel.Columns.OPERATION_COST}," +
            "   ${MatchingPairModel.Columns.RESULT_ID}" +
            ") VALUES (?, ?, ?, ?, ?, ?)"
        ) {
            setInt(1, TYPES_TO_INTEGERS.getValue(matchingPair.pairType))
            // todo: fix this
            setString(2, if (matchingPair.pairType == PairType.INSERT) null else matchingPair.firstName)
            setString(3, if (matchingPair.pairType == PairType.INSERT) matchingPair.firstName else matchingPair.secondName)
            setString(4, matchingPair.description)
            setDouble(5, matchingPair.operationCost)
            setLong(6, matchingPair.resultId)
        }
        matchingPair.id = keys.getValue(MatchingPairModel.Columns.ID) as Long
    }

    private class Mapper : RowMapper<MatchingPairModel> {
        @Throws(SQLException::class)
        override fun mapRow(resultSet: ResultSet, i: Int): MatchingPairModel {
            val id = resultSet.getLong(MatchingPairModel.Columns.ID)
            val pairType = INTEGERS_TO_TYPES.getValue(resultSet.getInt(MatchingPairModel.Columns.PAIR_TYPE))
            val firstName = resultSet.getString(MatchingPairModel.Columns.FIRST_NAME)
            val secondName = resultSet.getString(MatchingPairModel.Columns.SECOND_NAME)
            val description = resultSet.getString(MatchingPairModel.Columns.DESCRIPTION)
            val operationCost = resultSet.getDouble(MatchingPairModel.Columns.OPERATION_COST)
            val resultId = resultSet.getLong(MatchingPairModel.Columns.RESULT_ID)

            return MatchingPairModel(
                id = id,
                pairType = pairType,
                // todo: fix this
                firstName = if (pairType == PairType.INSERT) secondName else firstName,
                secondName = if (pairType == PairType.INSERT) null else secondName,
                description = description,
                operationCost = operationCost,
                resultId = resultId
            )
        }
    }
}


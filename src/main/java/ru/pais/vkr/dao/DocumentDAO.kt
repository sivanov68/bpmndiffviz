package ru.pais.vkr.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository
import ru.pais.vkr.models.Document
import ru.pais.vkr.utils.insertGenKeys

import java.sql.Date
import java.sql.ResultSet
import java.sql.SQLException

@Repository
class DocumentDAO @Autowired constructor(
    private val jdbcTemplate: JdbcTemplate
) {
    fun getDocumentByID(id: Long?): Document {
        return jdbcTemplate.queryForObject(
            "SELECT * FROM document WHERE document.id = ?",
            Mapper(),
            id
        )
    }

    fun saveDocument(document: Document) {
        val keys = jdbcTemplate.insertGenKeys(
            "INSERT INTO document (uploadDate, name, system_name) VALUES (?, ?, ?)"
        ) {
            setDate(1, Date(document.uploadDate!!.time))
            setString(2, document.name)
            setString(3, document.systemName)
        }
        document.id = keys["id"] as Long
    }

    fun updateDocument(document: Document) {
        jdbcTemplate.update(
            "UPDATE document SET uploadDate = ?, name = ?, system_name = ? " + "WHERE id = ? ",
            document.uploadDate,
            document.name,
            document.systemName,
            document.id
        )
    }

    fun saveOrUpdateDocument(document: Document) {
        if (document.id == 0L) {
            saveDocument(document)
        } else {
            updateDocument(document)
        }
    }

    private class Mapper : RowMapper<Document> {
        @Throws(SQLException::class)
        override fun mapRow(resultSet: ResultSet, i: Int): Document {
            val document = Document()
            document.id = resultSet.getLong(1)
            document.uploadDate = resultSet.getDate(2)
            document.name = resultSet.getString(3)
            document.systemName = resultSet.getString(4)
            return document
        }
    }
}

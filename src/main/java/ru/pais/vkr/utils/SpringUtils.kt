package ru.pais.vkr.utils

import org.springframework.web.servlet.ModelAndView

fun ModelAndView.addAllObjects(vararg parameters: Pair<String, Any>): ModelAndView {
    return addAllObjects(parameters.toMap())
}
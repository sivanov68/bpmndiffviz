package ru.pais.vkr.utils

import java.lang.AssertionError

object AssertionFlags {
    const val ENABLED: Boolean = false
}

inline fun executeIfAssert(action: () -> Unit) {
    if (AssertionFlags.ENABLED) {
        action()
    }
}

inline fun assertCustom(action: () -> Boolean) {
    if (AssertionFlags.ENABLED) {
        if (!action()) {
            throw AssertionError()
        }
    }
}

inline fun assertCustom(action: () -> Boolean, message: () -> String) {
    if (AssertionFlags.ENABLED) {
        if (!action()) {
            throw AssertionError(message())
        }
    }
}
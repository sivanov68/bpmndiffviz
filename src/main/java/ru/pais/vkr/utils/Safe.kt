package ru.pais.vkr.utils

class Safe private constructor() {
    companion object {
        @JvmStatic
        fun <T> contains(set: Set<T>, element: T): Boolean {
            return set.contains(element)
        }
    }
}

package ru.pais.vkr.utils

import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.support.GeneratedKeyHolder
import java.sql.PreparedStatement
import java.sql.Statement

inline fun JdbcTemplate.insertGenKeys(sql: String, crossinline statementFiller: PreparedStatement.() -> Unit): Map<String, Any> {
    val keyHolder = GeneratedKeyHolder()
    update(
        { connection ->
            connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS).apply(statementFiller)
        },
        keyHolder
    )
    return keyHolder.keys
}
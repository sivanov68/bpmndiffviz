package ru.pais.vkr.utils

import org.camunda.bpm.model.bpmn.BpmnModelInstance
import org.camunda.bpm.model.xml.instance.ModelElementInstance
import ru.pais.vkr.comparator.entities.BPMNElement
import ru.pais.vkr.comparator.entities.BPMNElementType
import ru.pais.vkr.graph_elements.Edge
import ru.pais.vkr.graph_elements.Graph
import ru.pais.vkr.graph_elements.Vertex
import ru.pais.vkr.services.BPMNElementToClassMatcher
import java.util.EnumSet
import kotlin.collections.ArrayList

fun BpmnModelInstance.toGraph(): Graph {
    val vertices: ArrayList<Vertex> = ArrayList()
    val edges: ArrayList<Edge> = ArrayList()

    for (vertexType: BPMNElementType in BPMNUtils.VERTEX_TYPES) {
        this.getElementsByType(vertexType).asSequence().mapNotNull { Vertex.tryCreate(it) }.forEach { vertices.add(it) }
    }

    for (edgeType: BPMNElementType in BPMNUtils.EDGE_TYPES) {
        this.getElementsByType(edgeType).asSequence().mapNotNull { Edge.tryCreate(it) }.forEach { edges.add(it) }
    }

    return Graph(vertices, edges)
}

fun BpmnModelInstance.getElementsByType(elementsType: BPMNElementType): List<BPMNElement> {
    return BPMNUtils.getElementsByType(this, elementsType)
}

class BPMNUtils private constructor() {
    companion object {
        @JvmField
        val CONTAINER_TYPES: Set<BPMNElementType> = EnumSet.of(
            BPMNElementType.Process,
            BPMNElementType.Lane,
            BPMNElementType.SubProcess
        )

        @JvmField
        val VERTEX_TYPES: Set<BPMNElementType> = EnumSet.of(
            BPMNElementType.Participant,
            BPMNElementType.Task,
            BPMNElementType.ExclusiveGateway,
            BPMNElementType.ParallelGateway,
            BPMNElementType.InclusiveGateway,
            BPMNElementType.EventBasedGateway,
            BPMNElementType.ComplexGateway,
            BPMNElementType.EndEvent,
            BPMNElementType.StartEvent
        )

        @JvmField
        val EDGE_TYPES: Set<BPMNElementType> = EnumSet.of(
            BPMNElementType.Association,
            BPMNElementType.ConversationLink,
            BPMNElementType.MessageFlow,
            BPMNElementType.SequenceFlow
        )

        @JvmStatic
        fun getElementsByType(bpmnModel: BpmnModelInstance, elementType: BPMNElementType): List<BPMNElement> {
            val rawElementClass = BPMNElementToClassMatcher.classForElement(elementType)
            val implementationName = rawElementClass.simpleName + "Impl"

            val rawElements: MutableCollection<out ModelElementInstance> = bpmnModel.getModelElementsByType(rawElementClass)

            return rawElements.asSequence()
                .filter { it -> it.javaClass.simpleName == implementationName }
                .map { BPMNElement(it) }
                .toList()
        }
    }
}

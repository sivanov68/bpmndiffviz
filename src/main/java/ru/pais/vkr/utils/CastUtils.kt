package ru.pais.vkr.utils

import java.util.*

inline fun <reified T: Enum<T>> Collection<T>.toEnumSet(): EnumSet<T> {
    return CastUtils.toEnumSet(this)
}

class CastUtils private constructor() {
    companion object {
        inline fun <reified T: Enum<T>> toEnumSet(collection: Collection<T>): EnumSet<T> {
            if (collection.isEmpty())
                return EnumSet.noneOf(T::class.java)

            return EnumSet.copyOf(collection)
        }
    }
}
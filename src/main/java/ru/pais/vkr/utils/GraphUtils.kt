package ru.pais.vkr.utils

import ru.pais.vkr.comparator.entities.CostConfig
import ru.pais.vkr.comparison_changes.EdgeChange
import ru.pais.vkr.graph_elements.Edge

class GraphUtils private constructor() {
    companion object {
        // creates a list of all possible matches with the minimum cost when two conditions are met:
        // 1. `source` and `target` edges are of the same type (undefined behaviour otherwise)
        // 2. we can only `add`, `remove` or `match` edges,
        //    where `add`, `remove` and `match` costs are taken from the `costConfig`
        fun minEdgeReplacementList(source: Edge?, target: Edge?, costConfig: CostConfig): List<EdgeChange> {
            val result = ArrayList<EdgeChange>(2)
            minEdgeReplacement(source, target, costConfig) {
                source: Edge?, target: Edge?, replacementCost: Double ->
                result.add(EdgeChange(source, target, replacementCost))
            }
            return result
        }

        fun minEdgeReplacementCost(source: Edge?, target: Edge?, costConfig: CostConfig): Double {
            var totalCost = 0.0
            minEdgeReplacement(source, target, costConfig) {
                _: Edge?, _: Edge?, replacementCost: Double -> totalCost += replacementCost
            }
            return totalCost
        }

        inline fun minEdgeReplacement(
            source: Edge?,
            target: Edge?,
            costConfig: CostConfig,
            crossinline handler: (source: Edge?, target: Edge?, replacementCost: Double) -> Unit
        ) {
            if (source == null && target == null)
                return

            if (source == null || target == null) {
                return handler(source, target, EdgeChange.costOf(source, target, costConfig))
            }

            val matchCost: Double = EdgeChange.costOf(source, target, costConfig)
            val deleteSourceCost: Double = EdgeChange.costOf(source, null, costConfig)
            val addTargetCost: Double = EdgeChange.costOf(null, target, costConfig)

            if (matchCost <= deleteSourceCost + addTargetCost)
                return handler(source, target, matchCost)

            handler(source, null, deleteSourceCost)
            handler(null, target, addTargetCost)
        }
    }
}
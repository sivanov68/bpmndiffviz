package ru.pais.vkr.utils

import org.camunda.bpm.model.bpmn.BpmnModelInstance
import ru.pais.vkr.comparator.entities.*
import ru.pais.vkr.comparison_changes.EdgeChange
import ru.pais.vkr.comparison_changes.VertexChange

class ModelComparatorUtils private constructor() {
    companion object {
        private val BPMN_COMPARING_TYPES = arrayOf(
            //Containers
            BPMNElementType.Process,
            BPMNElementType.Lane,
            BPMNElementType.SubProcess,

            BPMNElementType.Participant,
            BPMNElementType.Task,
            BPMNElementType.ExclusiveGateway,
            BPMNElementType.ParallelGateway,
            BPMNElementType.InclusiveGateway,
            BPMNElementType.EventBasedGateway,
            BPMNElementType.ComplexGateway,
            BPMNElementType.EndEvent,
            BPMNElementType.StartEvent,
            //      BPMNElementType.LaneSet,
            //      BPMNElementType.Definitions,
            //      BPMNElementType.ErrorEventDefinition,
            //      BPMNElementType.EscalationEventDefinition,
            //      BPMNElementType.Documentation,
            //      BPMNElementType.Operation,
            //      BPMNElementType.Artifact,
            //      BPMNElementType.Activity,
            //      BPMNElementType.ThrowEvent,
            //      BPMNElementType.DataOutput,
            //      BPMNElementType.DataInput,
            //      BPMNElementType.ResourceRole,
            //      BPMNElementType.CallableElement,
            //      BPMNElementType.DataAssociation,
            //      BPMNElementType.Collaboration,
            //      BPMNElementType.HumanPerformer,
            //      BPMNElementType.Expression,
            //      BPMNElementType.FormalExpression,
            //      BPMNElementType.Performer,
            //      BPMNElementType.ResourceAssignmentExpression,
            //      BPMNElementType.InputDataItem,
            //      BPMNElementType.CorrelationSubscription,
            //      BPMNElementType.ConversationAssociation,
            //      BPMNElementType.Property,
            //      BPMNElementType.CategoryValue,
            //      BPMNElementType.Import,
            //      BPMNElementType.CallConversation,
            //      BPMNElementType.SendTask,
            //      BPMNElementType.Resource,
            //      BPMNElementType.Conversation,
            //      BPMNElementType.Text,
            //      BPMNElementType.ServiceTask,
            //      BPMNElementType.TimeCycle,
            //      BPMNElementType.ActivationCondition,
            //      BPMNElementType.CompensateEventDefinition,
            //      BPMNElementType.DataInputAssociation,
            //      BPMNElementType.DataOutputAssociation,
            //      BPMNElementType.CancelEventDefinition,
            //      BPMNElementType.ResourceParameterBinding,
            //      BPMNElementType.DataObjectReference,
            //      BPMNElementType.UserTask,
            //      BPMNElementType.ConditionExpression,
            //      BPMNElementType.ExtensionElements,
            //      BPMNElementType.CorrelationPropertyBinding,
            //      BPMNElementType.ResourceParameter,
            //      BPMNElementType.ComplexBehaviorDefinition,
            //      BPMNElementType.Assignment,
            //      BPMNElementType.PotentialOwner,
            //      BPMNElementType.DataObject,
            //      BPMNElementType.TimeDuration,
            //      BPMNElementType.ReceiveTask,
            //      BPMNElementType.CorrelationPropertyRetrievalExpression,
            //      BPMNElementType.OutputSet,
            //      BPMNElementType.ItemDefinition,
            //      BPMNElementType.Monitoring,
            //      BPMNElementType.TimeDate,
            //      BPMNElementType.MessageEventDefinition,
            //      BPMNElementType.MultiInstanceLoopCharacteristics,
            //      BPMNElementType.Escalation,
            //      BPMNElementType.ConditionalEventDefinition,
            //      BPMNElementType.CorrelationKey,
            //      BPMNElementType.IntermediateCatchEvent,
            //      BPMNElementType.Rendering,
            //      BPMNElementType.Message,
            //      BPMNElementType.Auditing,
            //      BPMNElementType.ParticipantMultiplicity,
            //      BPMNElementType.IoBinding,
            //      BPMNElementType.Condition,
            //      BPMNElementType.Signal,
            //      BPMNElementType.GlobalConversation,
            //      BPMNElementType.SubConversation,
            //      BPMNElementType.Error,
            //      BPMNElementType.BusinessRuleTask,
            //      BPMNElementType.LoopCardinality,
            //      BPMNElementType.Interface,
            //      BPMNElementType.CorrelationProperty,
            //      BPMNElementType.OutputDataItem,
            //      BPMNElementType.LinkEventDefinition,
            //      BPMNElementType.Script,
            //      BPMNElementType.SignalEventDefinition,
            //      BPMNElementType.InputSet,
            //      BPMNElementType.BoundaryEvent,
            //      BPMNElementType.CallActivity,
            //      BPMNElementType.Relationship,
            //      BPMNElementType.EndPoint,
            //      BPMNElementType.CompletionCondition,
            //      BPMNElementType.TerminateEventDefinition,
            //      BPMNElementType.IntermediateThrowEvent,
            //      BPMNElementType.DataState,
            //      BPMNElementType.Extension,
            //      BPMNElementType.ManualTask,
            //      BPMNElementType.TextAnnotation,
            //      BPMNElementType.TimerEventDefinition,
            //      BPMNElementType.IoSpecification,
            //      BPMNElementType.ParticipantAssociation,
            //      BPMNElementType.ScriptTask,
            //      BPMNElementType.MessageFlowAssociation,
            //
            //With source and target elements
            BPMNElementType.Association,
            BPMNElementType.ConversationLink,
            BPMNElementType.MessageFlow,
            BPMNElementType.SequenceFlow
        )

        @JvmStatic
        fun fillModelElementTypes(
            firstBpmnModel: BpmnModelInstance,
            secondBpmnModel: BpmnModelInstance,
            typesInBothModels: ArrayList<BPMNElementType>,
            typesInAnyModel: ArrayList<BPMNElementType>
        ) {
            typesInBothModels.clear()
            typesInAnyModel.clear()

            for (bpmnElementType in BPMN_COMPARING_TYPES) {
                val firstContainsType = firstBpmnModel.getElementsByType(bpmnElementType).isNotEmpty()
                val secondContainsType = secondBpmnModel.getElementsByType(bpmnElementType).isNotEmpty()

                if (firstContainsType && secondContainsType) {
                    typesInBothModels.add(bpmnElementType)
                }

                if (firstContainsType || secondContainsType) {
                    typesInAnyModel.add(bpmnElementType)
                }
            }
        }

        fun constructComparisonResult(
            sourceBpmnModel: BpmnModelInstance,
            targetBpmnModel: BpmnModelInstance,
            matchedVertices: List<VertexChange>,
            matchedEdges: List<EdgeChange>,
            costConfig: CostConfig
        ): ComparisonResult {
            val matchedVerticesPairs: List<MatchingPair> = matchedVertices.map { it.toMatchingPair() }
            val matchedEdgesPairs: List<MatchingPair> = matchedEdges.map { it.toMatchingPair() }

            val matchedPairs: List<MatchingPair> = matchedVerticesPairs + matchedEdgesPairs

            @Suppress("UnnecessaryVariable")
            val result: ComparisonResult = ComparisonResult(
                sourceBpmnModel,
                targetBpmnModel,
                costConfig
            ).apply {
                penalty = matchedPairs.sumByDouble { it.differentLabel }

                addedSet.addAll(matchedPairs.asSequence().filter { it.type!! == PairType.INSERT })
                deletedSet.addAll(matchedPairs.asSequence().filter { it.type!! == PairType.DELETE })
                matchingSet.addAll(matchedPairs.asSequence().filter { it.type!! == PairType.MATCH })
            }

            return result
        }
    }
}
package ru.pais.vkr.utils

class StringUtil private constructor() {
    companion object {
        @JvmStatic
        fun replaceLast(string: String, substring: String, replacement: String): String {
            val index = string.lastIndexOf(substring)
            return if (index == -1) string else string.substring(0, index) + replacement + string.substring(index + substring.length)
        }

        @JvmStatic
        fun computeLevenshteinDistance(str1: String?, str2: String?): Int {
            val first = str1 ?: ""
            val second = str2 ?: ""

            val distance = Array(first.length + 1) { IntArray(second.length + 1) }

            for (i in 0..first.length)
                distance[i][0] = i
            for (j in 1..second.length)
                distance[0][j] = j

            for (i in 1..first.length)
                for (j in 1..second.length)
                    distance[i][j] = minOf(
                        distance[i - 1][j] + 1,
                        distance[i][j - 1] + 1,
                        distance[i - 1][j - 1] + if (first[i - 1] == second[j - 1]) 0 else 1
                    )
            return distance[first.length][second.length]
        }
    }
}

package ru.pais.vkr.utils

import org.camunda.bpm.model.xml.instance.ModelElementInstance
import ru.pais.vkr.comparator.entities.ComparisonResult

object SameForAllConfig {

    fun sameContainer(
        firstModel: ModelElementInstance,
        secondModel: ModelElementInstance,
        comparisonResult: ComparisonResult
    ): Boolean {
        val name1: String? = firstModel.parentElement.elementType.typeName
        val name2: String? = secondModel.parentElement.elementType.typeName

        val needsCompareParent =
            (name1 == "process" && name2 == "process") ||
            (name1 == "lane" && name2 == "lane") ||
            (name1 == "subProcess" && name2 == "subProcess")

        if (needsCompareParent) {
            val matchedPair = comparisonResult.matchingSet.firstOrNull { it.first!!.raw === firstModel.parentElement }

            val modelElementInstanceFromMap = if (matchedPair != null) matchedPair.second!!.raw else null

            return modelElementInstanceFromMap !== secondModel.parentElement
        }

        return false
    }
}

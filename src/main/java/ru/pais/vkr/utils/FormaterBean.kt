package ru.pais.vkr.utils

import java.text.MessageFormat
import java.util.Locale

class FormaterBean {
    fun format(number: String): String {
        val mf = MessageFormat("{0,number,#.##}", Locale("en_US"))
        return mf.format(arrayOf(number.toDouble()))
    }
}

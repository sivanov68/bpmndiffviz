package ru.pais.vkr.models

data class Model(
    var id: Long,
    var document: Document,
    var name: String
)
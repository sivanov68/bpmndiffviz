package ru.pais.vkr.models

import java.util.Date

data class Document(
    var id: Long = 0,
    var uploadDate: Date? = null,
    var name: String? = null,
    var systemName: String? = null
)
